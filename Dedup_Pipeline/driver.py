import warnings
warnings.filterwarnings("ignore")

import json
import time
from elasticsearch import helpers
import pandas as pd
import pymongo
from elasticsearch import Elasticsearch
import networkx as nx
import csv
import threading
import uuid
import pickle
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import datetime
import os
from bson import ObjectId
import gc
from ast import literal_eval
from multiprocessing import Pool
from tqdm import tqdm
tqdm.pandas()

from config import DATABASES, GOALS, FORMAT_REFERENCES, INTERMEDIATE_PATH

import utils
import image_vectorization as IV
import select_candidates as sc

class Dedup():
    def __init__(self, grade, status_filters):
        self.directory = f"{datetime.datetime.now():%d_%m_%Y}" 
        if not os.path.exists(os.path.join(INTERMEDIATE_PATH,self.directory)):
            os.makedirs(os.path.join(INTERMEDIATE_PATH,self.directory))

        self.mongo_uri = DATABASES.get("mongo").get("URL")
        self.es = Elasticsearch(DATABASES.get("ES").get("URL"))

        self.status_filters = status_filters
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client.contentgrail
        self.collection = self.db.learning_objects
        self.lm_coll = self.db.learning_maps
        self.lmf_coll = self.db.learning_map_formats
        self.lo_coll = self.db.learning_objects

        self.grade = grade
        self.all_data_dump = os.path.join(INTERMEDIATE_PATH,self.directory,'cg-{}.csv'.format(self.grade))
        self.extracted_clean_data_json_dump = os.path.join(INTERMEDIATE_PATH,self.directory,'cg-{}-cleaned'.format(self.grade))
        self.candidate_pairs_dump = os.path.join(INTERMEDIATE_PATH,self.directory,'cg-{}-candidate_pairs.csv'.format(self.grade))
        self.cluster_exact_file = os.path.join(INTERMEDIATE_PATH,self.directory,'cg_{}_exact_duplicates_clusters.csv'.format(self.grade))
        self.cluster_similar_file = os.path.join(INTERMEDIATE_PATH,self.directory,'cg_{}_similar_duplicates_clusters.csv'.format(self.grade))
        self.cluster_df_csv = os.path.join(INTERMEDIATE_PATH,self.directory,'cg_{}_final_clusters.csv'.format(self.grade))
        self.joined_df_cluster_csv = os.path.join(INTERMEDIATE_PATH,self.directory,'joined_q_level_df_with_cluster.csv')
        self.joined_q_level_df_csv = os.path.join(INTERMEDIATE_PATH,self.directory,'joined_q_level_df.csv')
        self.expected_output_list_path = os.path.join(INTERMEDIATE_PATH,self.directory,'expected_output_list.pkl')
        # self.final_question_level_output_index = 'cqi_cg_dedup_questions'
        # self.final_cluster_level_output_index = 'cqi_cg_dedup_clusters'

        self.es_index = "cg-{}-duplication-questions".format(str.lower(self.grade)) + "_" + self.directory
        self.final_index_name = "cg-{}-results_dedup".format(str.lower(self.grade)) + "_" + self.directory
        self.not_present_in_CB_LM = os.path.join(INTERMEDIATE_PATH,self.directory,'missing_lm_or_missing_cb.pkl')

        self.goals = GOALS.get(grade)

        self.FORMAT_REFERENCE = FORMAT_REFERENCES.get(grade)

        # self.img2vec = IV.Img2Vec(log_path=os.path.join(INTERMEDIATE_PATH,self.directory,"failed_image_urls.pkl"))

    def parallelize_dataframe(self,df,func,n_cores=4):
        df_split = np.array_split(df, n_cores)
        pool = Pool(n_cores)
        df = pd.concat(pool.map(func, df_split))
        pool.close()
        pool.join()
        return df

    def driver(self):
        # 1.        
        ques_los = utils.get_grade_wise_learning_objects(self.lm_coll, self.lo_coll, 'Question', self.goals, status_filters = self.status_filters)
        print("count of ques_los: ",len(ques_los))
        tenth_qs = list(ques_los['question_code'].unique())
        batch_size = 50000
        df = pd.DataFrame()

        for idx in tqdm(range(0,len(tenth_qs),batch_size),desc="Fetching LOs from CG"):
            temp = pd.DataFrame(self.collection.find({
                "question_code": {"$in": list(tenth_qs[idx:idx+batch_size])}},
                projection={
                    "id": True, "_version": True, "question_code": True, "subtype": True,
                    'content.question_details.en.answers.body': True, 
                    'content.question_details.en.question_txt': True, 
                    'content.question_details.en.answers.is_correct': True,
                    'content.question_details.hi.answers.body': True, 
                    'content.question_details.hi.question_txt': True,  
                    'content.question_details.hi.answers.is_correct': True
                    }
                )
            )
            df = pd.concat([df,temp])
        del ques_los

        # df.to_csv(self.all_data_dump,index=False)
        # print("data dumped to {}".format(self.all_data_dump))
        original_len = len(df)

        df = df.sort_values("_version",na_position="first").drop_duplicates(subset=["question_code"],keep="last")
        after_duplicate_removal = len(df)
        print("total_duplicates are {}".format(original_len - after_duplicate_removal))
        df_extract = df[["id", "_version", "question_code", "subtype", "content"]]
        del df  

        print("df_extract cols",df_extract.columns)
        print("df_extract: \n",df_extract.iloc[0])

        # 2.
        df_extract["extracted_field"] = df_extract["content"].progress_apply(utils.extract_content_field)

        df_extract["question_text"] = df_extract["extracted_field"].progress_apply(lambda x: x[0])

        df_extract["correct_answer_text"] = df_extract["extracted_field"].progress_apply(lambda x: x[1])

        df_extract["whole_answer_body"] = df_extract["extracted_field"].progress_apply(lambda x: x[2])

        #TODO: sort and concatenate whole_answer_body

        # 2.1 Fetch Image URLs
        df_extract["question_urls"] = df_extract["question_text"].progress_apply(utils.fetch_urls)
        # df_extract["answer_urls"] = df_extract["correct_answer_text"].progress_apply(utils.fetch_urls) #TODO: consider whole_answer_body
        df_extract["answer_urls"] = df_extract["whole_answer_body"].progress_apply(utils.fetch_urls)
        df_extract["is_image_present"] = df_extract.progress_apply(lambda x: True if sum([len(x["question_urls"]),len(x["answer_urls"])])>0 else False,axis=1)        

        print("count of LOs which have images: ",df_extract[df_extract.is_image_present].shape)
        # Optimization
        
        df_extract.drop(["content","extracted_field"],axis=1,inplace=True)

        # 3.0 Fetch Dense Representation

        # df_extract = df_extract.progress_apply(self.img2vec.find_image_dense_representation,axis=1)
        # img2vec = IV.Img2Vec(log_path=os.path.join(INTERMEDIATE_PATH,self.directory,"failed_image_urls.pkl"))
        df_extract = self.parallelize_dataframe(df_extract,utils.image_dense_tqdm_apply)

        # print("dumping failed URLs on local...")
        # img2vec.dump_failed_urls()

        print("df_extract with image dense vecs: \n",df_extract.head(2))
        df_extract.to_csv(self.extracted_clean_data_json_dump,index=False)

        # df_extract = pd.read_csv(self.extracted_clean_data_json_dump) # REMOVE

        # df_extract["question_urls"] = df_extract["question_urls"].progress_apply(literal_eval) # REMOVE
        # df_extract["answer_urls"] = df_extract["answer_urls"].progress_apply(literal_eval) # REMOVE

        # df_extract["question_image_dense"] = df_extract["question_image_dense"].progress_apply(lambda x: literal_eval(x) if str(x)!="nan" else x) # REMOVE
        # df_extract["answer_image_dense"] = df_extract["answer_image_dense"].progress_apply(lambda x: literal_eval(x) if str(x)!="nan" else x) # REMOVE
        # print("df_extract size: ",df_extract.shape) # REMOVE

        # 3.
        input_process = utils.Proccessing(grail_df=df_extract)
        grail_dataframe = input_process.start_processing() # TODO: change answer field in called function, and use whole_answer_body instead
        
        del input_process
        del df_extract

        grail_dataframe = grail_dataframe.where(pd.notnull(grail_dataframe), None)
        grail_dataframe["is_question_image_dense_present"] = grail_dataframe["question_image_dense"].progress_apply(lambda x: False if x is None else True)
        grail_dataframe["is_answer_image_dense_present"] = grail_dataframe["answer_image_dense"].progress_apply(lambda x: False if x is None else True)

        grail_dataframe["question_image_dense"] = grail_dataframe.progress_apply(
            lambda x: np.zeros((512,),dtype="int8").tolist() if x["is_question_image_dense_present"]==False else x["question_image_dense"],
            axis=1)
        grail_dataframe["answer_image_dense"] = grail_dataframe.progress_apply(
            lambda x: np.zeros((512,),dtype="int8").tolist() if x["is_answer_image_dense_present"]==False else x["answer_image_dense"],
            axis=1)

        grail_dataframe["is_question_text_present"] = grail_dataframe.question_info_clean_complete.progress_apply(lambda x: True if len(x)>0 else False)
        grail_dataframe["is_answer_text_present"] = grail_dataframe.whole_answer_body.progress_apply(lambda x: True if len(x)>0 else False)

        grail_dataframe["question_id_with_version"] = grail_dataframe.progress_apply(
            lambda x: str(int(x["id"]))+"__"+str(x["_version"]), axis=1
            )
        grail_dataframe["question_code_with_version"] = grail_dataframe.progress_apply(
            lambda x: str(x["question_code"])+"__"+str(x["_version"]), axis=1
            )

        #TODO: check appropriate answer field is present in filter
        grail_dataframe_extract = grail_dataframe[[
            "id", "_version", "question_code", "subtype", "question_id_with_version", "question_code_with_version",
            "is_question_text_present", "question_text", "question_info_clean_complete", 
            "is_answer_text_present", "whole_answer_body", "correct_answer_text", "correct_option",
            'question_urls', 'answer_urls', 'is_image_present',
            'question_image_dense', 'answer_image_dense',
            "is_question_image_dense_present", "is_answer_image_dense_present"
            ]]

        del grail_dataframe

        # print("grail df extract: \n",grail_dataframe_extract.head(2))
        grail_dataframe_extract.to_json(self.extracted_clean_data_json_dump, orient="records") 
        del grail_dataframe_extract
        # grail_dataframe_extract = pd.read_json(self.extracted_clean_data_json_dump, orient="records") # REMOVE
        # print("grail df extract: ",grail_dataframe_extract.shape)

        # 4.
        output_layer = utils.Ingest_ES(False, False, True, self.es, self.extracted_clean_data_json_dump, self.es_index) # TODO: change mapping in called function
        output_layer.data_ingestion()
        del output_layer

        grail_dataframe_extract = pd.read_json(self.extracted_clean_data_json_dump, orient="records")
        
        # TODO: add whole_answer_body field in col filter
        grail_dataframe_2 = grail_dataframe_extract[
            [
                "question_code", "is_question_text_present", "question_info_clean_complete", 
                "is_answer_text_present", "whole_answer_body",
                "is_question_image_dense_present", "question_image_dense",
                "is_answer_image_dense_present", "answer_image_dense"
            ]
        ]
        qcode_to_qid_dict = dict(zip(grail_dataframe_extract.question_code, grail_dataframe_extract.id))

        # 5. Text , Text + IMG and Only IMG sections
        num_threads = 8
        expected_output = []
        not_es_search_codes = []
        
        list_production = grail_dataframe_2.values.tolist()
        del grail_dataframe_2
        del grail_dataframe_extract

        print("len of grail_dataframe_2: ",len(list_production))

        # TODO: add extra function param for answer_text, change function definition at source to consider answer param
        threads = [threading.Thread(
            target= sc.search, args=(
                self.es, self.es_index,
                items[0], items[1], items[2], items[3], items[4], items[5], items[6], items[7], items[8],
                expected_output, not_es_search_codes
            )
        ) 
                for items in list_production]

        for i in tqdm(range(0, int(len(threads) / num_threads) + 1), desc="Multi-Threading"):
            [ti.start() for ti in threads[i * num_threads:(i + 1) * num_threads]]
            [ti.join() for ti in threads[i * num_threads:(i + 1) * num_threads]]

        print("expected_output list: ",len(expected_output))

        with open(self.expected_output_list_path,"wb") as file:
            pickle.dump(expected_output,file)

        del threads
        del not_es_search_codes

        # TODO: update col set and include answer + images fields
        candidate_df = pd.DataFrame(expected_output,
                                    columns=[
                                        'original_question_code', 'duplicate_question_code', "Laven_ratios", "answer_laven_ratios", 
                                        "original_question_text", "duplicate_question_text", "original_answer_text", "duplicate_answer_text",
                                        "question_image_similarity", "answer_image_similarity"
                                        ]) # TODO: original_answer_text, duplicate_answer_text, ques_img_score, ans_img_score, rm img_field

        del expected_output[:]
        del expected_output
        del list_production

        candidate_df.to_csv(self.candidate_pairs_dump,index=False)        
        
        # candidate_df = pd.read_csv(self.candidate_pairs_dump) #REMOVE
        # candidate_df["original_question_text"].fillna("",inplace=True)
        # candidate_df["original_answer_text"].fillna("",inplace=True)
        # candidate_df["duplicate_question_text"].fillna("",inplace=True)
        # candidate_df["duplicate_answer_text"].fillna("",inplace=True)
        # candidate_df["original_dense_vector"] = candidate_df["original_dense_vector"].apply(lambda x: np.array(eval(x),dtype="float16")) # REMOVE
        # candidate_df["duplicate_dense_vector"] = candidate_df["duplicate_dense_vector"].apply(lambda x: np.array(x[1:-1].split()).astype("float16")) #REMOVE
        # grail_dataframe_extract = pd.read_json(self.extracted_clean_data_json_dump, orient="records") #REMOVE
        # print("grail df extract: ",grail_dataframe_extract.shape) #REMOVE
        # print("candidate df: ",candidate_df.shape) #REMOVE

        # TODO: consider both answer and question images
        # candidate_df["original_dense_vector"] = candidate_df["original_dense_vector"].progress_apply(lambda x: np.array(x,dtype="float16"))
        # candidate_df["duplicate_dense_vector"] = candidate_df["duplicate_dense_vector"].progress_apply(lambda x: np.array(x,dtype="float16"))

        # candidate_df_text = candidate_df[~candidate_df.Laven_ratios.isnull()]
        # candidate_df_image = candidate_df[candidate_df.Laven_ratios.isnull()]
        # print(candidate_df_text.shape, candidate_df_image.shape, candidate_df.shape)
        # del candidate_df

        # candidate_df
        # candidate_df_text["tuple_org_dup_text"] = candidate_df_text.progress_apply(
        #     lambda x: (x["original_question_text"], x["duplicate_question_text"]), axis=1)

        # candidate_df_text["same"] = candidate_df_text.progress_apply(
        #     lambda x: True if (x["original_question_code"] == x["duplicate_question_code"]) else False, axis=1)
        candidate_df["same"] = candidate_df.progress_apply(
            lambda x: True if (x["original_question_code"] == x["duplicate_question_code"]) else False, axis=1)



        # 6.

        # candidate_df_85 = candidate_df_text #[candidate_df_text["Laven_ratios"] > 0.85]
        # del candidate_df_text
        # candidate_df_85["transformer_scores"] = candidate_df_85["tuple_org_dup_text"].progress_apply(get_transformer_score)
        # candidate_df_85


        # TODO: calculate laven scores for answer body as well
        # candidate_df_85["clean_laven_ratios"] = candidate_df_85.progress_apply(sc.clean_laven_ratios, axis=1)
        # candidate_df_85["same"] = candidate_df_85.progress_apply(
        #     lambda x: True if (x["original_question_code"] == x["duplicate_question_code"]) else False, axis=1)
        # candidate_df_85 = candidate_df_85[candidate_df_85["same"] != True]
        # candidate_df_85.rename(columns = {'transformer_scores':'transformer_score'}, inplace = True)
        # candidate_df_85["true_labels"] = 1

        candidate_df["clean_laven_ratios"] = candidate_df.progress_apply(lambda x: sc.clean_laven_ratios(x,"question"), axis=1)
        candidate_df["answer_clean_laven_ratios"] = candidate_df.progress_apply(lambda x: sc.clean_laven_ratios(x, "answer"), axis=1)
        candidate_df = candidate_df[candidate_df["same"] != True]


        # candidate_df_85["transformer_score"] = 0  # TODO: Remove this line

        ## Find Exact and Similar subgroups 

        ### For Text
        # 7.

        # TODO: change definition at source, consider answer body + all images
        # candidate_df_85["pred_label"] = candidate_df_85.progress_apply(sc.check_duplicate_exact, axis=1)
        # candidate_df_85_exact = candidate_df_85[candidate_df_85["pred_label"] == 1]
        # candidate_df_85_similar = candidate_df_85[candidate_df_85["pred_label"] != -1]
        # candidate_df_85_similar = candidate_df_85[candidate_df_85["pred_label"] == 0]

        candidate_df["pred_label"] = candidate_df.progress_apply(sc.check_duplicate_exact, axis=1) # TODO: change called function
        
        #TODO: add question_ids for org and dup questions
        candidate_df["original_id"] = candidate_df.original_question_code.apply(lambda x: qcode_to_qid_dict.get(x,None))
        candidate_df["duplicate_id"] = candidate_df.duplicate_question_code.apply(lambda x: qcode_to_qid_dict.get(x,None))

        shape_before = candidate_df.shape

        candidate_df = candidate_df.dropna(subset=["original_id","duplicate_id"])
        print("before dropping nan:",shape_before," after dropping nan:",candidate_df.shape)
        
        candidate_df_exact = candidate_df[candidate_df["pred_label"] == 1]
        candidate_df_similar = candidate_df[candidate_df["pred_label"] != -1]
        
        # TODO: remove unneccesary answer related ops
        # correct_answer_df = grail_dataframe_extract[["question_code", "correct_option"]]
        # del candidate_df_85

        ### For Image
        # TODO: add both answer and question image scores
        # candidate_df_image["cosine_similarity"] = candidate_df_image.progress_apply(
        #     lambda x: cosine_similarity(x["original_dense_vector"].reshape((1,512)),x["duplicate_dense_vector"].reshape((1,512))).item(),
        #     axis=1
        # )
        # candidate_df_image = pd.merge(grail_dataframe_extract[['id', 'question_code']], candidate_df_image, 
        #                             left_on="question_code",
        #                     right_on="original_question_code")
        # candidate_df_image.rename(columns={"id":"original_id"},inplace=True)

        # candidate_df_image = pd.merge(grail_dataframe_extract[['id', 'question_code']], candidate_df_image, 
        #                             left_on="question_code",
        #                     right_on="duplicate_question_code")
        # candidate_df_image.rename(columns={"id":"duplicate_id"},inplace=True)

        # TODO: consider both question and answer image scores to decide similarity
        # candidate_df_image_exact = candidate_df_image[(candidate_df_image.cosine_similarity > 0.99)&
        #                                             (candidate_df_image.duplicate_question_text.isnull())]
        # candidate_df_image_similar = candidate_df_image[(candidate_df_image.cosine_similarity > 0.99)]
        # del candidate_df_image

        ## Exact

        ### Only Text and Text + Image

        # TODO: see if correct_answer_dict can be removed
        # correct_answer_dict = correct_answer_df.set_index("question_code")["correct_option"].to_dict()


        # 8.
        # candidate_df_85_exact["answers"] = candidate_df_85_exact.progress_apply(lambda x: utils.get_answer(x, correct_answer_dict), axis=1)


        # org_correct_ans = pd.merge(correct_answer_df, candidate_df_85_exact, left_on="question_code",
        #                         right_on="original_question_code")
        # dup_correct_ans = pd.merge(correct_answer_df, org_correct_ans, left_on="question_code",
        #                         right_on="duplicate_question_code")
        # added_id_df = pd.merge(grail_dataframe_extract[['id', 'question_code']], dup_correct_ans, left_on="question_code",
        #                     right_on="original_question_code")
        # added_id_df = added_id_df.drop(columns=['question_code'])
        # added_id_df = added_id_df.rename(columns={"id": "original_id"})
        # added_id_df = pd.merge(grail_dataframe_extract[['id', 'question_code']], added_id_df, left_on="question_code",
        #                     right_on="duplicate_question_code")
        # added_id_df = added_id_df.drop(columns=['question_code'])
        # added_id_df = added_id_df.rename(columns={"id": "duplicate_id"})
        # dup_correct_ans = added_id_df
        # 9.

        # dup_correct_ans["correct_option_x"] = dup_correct_ans["correct_option_x"].progress_apply(utils.make_answer_lower)
        # dup_correct_ans["correct_option_y"] = dup_correct_ans["correct_option_y"].progress_apply(utils.make_answer_lower)


        # dup_correct_ans["laven_ans_ratio"] = dup_correct_ans.progress_apply(utils.laven_ans_ratio, axis=1)
        # dup_correct_ans["laven_ans_ratio"].value_counts(bins=10)
        # dup_correct_ans_exact = dup_correct_ans[
        #     (dup_correct_ans["laven_ans_ratio"] > 0.72) & (dup_correct_ans["laven_ans_ratio"] <= 1)]
        # dup_correct_ans_exact = added_id_df # dup_correct_ans

        # exact_pairs = dup_correct_ans_exact[["original_id","duplicate_id"]]
        # print("exact pairs: ",exact_pairs.shape)
        # exact_pairs = pd.concat([exact_pairs,candidate_df_image_exact[["original_id","duplicate_id"]]])

        ### clustering
        exact_pairs = candidate_df_exact[["original_id","duplicate_id"]]

        duplicate_pairs_list = []
        for _, row in tqdm(exact_pairs.iterrows(), desc="pair ids"):
            duplicate_pairs_list.append((str(row["original_id"]), str(row["duplicate_id"])))
        G_dup = nx.Graph()
        G_dup.add_edges_from(duplicate_pairs_list)
        clusters = list(G_dup.subgraph(c) for c in nx.connected_components(G_dup))
        # clusters = list(nx.connected_component_subgraphs(G_dup))
        cluster_outcsv_85 = open(self.cluster_exact_file, 'w')
        cluster_writer_85 = csv.writer(cluster_outcsv_85)
        cluster_writer_85.writerow(['cluster_question_codes'])
        for i in range(len(clusters)):
            cluster_writer_85.writerow([', '.join(clusters[i].nodes())])
        cluster_outcsv_85.close()
        # cluster_df = pd.read_csv(cluster_exact_file)
        # cluster_level_info = []
        # failed = []

        ## Similar

        ### only Text and Text + Image


        # candidate_df_85_similar["answers"] = candidate_df_85_similar.progress_apply(lambda x: utils.get_answer(x,correct_answer_dict), axis=1)


        # print("candidate_df_85_similar: ",candidate_df_85_similar.shape)


        # org_correct_ans_similar = pd.merge(correct_answer_df, candidate_df_85_similar, left_on="question_code",
        #                         right_on="original_question_code")
        # dup_correct_ans_similar = pd.merge(correct_answer_df, org_correct_ans_similar, left_on="question_code",
        #                         right_on="duplicate_question_code")
        # added_id_df_similar = pd.merge(grail_dataframe_extract[['id', 'question_code']], dup_correct_ans_similar, left_on="question_code",
        #                     right_on="original_question_code")
        # added_id_df_similar = added_id_df_similar.drop(columns=['question_code'])
        # added_id_df_similar = added_id_df_similar.rename(columns={"id": "original_id"})
        # added_id_df_similar = pd.merge(grail_dataframe_extract[['id', 'question_code']], added_id_df_similar, left_on="question_code",
        #                     right_on="duplicate_question_code")
        # added_id_df_similar = added_id_df_similar.drop(columns=['question_code'])
        # added_id_df_similar = added_id_df_similar.rename(columns={"id": "duplicate_id"})
        # dup_correct_ans_similar = added_id_df_similar
        # dup_correct_ans_similar["correct_option_x"] = dup_correct_ans_similar["correct_option_x"].progress_apply(utils.make_answer_lower)
        # dup_correct_ans_similar["correct_option_y"] = dup_correct_ans_similar["correct_option_y"].progress_apply(utils.make_answer_lower)

        # dup_correct_ans_similar["laven_ans_ratio"] = dup_correct_ans_similar.progress_apply(utils.laven_ans_ratio, axis=1)
        # dup_correct_ans_similar["laven_ans_ratio"].value_counts(bins=10)
        # dup_correct_ans_exact_similar = dup_correct_ans_similar[
        #     (dup_correct_ans_similar["laven_ans_ratio"] > 0.72) & (dup_correct_ans_similar["laven_ans_ratio"] <= 1)]
        # dup_correct_ans_exact_similar =  added_id_df_similar  # dup_correct_ans_similar

        # print("dup_correct_ans_exact_similar: ",dup_correct_ans_exact_similar.shape)

        ### clustering


        similar_pairs = candidate_df_similar[["original_id","duplicate_id"]]
        # similar_pairs = pd.concat([similar_pairs,candidate_df_image_similar[["original_id","duplicate_id"]]])
        print("similar_pairs: ",similar_pairs.shape)

        duplicate_pairs_list = []
        for _, row in tqdm(similar_pairs.iterrows(),desc="pair ids"):
            duplicate_pairs_list.append((str(row["original_id"]), str(row["duplicate_id"])))

        G_dup = nx.Graph()
        G_dup.add_edges_from(duplicate_pairs_list)
        clusters = list(G_dup.subgraph(c) for c in nx.connected_components(G_dup))
        # clusters = list(nx.connected_component_subgraphs(G_dup))
        cluster_outcsv_85 = open(self.cluster_similar_file, 'w')
        cluster_writer_85 = csv.writer(cluster_outcsv_85)
        cluster_writer_85.writerow(['cluster_question_codes'])
        for i in range(len(clusters)):
            cluster_writer_85.writerow([', '.join(clusters[i].nodes())])
        cluster_outcsv_85.close()
        # cluster_df = pd.read_csv(cluster_exact_file)
        # cluster_level_info = []
        # failed = []

        # del candidate_df_85_exact
        # del candidate_df_85_similar
        # del candidate_df_image_exact
        # del candidate_df_image_similar
        # del correct_answer_df
        # del correct_answer_dict
        del duplicate_pairs_list
        # del grail_dataframe_extract
        del G_dup
        del similar_pairs
        del exact_pairs
        del clusters
        # del dup_correct_ans
        # del dup_correct_ans_exact
        # del dup_correct_ans_exact_similar
        # del dup_correct_ans_similar
        # del added_id_df
        # del added_id_df_similar
        # del org_correct_ans
        # del org_correct_ans_similar
        del original_len 
        
        # ------------------------------------ Post Processing ---------------------------------------------

        res = helpers.scan(
            client = self.es,scroll = '2m',
            query = {"query":{"match_all":{}},"_source":["_id","question_code","question_version","is_question_image_dense_present","is_answer_image_dense_present"]},
            index= self.es_index)
        ids = []
        id_code = []
        for id in tqdm(res,desc="fetching los from ES"):
            ids.append(int(float(id['_id'])))
            id_code.append([
                int(float(id['_id'])),id["_source"]["question_code"],id["_source"]["question_version"],
                id["_source"]["is_question_image_dense_present"],id["_source"]["is_answer_image_dense_present"]
                ])

        print("ques fetched from ES: ",len(ids))

        # res = helpers.scan(client = self.es,size=10000,scroll = '5m',query = {"query":{"terms":{"_id":ids}},"_source":["_id","avg_hygiene_score","lm_codes"]},index="cqi-content-bank")
        # res = helpers.scan(client = self.es,size=10000,scroll = '5m',query = {"query":{"match_all":{}},"_source":["_id","avg_hygiene_score","lm_codes"]},index="cqi-content-bank")
        # id_hyg = {}
        # for id in tqdm(res,desc="fetching meta from CB"):
        #     id_hyg[int(float(id['_id']))]= (id['_source']['avg_hygiene_score'],id['_source']['lm_codes'])

        id_hyg = {}

        for i in tqdm(range(0,len(ids),10000),desc="fetching meta from CB"):
            _ids = ids[i:i+10000]
            res = helpers.scan(client = self.es,size=10000,scroll = '5m',query = {"query":{"terms":{"_id":_ids}},"_source":["_id","avg_hygiene_score","lm_codes"]},index="cqi-content-bank")
            for id in res:
                id_hyg[int(float(id['_id']))]= (id['_source']['avg_hygiene_score'],id['_source']['lm_codes'])

        print("ques fetched from CB: ",len(id_hyg))


        all_q_lms = []
        lm_codes = set()
        no_lm = []
        no_cb = []

        for i in ids:
            q_lms = []
            cb_op = id_hyg.get(i,None)
            if cb_op:
                q_lms.extend(cb_op[1])
                lm_codes.update(cb_op[1])
                all_q_lms.append({"id":i,"learning_maps":q_lms,"avg_hygiene":cb_op[0]})
            else:
                no_cb.append(i)
            if len(q_lms) == 0:
                no_lm.append(i)

        del id_hyg

        with open(self.not_present_in_CB_LM,"wb") as file:
            pickle.dump((no_cb,no_lm),file)

        print("len of no_cb: ",len(no_cb)," len of no_lm: ",len(no_lm))

        print("length of all_q_lms: ",len(all_q_lms))


        q_lms_df = pd.DataFrame(all_q_lms)

        q_lm_map_df = q_lms_df.explode("learning_maps")
        del q_lms_df

        print("shape q_lm_map_df: ",q_lm_map_df.shape)

        a = self.lm_coll.find({"code":{"$in":list(lm_codes)},"status":"active"},{"format_reference":1,"learnpath_name":1,"code":1,"learnpath_code":1})

        lms =[]
        for i in a:
            lms.append(i)

        lm_df = pd.DataFrame(lms)
        print("lm_df: ",lm_df.shape)
        # lm_f = db.learning_map_formats

        maps = self.FORMAT_REFERENCE 

        if len(maps)!=0:
            object_maps = [ObjectId(_) for _ in maps]
            lm_df = lm_df[lm_df['format_reference'].isin(object_maps)]


        updated = []
        for i, row in tqdm(lm_df.iterrows(),desc="split TLPN"):
            lm_level_values = row['learnpath_name'].split("--")
            if len(lm_level_values)!=6:
                print(i," more than 6 entities: ",lm_level_values)
                continue
            else:
                row["goal"] = lm_level_values[0]
                row['grade'] = lm_level_values[1]
                row['subject'] = lm_level_values[2]
                row['unit'] = lm_level_values[3]
                row['chapter'] = lm_level_values[4]
                row['topic'] = lm_level_values[5]
            updated.append(row)


        lm_df_with_all = pd.DataFrame(updated)
        joined_q_level_df = q_lm_map_df.merge(lm_df_with_all,left_on="learning_maps",right_on="code")
        del lm_df_with_all
        # joined_q_level_df.to_csv(self.joined_q_level_df_csv,index=False)

        print("joined_q_level_df: ",joined_q_level_df.shape)


        clusters = pd.read_csv(self.cluster_exact_file)
        clusters['ids'] = clusters['cluster_question_codes'].progress_apply(lambda x:[i.strip() for i in x.split(',')])
        q_cluster = {}
        cluster_q_map = []
            
        for i, row in tqdm(clusters.iterrows(),desc="exact clusters 1"):
            cluster_id = uuid.uuid1()
            cluster_q_map.append({'cluster_id': cluster_id,
        'question_id': set(row['ids'])})
            for r in row['ids']:
                q_cluster[int(float(r))] = cluster_id        

        for i in tqdm(all_q_lms,desc="exact clusters 2"):
            if i['id'] not in q_cluster:
                cluster_id = uuid.uuid1()
                cluster_q_map.append({'cluster_id': cluster_id,
                'question_id': set([str(i['id'])])})
                q_cluster[i['id']] = cluster_id
        
        cl_df = pd.DataFrame(q_cluster.items())
        cl_df = cl_df.rename(columns=({0:"q_id",1:"cluster_id"}))
        cl_df['q_id'] = cl_df['q_id'].progress_apply(int)
        cl_df['cluster_id'] = cl_df['cluster_id'].progress_apply(str)
        cl_df.to_csv(os.path.join(INTERMEDIATE_PATH,self.directory,"cl_df.csv"))
                
        ### --------------------- finding similar cluster, by excluding exacts from similars -------------------        
        id_code_df = pd.DataFrame(id_code,columns=["id","question_code","question_version","is_question_image_dense_present","is_answer_image_dense_present"])
        qid_to_qversion = dict(zip(id_code_df.id, id_code_df.question_version))

        sim_clusters = pd.read_csv(self.cluster_similar_file)
        sim_clusters['ids'] = sim_clusters['cluster_question_codes'].progress_apply(lambda x:set([i.strip() for i in x.split(',')]))
        # set_sim_clusters=[]
        # for i in range(len(sim_clusters)):
        #     set_sim_clusters.append(set(sim_clusters[i].nodes()))
        set_sim_clusters = sim_clusters.ids.tolist()

        result = []
        for exact_cluster in tqdm(cluster_q_map,desc="similar nested loop"):
            sims = []
            for sim_cluster in set_sim_clusters:
                if exact_cluster['question_id'].issubset(sim_cluster):
                    sims.extend(sim_cluster.difference(exact_cluster['question_id']))
            cluster_info = {}
            cluster_info['similar_questions'] = set(sims)
            cluster_info["similar_questions_with_version"] = [str(int(x))+"__"+str(qid_to_qversion.get(int(x),0)) for x in cluster_info['similar_questions']]
            cluster_info['cluster_id'] = exact_cluster['cluster_id']
            cluster_info['exact_questions'] = exact_cluster['question_id']
            cluster_info["exact_questions_with_version"] = [str(int(x))+"__"+str(qid_to_qversion.get(int(x),0)) for x in cluster_info['exact_questions']]
            result.append(cluster_info)

        cluster_df = pd.DataFrame(result)
        cluster_df["cluster_id"] = cluster_df["cluster_id"].progress_apply(str)
        cluster_df.to_csv(self.cluster_df_csv,index=False)


        cl_df = cl_df.merge(cluster_df,left_on="cluster_id",right_on="cluster_id")
        joined_q_level_df_with_cluster = joined_q_level_df.merge(cl_df,left_on='id',right_on='q_id') # joined_q_level_df_with_all

        del joined_q_level_df

        joined_q_level_df_with_cluster["similar_questions"] = joined_q_level_df_with_cluster["similar_questions"].progress_apply(lambda x: list(eval(x)) if type(x)==str else list(x))
        joined_q_level_df_with_cluster["exact_questions"] = joined_q_level_df_with_cluster["exact_questions"].progress_apply(lambda x: list(eval(x)) if type(x)==str else list(x))
        # joined_q_level_df_with_cluster.to_csv(self.joined_df_cluster_csv,index=False)
        
        # joined_q_level_df_with_cluster = pd.read_csv(self.joined_df_cluster_csv,converters={"similar_questions":lambda x: list(eval(x)),"exact_questions":list(eval(x))}) # REMOVE


        df = joined_q_level_df_with_cluster
        df.reset_index(inplace=True)
        df = df.sort_values("avg_hygiene",ascending=False)
        df = utils.find_representatives(df,"grade")
        df = utils.find_representatives(df,"subject")
        df = utils.find_representatives(df,"chapter")
        df = utils.find_representatives(df,"topic")
        joined_q_level_df_with_cluster = df.drop("index",axis=1)
        
        joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.merge(id_code_df,on="id",how="left")

        joined_q_level_df_with_cluster.rename(columns={"q_id":"question_id"},inplace=True)
        joined_q_level_df_with_cluster["size_exact_cluster"] = joined_q_level_df_with_cluster.exact_questions.apply(len)
        joined_q_level_df_with_cluster["size_similar_cluster"] = joined_q_level_df_with_cluster.similar_questions.apply(len)

        joined_q_level_df_with_cluster["question_id_with_version"] = joined_q_level_df_with_cluster.progress_apply(
            lambda x: str(x["question_id"])+"__"+str(x["question_version"]), axis=1
        )

        joined_q_level_df_with_cluster["question_code_with_version"] = joined_q_level_df_with_cluster.progress_apply(
            lambda x: str(x["question_code"])+"__"+str(x["question_version"]), axis=1
        )

        joined_q_level_df_with_cluster.to_csv(os.path.join(INTERMEDIATE_PATH,self.directory,"final_df.csv"),index=False)
        # joined_q_level_df_with_cluster = pd.read_csv(os.path.join(INTERMEDIATE_PATH,self.directory,"final_df.csv"),converters={"similar_questions":lambda x: list(eval(x)),"exact_questions":lambda x: list(eval(x))}) # REMOVE

        print("joined_q_level_df_with_cluster: ",joined_q_level_df_with_cluster.shape)
        print("joined_q_level_df_with_cluster: \n",joined_q_level_df_with_cluster.head())


        joined_q_level_df_with_cluster.drop(["id","_id","code"],axis=1,inplace=True)
        joined_q_level_df_with_cluster["format_reference"] = joined_q_level_df_with_cluster["format_reference"].progress_apply(str)

        joined_q_level_df_with_cluster["similar_questions"] = joined_q_level_df_with_cluster["similar_questions"].progress_apply(list)

        joined_q_level_df_with_cluster.drop_duplicates(["question_id","learning_maps"],inplace=True)
        # joined_q_level_df_with_cluster.head()

        def doc_generator(documents_to_be_indexed):
            for document in tqdm(iter(documents_to_be_indexed),desc="Ingest final clusters on ES"):
                yield {
                    "_index": self.final_index_name,
                    "_type": "_doc",
                    "_source": json.loads(json.dumps(document)),
                }
            raise StopIteration

        _ = helpers.bulk(self.es, doc_generator(joined_q_level_df_with_cluster.to_dict(orient='records'))) 

