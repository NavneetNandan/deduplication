import pandas as pd
import ast
import re
from bs4 import BeautifulSoup
from pylatexenc.latex2text import LatexNodes2Text
import json
from elasticsearch import helpers
import math 
import Levenshtein
import image_vectorization as IV
import os
from config import INTERMEDIATE_PATH
import random
from tqdm import tqdm
tqdm.pandas()

# 1.
def get_lm_codes(lm_coll, grades):
    lm_data = list(
        lm_coll.find({'status': 'active', 'format.index': 2, 'format.name': {"$in": grades}}, {"code": 1, "_id": 0}))
    return pd.DataFrame(lm_data).code.to_list()

def get_learning_objects(lo_coll, lo_type, lms, grades, a_id, status_filters):
    if status_filters:
        query = {
                'type': lo_type, 'status': {"$in": status_filters}, 
                'content.question_meta_tags.learning_maps': {"$in": lms}
            }
        if "Banking" in grades:
            query2 = {
                'type':lo_type,'content.question_meta_tags.source': {"$in":["mockbank","OT"]}, 'status': {"$in": status_filters}
            }
        else:
            query2 = None
    else:
        query = {
                'type': lo_type, 
                'content.question_meta_tags.learning_maps': {"$in": lms}
            }
        if "Banking" in grades:
            query2 = {
                'type':lo_type,'content.question_meta_tags.source': {"$in":["mockbank","OT"]}
            }
        else:
            query2=None
    
    _projection = {"question_code": True}

    if a_id == None:
        lo_data = list(lo_coll.find(query,projection= _projection))
        if query2:
            lo_data.extend(list(lo_coll.find(query2,projection= _projection)))
    else:
        lo_data = list(lo_coll.find(
            {'type': lo_type, "author_id": a_id, 'content.question_meta_tags.learning_maps': {"$in": lms}},projection=_projection))

    return pd.DataFrame(lo_data)

def get_grade_wise_learning_objects(lm_coll, lo_coll, lo_type, grades, a_id=None, status_filters=None):
    lms = get_lm_codes(lm_coll, grades)
    print(len(lms))
    data = get_learning_objects(lo_coll, lo_type, lms, grades, a_id, status_filters)
    return data

# 2.
def extract_content_field(x):
    try:
        content = ast.literal_eval(x)
    except:
        content = x

    try:
        question_details = content["question_details"]
        tag = list(question_details.keys())[0]
        question_text = question_details[tag]["question_txt"]
    except:
        print(content)
        question_text = ""
    try:
        whole_answer = question_details[tag]["answers"]
        whole_answer_body = []
        # print(whole_answer)
        if not whole_answer:
            whole_answer_body = []
            correct_answer = ""

        else:
            try:
                for answer in whole_answer:
                    if answer["is_correct"] == True:
                        correct_answer = answer["body"]
                        # whole_answer_body.append(correct_answer)

                    whole_answer_body.append(answer["body"])
            except:
                try:
                    for answer in whole_answer[0]:
                        if answer["is_correct"] == True:
                            correct_answer = answer["body"]
                            # whole_answer_body.append(correct_answer)

                        whole_answer_body.append(answer["body"])
                except:
                    whole_answer_body = []
                    correct_answer = ""
    except:
        whole_answer_body = []
        correct_answer = ""
    
    whole_answer_body = " ".join(sorted(map(lambda x: x.strip(),whole_answer_body))).strip()
    try:
        question_text_answer = (question_text, correct_answer, whole_answer_body)
    except:
        question_text_answer = (question_text, "", whole_answer_body)

    return question_text_answer

# 2.1 Fetch Image URLs
def fetch_urls(text):
    if type(text)!=str or len(text)==0:
        return []
    else:
        return re.findall(r"src\s*=\s*[\"|'](.+?)['|\"]",text)

def image_dense_tqdm_apply(df):
    img2vec = IV.Img2Vec(log_path=os.path.join(INTERMEDIATE_PATH,"failed_image_urls_{}.pkl".format(random.randint(10,1000))))
    df = df.progress_apply(img2vec.find_image_dense_representation,axis=1)
    img2vec.dump_failed_urls()
    return df

# 3.
class Proccessing:
    def __init__(self, ca_df=pd.DataFrame(), prod_df=pd.DataFrame(), grail_df=pd.DataFrame()):
        self.ca_df = ca_df
        self.prod_df = prod_df
        self.grail_df = grail_df
        self.prod_flag = False
        self.ca_flag = False
        self.grail_flag = False
        # self.file_name = file_name

    @staticmethod
    def Parse_Question_Text(question_info_object):
        try:
            question_info_object = question_info_object['en']
        except:
            pass
        try:
            qcontent = re.sub('&nbsp;', ' ', question_info_object).replace('&there4;', ' ')
            return qcontent
        except:
            return question_info_object

    @staticmethod
    def clean(text):
        text = str(text)
        if not text:
            return "No explanation_given"
        else:
            soup = BeautifulSoup(text, 'html.parser')
            soup.prettify()
            soup_text = soup.get_text()
            soup_text = soup_text.replace("\\\\", "\\")
            soup_text = soup_text.replace("\\n", " ")
            try:
                soup_text = LatexNodes2Text().latex_to_text(soup_text)
            except:
                print(soup_text)
                soup_text = soup_text.strip('\"').replace('\\\\', '\\').replace('\\"', '\"').replace('\\n',
                                                                                                              '\n').replace(
                    "\\r", "\r").replace("\\t", "\t")
                try:
                    soup_text = LatexNodes2Text().latex_to_text(soup_text)
                except:
                    pass

            soup_text = soup_text.strip('\"')
            soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            soup_text = re.sub(r"(^Q.\w+.:)", '', soup_text)
            soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            soup_text = re.sub(r"(^Ans\w+.)", '', soup_text)
            soup_text = soup_text.replace("\n", " ")
            soup_text = soup_text.replace("\r", " ")
            soup_text = soup_text.replace("  ", " ")
            soup_text = soup_text.replace("   ", " ")
            soup_text = re.sub(r'\n', "", soup_text)
            soup_text = re.sub(r'\r', "", soup_text)

        return soup_text

    @staticmethod
    def cleaning_content(qcontent):
        try:
            qcontent = re.sub('<[^>]*>', ' ', qcontent).replace('&nbsp;', ' ').replace('&there4;', ' ').replace('&#39;',
                                                                                                                '').replace(
                '<sup>', '').replace('</sup>', '').replace('<br>', '').replace('<br />', '')
        except Exception as e:
            print("Exception in cleaning content: ",e.__str__())
        return qcontent

    @staticmethod
    def get_symbols(mstring):
        try:
            mstring = str(mstring)
            list_symbols = []
            list_items = mstring.split()
            for item in list_items:
                if (len(item) == 1):
                    if ((re.match('[a-z]', item) == None) and (item != ' ') and (
                            item not in ['.', ',', '\'', ';', '?', '\u2061'])):
                        list_symbols.append(item)

            list_symbols = list(set(list_symbols))
            return list_symbols
        except:
            print(mstring)
            return []

    @staticmethod
    def clean_data(string, list_symbols):
        try:
            string = string.split(' ')
            unwanted = []
            for i in string:
                if (len(i) == 1):
                    if (ord(i) > 128 and i not in list_symbols):
                        unwanted.append(i)
            unwanted = list(unwanted)
            for i in unwanted:
                string.remove(i)
            string = ' '.join(string)
            return string
        except:
            return string

    @staticmethod
    def remove_unicode_content(qcontent):
        try:
            qcontent = re.sub('\u2061', '', qcontent)
            return qcontent
        except:
            return qcontent

    @staticmethod
    def remove_page_margin(content):
        if content.startswith("<!--"):
            return ""
        else:
            return content

    def process_dataframe(self, all_contents_new):        
        all_contents_new['question_info_clean'] = all_contents_new['question_text'].progress_apply(
            lambda x: self.Parse_Question_Text(x))

        all_contents_new.dropna(subset=['question_info_clean'], inplace=True)

        all_contents_new['question_info_clean'] = all_contents_new['question_info_clean'].progress_apply(lambda x: self.clean(x))

        all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean'].apply(
            lambda x: self.cleaning_content(x))

        all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(
            lambda x: self.remove_unicode_content(x))

        all_contents_new = all_contents_new.reset_index()

        all_contents_new['symbols'] = all_contents_new['question_info_clean_complete'].apply(
            lambda x: self.get_symbols(x))
        list_of_list = all_contents_new['symbols'].tolist()
        flat_list = [item for sublist in list_of_list for item in sublist]
        list_symbols = list(set(flat_list))

        all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(
            lambda x: self.clean_data(x, list_symbols))
        
        # --------- correct option ----------------
        all_contents_new['answer_info'] = all_contents_new['correct_answer_text'].apply(
            lambda x: self.Parse_Question_Text(x))

        all_contents_new['correct_option'] = all_contents_new['answer_info'].apply(lambda x: self.clean(x))

        all_contents_new['correct_option'] = all_contents_new['correct_option'].apply(
            lambda x: self.remove_page_margin(x))
        
        # --------- all options ----------------
        all_contents_new['answer_info'] = all_contents_new['whole_answer_body'].apply(
            lambda x: self.Parse_Question_Text(x))

        all_contents_new['whole_answer_body'] = all_contents_new['answer_info'].apply(lambda x: self.clean(x))

        all_contents_new['whole_answer_body'] = all_contents_new['whole_answer_body'].apply(
            lambda x: self.remove_page_margin(x))

        # all_contents_new_removed_dup = all_contents_new[~all_contents_new['question_code'].str.contains("DUP")]

        # all_contents_new_columns = all_contents_new_removed_dup[["question_code","subject","is_test","is_practice","created_at","exams","is_prev_year","question_info_clean_complete","correct_option","question_type","question_images","answer_images"]]

        all_contents_new["question_info_clean_complete"] = all_contents_new["question_info_clean_complete"].apply(
            lambda x: str(x))
        all_contents_new["correct_option"] = all_contents_new["correct_option"].apply(lambda x: str(x))
        all_contents_new["whole_answer_body"] = all_contents_new["whole_answer_body"].apply(lambda x: str(x))

        # all_contents_new["question_answer_clean"] = all_contents_new["question_info_clean_complete"] +" "+ all_contents_new["correct_option"]

        # all_contents_new_columns["is_test"].fillna(0, inplace=True)
        # all_contents_new_columns["is_practice"].fillna(0, inplace=True)

        return all_contents_new

    def start_processing(self):
        if self.ca_df.empty:
            print("emptyDF")

        else:
            content_dataframe = self.process_dataframe(self.ca_df)
            content_dataframe.to_json(r"new_content_data", orient='records',index=False)
            self.ca_flag = True

        if self.prod_df.empty:
            print("emptyDF")
        else:
            prod_dataframe = self.process_dataframe(self.prod_df)
            prod_dataframe.to_json(r"new_production_data", orient='records',index=False)
            self.prod_flag = True

        if self.grail_df.empty:
            print("emptyDF")
        else:            
            grail_dataframe = self.process_dataframe(self.grail_df)
            return grail_dataframe        

        return (self.ca_flag, self.prod_flag, self.grail_flag)

# 4. 
class Ingest_ES:
    def __init__(self, ca_flag, prod_flag, grail_flag, es_object, extracted_clean_data_json_dump, es_index):
        self.flag_dict = {"ca": ca_flag, "prod": prod_flag, "grail": grail_flag}
        self.json_file_name_dict = {"ca": "new_content_data", "prod": "new_production_data",
                                    "grail": extracted_clean_data_json_dump}
        self.index_dict = {"ca": "cqi-duplicate-contentadmin", "prod": "cqi-duplicate-production", "grail": es_index}
        self.keys = ["ca", "prod", "grail"]
        self.es = es_object

    def create_index_with_mapping(self, idx_name):
        mapping = mapping = {
            "mappings": {
                "properties": {
                    "question_code": {
                        "type": "text" # formerly "string"
                    },
                    "question": {
                        "type": "text"
                    },
                    "question_version": {
                        "type": "integer"
                    },
                    "question_code_with_version": {
                        "type": "text"
                    },
                    "question_id_with_version": {
                        "type": "text"
                    },
                    "subtype": {
                        "type": "text"
                    },
                    "question_urls": {
                        "type": "text"
                    },
                    "answer_urls": {
                        "type": "text"
                    },
                    "is_image_present": {
                        "type": "boolean"
                    },
                    "question_image_dense": {
                        "type": "dense_vector",
                        "dims": 512
                    },
                    "answer_image_dense": {
                        "type": "dense_vector",
                        "dims": 512
                    },
                    "is_question_image_dense_present": {
                        "type": "boolean"
                    },
                    "is_answer_image_dense_present": {
                        "type": "boolean"
                    },
                    "is_question_text_present": {
                        "type": "boolean"
                    },
                    "is_answer_text_present": {
                        "type": "boolean"
                    },
                    "whole_answer_body": {
                        "type": "text"
                    },
                    "correct_option": {
                        "type": "text"
                    }
                }
            }
        }
        resp = self.es.indices.create(idx_name,body=mapping,ignore=400)
        print(resp)
    
    def ingest_in_es(self, db_key):
        if self.flag_dict[db_key]:
            with open(self.json_file_name_dict[db_key]) as f:
                data = json.load(f)
                
            self.create_index_with_mapping(self.index_dict[db_key])
            
            k = ({
                '_index': self.index_dict[db_key], '_id': quest_data["id"], 
                "question_id_with_version":quest_data["question_id_with_version"], "question_code_with_version":quest_data["question_code_with_version"],
                'question_code': quest_data['question_code'], "subtype": quest_data['subtype'],
                'question': quest_data['question_info_clean_complete'],
                "is_answer_text_present": quest_data["is_answer_text_present"], "whole_answer_body": quest_data["whole_answer_body"], 
                "correct_option": quest_data["correct_option"],
                "question_version": quest_data["_version"],
                "question_urls": quest_data["question_urls"], "answer_urls": quest_data["answer_urls"],
                "is_image_present": quest_data["is_image_present"], 
                "question_image_dense": quest_data["question_image_dense"],
                "answer_image_dense": quest_data["answer_image_dense"],
                "is_question_image_dense_present": quest_data["is_question_image_dense_present"],
                "is_answer_image_dense_present": quest_data["is_answer_image_dense_present"],
                "is_question_text_present": quest_data["is_question_text_present"]
                 }
                 for quest_data in tqdm(data,desc="ES INGEST"))

            helpers.bulk(self.es, k, request_timeout=40)
    
    def data_ingestion(self):
        for db_key in self.keys:
            self.ingest_in_es(db_key)

# 8.
def get_answer(df, correct_ans_lookup):
    original_question_code = df["original_question_code"]
    duplicate_question_code = df["duplicate_question_code"]
    try:
        org_ans = correct_ans_lookup.get(original_question_code,"")
    except:
        print("exception: ", df["question_code"])
        org_ans = ""
    try:
        dup_ans = correct_ans_lookup.get(duplicate_question_code,"")
    except:
        print("exception: ", df["question_code"])
        dup_ans = ""
    return (org_ans, dup_ans)

# 9.
def laven_ans_ratio(df):
    correct_option_x = df["correct_option_x"]
    correct_option_y = df["correct_option_y"]
    if (correct_option_x == "") or (correct_option_y == ""):
        return 1
    return Levenshtein.ratio(correct_option_x, correct_option_y)

def make_answer_lower(text):
    try:
        if math.isnan(text):
            return ""
    except:
        return text.lower()

def find_representatives(df,level):
    field_name = "is_{}_level_rep".format(level)
    representative = df.groupby(["cluster_id",level],as_index=False).first()
    df.loc[df["index"].isin(representative["index"].tolist()),field_name] = True
    df[field_name].fillna(False,inplace=True)
    return df
