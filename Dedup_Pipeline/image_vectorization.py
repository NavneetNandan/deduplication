# 3.0
import torch
import torch.nn as nn
import torchvision.models as models
import torchvision.transforms as transforms
import numpy as np
# import cv2
import tqdm

from PIL import Image
import os
from config import INTERMEDIATE_PATH
import io
from urllib.request import urlopen
import pickle

class Img2Vec():

    def __init__(self, cuda=False, model='resnet-18', layer='default', layer_output_size=512, log_path=None):
        """ Img2Vec
        :param cuda: If set to True, will run forward pass on GPU
        :param model: String name of requested model
        :param layer: String or Int depending on model. layer = 'layer_name' or int   
                      which layer of the model to extract the output from.   default: 'avgpool'
                      See more docs: https://github.com/christiansafka/img2vec.git
        :param layer_output_size: Int depicting the output size of the requested layer
        """
        self.device = torch.device("cuda" if cuda else "cpu")
        self.layer_output_size = layer_output_size
        self.model_name = model
        
        self.model, self.extraction_layer = self._get_model_and_layer(model, layer)

        self.model = self.model.to(self.device)

        self.model.eval()

        self.scaler = transforms.Resize((224, 224)) # Scale()
        self.normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                              std=[0.229, 0.224, 0.225])
        self.to_tensor = transforms.ToTensor()

        self.log_path = log_path
        self.failed_urls = []

    def get_vec(self, img, tensor=False):
        """ Get vector embedding from PIL image
        :param img: PIL Image or list of PIL Images
        :param tensor: If True, get_vec will return a FloatTensor instead of Numpy array
        :returns: Numpy ndarray
        """
        # convert_img_channel = lambda x: cv2.cvtColor(x, cv2.COLOR_BGRA2BGR) if len(x.shape) > 2 and x.shape[2] == 4 else (cv2.cvtColor(x, cv2.COLOR_GRAY2BGR) if len(x.shape)==2 else x)
        # convert_img_to_BGR = lambda x: Image.fromarray(convert_img_channel(np.array(x)))
        
        if type(img) == list:
            # a = [self.normalize(self.to_tensor(self.scaler(convert_img_to_BGR(im)))) for im in img]
            a = [self.normalize(self.to_tensor(self.scaler(im))) for im in img]
            
            images = torch.stack(a).to(self.device) 
            if self.model_name == 'alexnet':
                my_embedding = torch.zeros(len(img), self.layer_output_size)
            else:
                my_embedding = torch.zeros(len(img), self.layer_output_size, 1, 1)

            def copy_data(m, i, o):
                my_embedding.copy_(o.data)

            h = self.extraction_layer.register_forward_hook(copy_data)
            h_x = self.model(images)
            h.remove()

            if tensor:
                return my_embedding
            else:
                if self.model_name == 'alexnet':
                    return my_embedding.numpy()[:, :]
                else:
                    print(my_embedding.numpy()[:, :, 0, 0].shape)
                    return my_embedding.numpy()[:, :, 0, 0]
        else:
            
            # image = self.normalize(self.to_tensor(self.scaler(convert_img_to_BGR(img)))).unsqueeze(0).to(self.device)
            image = self.normalize(self.to_tensor(self.scaler(img))).unsqueeze(0).to(self.device)

            if self.model_name == 'alexnet':
                my_embedding = torch.zeros(1, self.layer_output_size)
            else:
                my_embedding = torch.zeros(1, self.layer_output_size, 1, 1)

            def copy_data(m, i, o):
                my_embedding.copy_(o.data)

            h = self.extraction_layer.register_forward_hook(copy_data)
            h_x = self.model(image)
            h.remove()

            if tensor:
                return my_embedding
            else:
                if self.model_name == 'alexnet':
                    return my_embedding.numpy()[0, :]
                else:
                    return my_embedding.numpy()[0, :, 0, 0]

    def _get_model_and_layer(self, model_name, layer):
        """ Internal method for getting layer from model
        :param model_name: model name such as 'resnet-18'
        :param layer: layer as a string for resnet-18 or int for alexnet
        :returns: pytorch model, selected layer
        """
        if model_name == 'resnet-18':
            model = models.resnet18(pretrained=True)
            if layer == 'default':
                layer = model._modules.get('avgpool')
                self.layer_output_size = 512
            else:
                layer = model._modules.get(layer)

            return model, layer

        elif model_name == 'alexnet':
            model = models.alexnet(pretrained=True)
            if layer == 'default':
                layer = model.classifier[-2]
                self.layer_output_size = 4096
            else:
                layer = model.classifier[-layer]

            return model, layer

        else:
            raise KeyError('Model %s was not found' % model_name)

    def preprocess_image(self, img):
        try:
            desired_size = 224
            old_size = img.size  # old_size[0] is in (width, height) format

            ratio = float(desired_size)/max(old_size)
            new_size = tuple([int(x*ratio) for x in old_size])

            img = img.resize(new_size, Image.ANTIALIAS)

            new_img = Image.new("RGB", (desired_size, desired_size), (255,255,255))
            new_img.paste(img, ((desired_size-new_size[0])//2,
                                        (desired_size-new_size[1])//2))
            return new_img
        except Exception as e:
            raise(e.__str__())

    def download_and_preprocess_images(self, qcode,ques,ans):
        question_images, answer_images = [], []
        
        for url in ques:
            try:
                response = urlopen(url,timeout=2)
                img = response.read()
                img = Image.open(io.BytesIO(img)).convert("RGBA")
                question_images.append(self.preprocess_image(img))
            except Exception as e:
                print(f"Exception in Image Processing({qcode}, {url}): {e.__str__()}")
                self.failed_urls.append([qcode,url,e.__str__()])
        for url in ans:
            try:
                response = urlopen(url,timeout=2)
                img = response.read()
                img = Image.open(io.BytesIO(img)).convert("RGBA")
                answer_images.append(self.preprocess_image(img))
            except Exception as e:
                print(f"Exception in Image Processing({qcode}, {url}): {e.__str__()}")
                self.failed_urls.append([qcode,url,e.__str__()])
        return question_images, answer_images

    def find_image_dense_representation(self, row):
        question_code = row["question_code"]
        if row["is_image_present"]:
            question_images, answer_images = row["question_urls"], row["answer_urls"]
            question_images, answer_images = self.download_and_preprocess_images(question_code, question_images, answer_images)
            # question_images, answer_images = question_images[:2], answer_images[:2]
            row["question_image_dense"] = np.array([self.get_vec(img) for img in question_images]).mean(axis=0,dtype="float16").tolist()
            row["answer_image_dense"] = np.array([self.get_vec(img) for img in answer_images]).mean(axis=0,dtype="float16").tolist()
        else:
            row["question_image_dense"] = None
            row["answer_image_dense"] = None
        return row

    def dump_failed_urls(self):
        with open(self.log_path,"wb") as file:
            pickle.dump(self.failed_urls,file)
    
    