from driver import Dedup
import datetime
from logging import Logger
import time

if __name__ == "__main__":
    goal = "K12"
    status = ["Approved","Published"]
    obj = Dedup(goal,status)
    logger = Logger(name="dedup_logging")
    start = time.time()
    print("dedup pipeline for {} goal and {} status is triggered at {}...".format(goal,status,datetime.datetime.now()))
    try:
        obj.driver()
        print("Dedup completed! It took {} seconds".format(time.time()-start))
    except Exception as e:
        logger.exception("driver crashed, after executing for {} seconds".format(time.time()-start))
