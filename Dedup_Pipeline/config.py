appName = "CQI-Deduplication"
version = 1.0

DATABASES = {
    "mongo": {"URL": "mongodb://ro_dsl:EHJpUwVO2vgMuk@10.141.11.77:27017/?authSource=contentgrail&readPreference=secondaryPreferred&appname=MongoDB%20Compass&ssl=false"},
    "ES": {"URL": "10.144.131.98"}
}

GOALS = {
    "all": [
        "1st CBSE", "2nd CBSE", "3rd CBSE", "4th CBSE", "5th CBSE", "6th CBSE", "7th CBSE", "8th CBSE", "9th CBSE", "10th CBSE", "11th CBSE", "12th CBSE",
        "Banking", "Banking and Clerical", "Teaching",
        "Engineering", "Medical"
    ],
    "K12": ["1st CBSE", "2nd CBSE", "3rd CBSE", "4th CBSE", "5th CBSE", "6th CBSE", "7th CBSE", "8th CBSE", "9th CBSE", "10th CBSE", "11th CBSE", "12th CBSE"],
    "prepg": ["Banking", "Banking and Clerical", "Teaching"],
    "preug": ["Engineering", "Medical"]
}

FORMAT_REFERENCES = {
    "all": [
        '5ec5867a0c88fe5860961943', '5ec586730c88fe5860961938', '5ec586760c88fe586096193c', '5e9b564928e1174ef2785843', '5ec586740c88fe586096193a',
        "5ec586890c88fe5860961958", "5f17102be61885046e5d9780", "5f171296e61885046e5da6a9", "5f17177ee61885046e5db8d0", "5ec5867f0c88fe5860961948"
    ],
    "K12": ['5ec5867a0c88fe5860961943', '5ec586730c88fe5860961938',
            '5ec586760c88fe586096193c', '5e9b564928e1174ef2785843',
            '5ec586740c88fe586096193a'],
    "prepg": [],
    "preug": ["5ec586890c88fe5860961958", "5f17102be61885046e5d9780", "5f171296e61885046e5da6a9", "5f17177ee61885046e5db8d0", "5ec5867f0c88fe5860961948"]
}

INTERMEDIATE_PATH = "/home/devops/learning_outcome/Image_Dedup_Training/data_dedup_modularized"