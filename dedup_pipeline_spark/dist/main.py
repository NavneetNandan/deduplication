import time
import os
import sys
from pyspark.sql import SparkSession

if os.path.exists('utilities.zip'):
    sys.path.insert(0, 'utilities.zip')
else:
    sys.path.insert(0, './utilities')

from utilities.driver import Dedup

if __name__ == '__main__':
    spark_session = (SparkSession
    .builder
    .appName('CQI Deduplication Batch Pipeline')
    .getOrCreate())
    

    start = time.time()
    
    status = ['Draft', 'Pending Approval', 'Published', 'Approved',
       'UAT Rejected', 'Published UAT Accepted', 'UAT Accepted', 'draft']
    status_filters_clustering = ['Approved','Published','UAT Accepted','Published UAT Accepted']

    # status = ['UAT Accepted']
    # status_filters_clustering = ['UAT Accepted']

    obj = Dedup(spark_session, status, status_filters_clustering)
    obj.driver()
    
    end = time.time()

    print("*"*15,"Execution of job took {} seconds".format(end-start),"*"*15)