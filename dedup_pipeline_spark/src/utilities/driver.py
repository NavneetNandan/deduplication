import subprocess

from pyspark.sql import Row
import pyspark.sql.functions as F
import pyspark.sql.types as t
from pyspark.sql import Row
from pyspark import StorageLevel
import graphframes as GF
from pyspark.sql.window import Window

import warnings
warnings.filterwarnings("ignore")

from elasticsearch import Elasticsearch, helpers
import os
from tqdm import tqdm

from utilities.config import PATHS, DATABASES, VERSION

import utilities.utils as utils
import utilities.image_vectorization as IV
import utilities.select_candidates as sc
import utilities.spark_variables as sv

class Dedup():
    def __init__(self, spark, status_filters, status_filters_clustering):
        self.directory = PATHS.get("base")

        self.spark = spark
        self.mongo_config = DATABASES.get("mongo")
        self.es_config = DATABASES.get("ES")
        self.es_nodes = DATABASES.get("ES_CLUSTER_NODES")
        self.es = Elasticsearch(self.es_nodes,timeout=30, max_retries=5, retry_on_timeout=True)

        self.status_filters = status_filters
        self.status_filters_clustering = status_filters_clustering
        self.mongo_config["spark.mongodb.input.collection"] = "learning_objects"
        self.lo_coll = self.mongo_config.copy()

        # self.grade = grade
        self.all_data_dump = os.path.join(self.directory,VERSION,'cg-processed-questions.parquet')
        self.candidate_pairs_dump = os.path.join(self.directory,VERSION,'cg-candidate_pairs.parquet')
        self.candidate_pairs_exact = os.path.join(self.directory,VERSION,'cg-candidate_pairs_exact.parquet')
        self.cluster_exact_file = os.path.join(self.directory,VERSION,'cg_exact_duplicates_clusters.parquet')
        # self.cluster_similar_file = os.path.join(self.directory,VERSION,'cg_similar_duplicates_clusters.parquet')
        # self.not_present_in_CB = os.path.join(self.directory,VERSION,'missing_cb.parquet')
        # self.not_present_in_LM = os.path.join(self.directory,VERSION,'missing_lm.parquet')
        self.joined_q_level_df_with_cluster = os.path.join(self.directory,VERSION,'joined_q_level_df_with_cluster.parquet')
        self.checkpoint_dir = os.path.join(self.directory,VERSION,"checkpoint/")
        
        self.es_index = "cg-duplication-questions" + "_" + str.lower(VERSION)
        # self.es_index = "cg-duplication-questions_batch_v3_reindexed" 
        # self.final_index_name = "cg-results_dedup" + "_" + str.lower(VERSION)
        self.final_index_name = "cg-results_dedup" + "_" + str.lower("v3_latest")

    def driver(self):
        # 1.   
        union_df = utils.get_learning_objects(self.spark, self.lo_coll, 'Question', status_filters = self.status_filters)

        # 3 Preprocess text data
        input_process = utils.Proccessing(grail_df=union_df)
        grail_dataframe = input_process.start_processing()
        
        # 4.0 Compute meta fields
        grail_dataframe = grail_dataframe.withColumn(
            "question_id_with_version",
            F.concat(
                F.col("id").cast("string"),
                F.lit("__"),
                F.col("_version").cast("string")
            )
        )
        grail_dataframe = grail_dataframe.withColumn(
            "question_code_with_version",
            F.concat(
                F.col("question_code"),
                F.lit("__"),
                F.col("_version").cast("string")
            )
        )

        # 3.0 Fetch Dense Representation
        grail_dataframe = grail_dataframe.withColumn("dense_vectors",IV.find_image_dense_representation(
                F.col("question_code"),F.col("question_urls"),F.col("answer_urls")
            )
        ).persist(StorageLevel.MEMORY_AND_DISK)
                
        grail_dataframe = grail_dataframe.withColumn("avg_hygiene_score",F.lit(-1.0))

        grail_dataframe = grail_dataframe.select(
            "id", F.col("_version").alias("question_version"), "status", "question_code", "subtype",
            "learning_maps", "avg_hygiene_score",
            "question_id_with_version", "question_code_with_version",
            "is_question_text_present", F.col("question_info_clean_complete").alias("question"),
            "is_answer_text_present", "whole_answer_body","correct_options",
            'question_urls', 'answer_urls', 
            "dense_vectors.*"
        )

        grail_dataframe = grail_dataframe.withColumn("timestamp",F.current_timestamp())
        

        # 4.1 Store Data on HDFS and ES
        ### 1. HDFS/WASB
        grail_dataframe.write.mode("overwrite").parquet(self.all_data_dump)
        union_df.unpersist()

        ### 2. ES
        print("Count of Questions:",grail_dataframe.count())
        utils.driver_ingest_on_es(self.es, self.es_index, grail_dataframe, self.es_config)
        
        # utils.fetch_questions_index_and_store_on_HDFS(self.spark,self.es,self.es_index,self.all_data_dump) # TEMP
        # grail_dataframe = self.spark.read.parquet(self.all_data_dump).persist() # TEMP
        # grail_dataframe = grail_dataframe.dropDuplicates(subset=["id"]) # TEMP
        # print("Count of unique question ids:") # TEMP
        # grail_dataframe.select(F.countDistinct("id")).show() # TEMP
        # print("Count of Questions:",grail_dataframe.count()) # TEMP
        
        grail_dataframe = grail_dataframe.select(sorted(grail_dataframe.columns))

        grail_dataframe_cols = grail_dataframe.columns
        try:
            qid_idx = grail_dataframe_cols.index("id")
        except:
            qid_idx = 1
        try:
            qcode_idx = grail_dataframe_cols.index("question_code")
        except:
            qcode_idx = 7

        # 5. Find Candidates
        qcode_to_qid_dict_broadcast = self.spark.sparkContext.broadcast(grail_dataframe.rdd.map(lambda x: (x[qcode_idx],x[qid_idx])).collectAsMap())
        print("qcode_to_qid_dict_broadcast:",list(qcode_to_qid_dict_broadcast.value.items())[:5])
        
        grail_dataframe = grail_dataframe.filter(grail_dataframe.status.isin(self.status_filters_clustering))

        acc = self.spark.sparkContext.accumulator(list(), sv.ListAccumulator())
        acc_value = sc.process_data_select_candidates(grail_dataframe,acc,self.es_nodes,self.es_index)
        
        grail_dataframe.unpersist()
        
        # 6. Prepare candidate dataframe, calculate ratios and decide similarity
        candidate_df = self.spark.createDataFrame(acc_value).persist(StorageLevel.MEMORY_AND_DISK)
        print("Count of Candidates:",candidate_df.count())
        candidate_df.write.mode("overwrite").parquet(self.candidate_pairs_dump)
        del acc_value
        
        # candidate_df = self.spark.read.parquet(self.candidate_pairs_dump).persist() # TEMP write.mode("overwrite").parquet(self.candidate_pairs_dump)

        ## Compute Clean Laven Scores
        candidate_df = candidate_df.withColumn("clean_laven_ratios",sc.clean_laven_ratios(F.col("duplicate_question_text"),F.col("original_question_text")))
        candidate_df = candidate_df.withColumn("answer_clean_laven_ratios",sc.clean_laven_ratios(F.col("duplicate_answer_text"),F.col("original_answer_text")))

        ## Find Similarity Flags
        candidate_df = candidate_df.withColumn("similarity_output",sc.check_duplicate_exact(
            F.col("Laven_ratios"),F.col("original_question_text"),F.col("duplicate_question_text"),
            F.col("clean_laven_ratios"),F.col("question_image_similarity"),F.col("answer_laven_ratios"),
            F.col("original_answer_text"),F.col("duplicate_answer_text"),F.col("answer_clean_laven_ratios"),
            F.col("answer_image_similarity")
        ))

        # 7. Add Question IDs for Original and Duplicate Questions
        @F.udf(t.IntegerType())
        def return_question_id(question_code):
            id = qcode_to_qid_dict_broadcast.value.get(question_code,None)
            if id is None:
                print("*"*10,"question code missing:",question_code,"*"*10)
            return id

        candidate_df = candidate_df.withColumn("original_id",return_question_id(F.col("original_question_code")))
        candidate_df = candidate_df.withColumn("duplicate_id",return_question_id(F.col("duplicate_question_code")))
        
        candidate_df = candidate_df.dropna(subset=["original_id","duplicate_id"])

        candidate_df_exact = candidate_df.filter(candidate_df.similarity_output.pred_label == 1).persist(StorageLevel.MEMORY_AND_DISK) 
        candidate_df_exact = candidate_df_exact.withColumn(
                "original_id",
                F.col("original_id").cast("int")
            ).withColumn(
                "duplicate_id",
                F.col("duplicate_id").cast("int")
            ).select("original_id","duplicate_id","similarity_output.similarity_score")

        print("candidate_df_exact schema:",candidate_df_exact.printSchema())
        print("candidate_df_exact columns:",candidate_df_exact.columns)
        candidate_df_exact.write.mode("overwrite").parquet(self.candidate_pairs_exact)
    
        candidate_df.unpersist()
        qcode_to_qid_dict_broadcast.unpersist()
        del qcode_to_qid_dict_broadcast

        # 8. Clustering Exact and Similar Questions
        verticesDf_exact= candidate_df_exact.select("original_id").union(candidate_df_exact.select("duplicate_id")).distinct().withColumnRenamed('original_id', 'id')
        graph_exact = GF.GraphFrame(
            verticesDf_exact,
            candidate_df_exact.select(
                    F.col("original_id").alias("src"),
                    F.col("duplicate_id").alias("dst")
                )
            )

        try:
            _ = subprocess.call(["hdfs","dfs","-rm","-f","-r",self.checkpoint_dir]) # remove checkpoint directory
        except Exception as e:
            print("Exception in subprocess call:",e.__str__())
            
        self.spark.sparkContext.setCheckpointDir(self.checkpoint_dir)

        cc_exact = graph_exact.connectedComponents().persist(StorageLevel.MEMORY_AND_DISK)

        # candidate_df_exact.unpersist()

        # 9. Store Clusters on HDFS
        cc_exact.write.mode("overwrite").parquet(self.cluster_exact_file)

        # cc_exact = self.spark.read.parquet(self.cluster_exact_file) #.persist(storageLevel=StorageLevel.MEMORY_AND_DISK) # TEMP

        # ------------------------------------ Post Processing ---------------------------------------------
        
        # 1. Fetch All Questions Meta From "Question's Index"
        
        es_read_config = self.es_config.copy()
        es_read_config["es.resource"] = "{}".format(self.es_index) # /_doc
        es_read_config["es.read.field.as.array.include"] = "learning_maps"
        es_read_config["es.read.field.include"] = "id,question_code,question,correct_options,status,question_version,question_id_with_version,question_code_with_version,learning_maps,is_question_image_dense_present,is_answer_image_dense_present,is_question_text_present,is_answer_text_present"
        es_read_config["es.field.read.empty.as.null"] = "no"
        
        questions_df = self.spark.read.format(
            "org.elasticsearch.spark.sql"
        ).options(**es_read_config).load().persist(storageLevel=StorageLevel.MEMORY_AND_DISK)
        
        questions_df_cols = questions_df.columns
        try:
            qid_idx = questions_df_cols.index("id")
        except:
            qid_idx = 1
        try:
            qversion_idx = questions_df_cols.index("question_version")
        except:
            qversion_idx = 11
        try:
            qtext_idx = questions_df_cols.index("question")
        except:
            qtext_idx = 7
        try:
            atext_idx = questions_df_cols.index("correct_options")
        except:
            atext_idx = 0
        try:
            qImgPresent_idx = questions_df_cols.index("is_question_image_dense_present")
        except:
            qImgPresent_idx = 4
        try:
            aImgPresent_idx = questions_df_cols.index("is_answer_image_dense_present")
        except:
            aImgPresent_idx = 2
        try:
            qTxtPresent_idx = questions_df_cols.index("is_question_text_present")
        except:
            qTxtPresent_idx = 5
        try:
            aTxtPresent_idx = questions_df_cols.index("is_answer_text_present")
        except:
            aTxtPresent_idx = 3

        qid_to_qinfo_broadcast = self.spark.sparkContext.broadcast(
            questions_df.rdd.map(
                lambda x: (
                    x[qid_idx],
                    (x[qversion_idx], x[qtext_idx], x[atext_idx], x[qImgPresent_idx], x[aImgPresent_idx], x[qTxtPresent_idx], x[aTxtPresent_idx])
                    )
                ).collectAsMap()
            )
        questions_df_rel = questions_df.filter(questions_df.status.isin(self.status_filters_clustering)) #.persist(storageLevel=StorageLevel.MEMORY_AND_DISK)

        # 2. Prepare all Exact Clusters (or Extend Exact Cluster DF with non-clustered Questions)
        # exact_ques = cc_exact.select("id").rdd.flatMap(lambda x: x).collect()
        # exact_ques = list(set(exact_ques))
        # all_ques = questions_df_rel.select("id").rdd.flatMap(lambda x: x).collect()
        # non_clustered_ques = list(set(all_ques)-set(exact_ques))

        # questions_df.unpersist()
        
        # rdd1 = self.spark.sparkContext.parallelize(non_clustered_ques)
        # row_rdd = rdd1.map(lambda x: Row(x))
        # non_clustered_ques = self.spark.createDataFrame(row_rdd,["id"])

        # non_clustered_ques = non_clustered_ques.withColumn("component",F.col("id"))
        # non_clustered_ques = non_clustered_ques.select("id",F.col("component").cast("long"))

        # cl_df = cc_exact.union(non_clustered_ques.select(cc_exact.columns)) #.persist(storageLevel=StorageLevel.MEMORY_AND_DISK) # id, component (cluster id)

        cl_df = questions_df_rel.join(cc_exact, on= "id", how="full_outer").select( # .select(F.col("id"))
            F.col("id"), F.when(F.col("component").isNull(),F.col("id")).otherwise(F.col("component")).alias("cluster_id"),
            "question_code","question_version","question_id_with_version","question_code_with_version","learning_maps","is_question_image_dense_present",
            "is_answer_image_dense_present"
        ).repartition("cluster_id")

        # 3. GroupBy on Cluster DF to get exact clusters (cluster_id, question_id/set)
        exact_clusters_df = cl_df.select("cluster_id","id").groupby("cluster_id").agg(F.collect_set("id").alias("question_id")) #.select(F.col("component").alias("cluster_id"),"question_id") #.persist(storageLevel=StorageLevel.MEMORY_AND_DISK)

        # 4. Find Exact Cluster with version
        # similar_clusters = cc_similar.groupby("component").agg(F.collect_set("id").alias("question_id")).select("question_id").rdd.flatMap(lambda x: x).collect()
        # similar_clusters_broadcast = self.spark.sparkContext.broadcast(list(map(set,similar_clusters)))
        
        exact_clusters_df = exact_clusters_df.withColumn(
            "exact_questions_with_version",utils.curried_find_version(
                qid_to_qinfo_broadcast
            )(
                F.col("question_id")
            )
        ).select("cluster_id",F.col("question_id").alias("exact_questions"),"exact_questions_with_version")
        
        # 5. Attach Cluster Info with Individual Questions and Add Cluster info with Meta df
        # cl_df = cl_df.select("id",F.col("component").alias("cluster_id"))
        joined_q_level_df_with_cluster = cl_df.filter(cl_df.question_code.isNotNull()).join(exact_clusters_df,"cluster_id") # los_with_cluster_info 

        # joined_q_level_df_with_cluster = los_with_cluster_info.join(questions_df_rel,"id","left")

        joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.withColumn("size_exact_cluster",F.size("exact_questions"))
        # joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.withColumn("size_similar_cluster",F.size("similar_questions"))

        # 6. Store on ES
        joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.select(
            F.col('id').alias("question_id"), 'learning_maps','cluster_id', 'exact_questions', 'exact_questions_with_version', 'question_code', 
            'question_version', 'is_question_image_dense_present', 'is_answer_image_dense_present', 'size_exact_cluster',
            'question_id_with_version', 'question_code_with_version'
        )
        
        # joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.filter(joined_q_level_df_with_cluster.question_id.isin(all_ques))
        # joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.dropDuplicates(["question_id"])

        ## Add Similarity Scores
        # candidate_df_exact = self.spark.read.parquet(self.candidate_pairs_exact) # TEMP
        candidate_df_exact = candidate_df_exact.union(
            candidate_df_exact.select("duplicate_id","original_id","similarity_score")
        ).repartition("original_id") #.dropDuplicates(subset=["original_id","duplicate_id"])

        candidate_df_exact = candidate_df_exact.groupby(
            "original_id"
            ).agg(
                F.collect_list("duplicate_id").alias("exact_duplicates_with_scores"),
                F.collect_list("similarity_score").alias("similarity_scores")
            ).select(F.col("original_id").alias("question_id"),"exact_duplicates_with_scores","similarity_scores") #.persist(storageLevel=StorageLevel.MEMORY_AND_DISK)
        
        joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.join(candidate_df_exact,"question_id","left")
        
        joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.dropDuplicates(["question_id"])

        joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.withColumn(
            "all_similarity_scores", utils.populate_similarity_scores_for_all_duplicates(
                qid_to_qinfo_broadcast
            )(
                F.col("question_id"), F.col("exact_questions"), F.col("exact_duplicates_with_scores"), F.col("similarity_scores")
            )
        )

        joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.drop("exact_duplicates_with_scores","similarity_scores")

        ## Add Timestamp
        joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.withColumn("timestamp",F.current_timestamp())

        ## Write on ES
        es_write_config = self.es_config.copy()
        es_write_config["es.resource"] = "{}/_doc".format(self.final_index_name)
        es_write_config["es.read.field.as.array.include"] = 'exact_questions,exact_questions_with_version,learning_maps,exact_duplicates_with_scores,similarity_scores,all_similarity_scores'
        
        joined_q_level_df_with_cluster.write.format(
            "org.elasticsearch.spark.sql"
        ).options(**es_write_config).mode("overwrite").save()


        try:
            _ = subprocess.call(["hdfs","dfs","-rm","-f","-r",self.checkpoint_dir]) # remove checkpoint directory
            print("removed checkpoint dir:",self.checkpoint_dir,"\n","now stopping the spark session...")
        except Exception as e:
            print("Exception in subprocess call:",e.__str__())

        self.spark.stop()