from flask import Blueprint, jsonify, request

from modules.utils import fetch_dedup_results, find_duplicates
from config.functional_config import GOALS

cqi_dedup = Blueprint("dsl_cqi",__name__)


@cqi_dedup.route("/candidates", methods = ['GET'])
def get_duplicates():
    goal = request.args.get("goal",None)
    question_id = request.args.get("question_id",None)
    question_code = request.args.get("question_code",None)

    if not goal or goal not in GOALS:
        return jsonify({"message":"Either 'goal' is missing or it is invalid"}), 422
    if not question_code and not question_id:
        return jsonify({"message":"Either 'question_id' or 'question_code' is required"}), 422
    else:
        field_name = ""
        value_dict = {"question_id":int(question_id), "question_code":question_code}
        if question_id:
            field_name = "question_id"
            hits,response = fetch_dedup_results(int(question_id),field_name,goal)
        else:
            field_name = "question_code"
            hits,response = fetch_dedup_results(question_code,field_name,goal)
        
        if hits:
            return jsonify(response), 200
        else:
            # return {"message":"no match found"}
            value = value_dict[field_name]
            name = field_name if field_name=="question_code" else "id"
            response = find_duplicates(value, name, goal)
            return jsonify(response), 200