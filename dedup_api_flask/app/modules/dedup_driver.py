import modules.dedup_utils as utils
from config.functional_config import K12_INDEX_DATA,PREPG_INDEX_DATA,PREUG_INDEX_DATA
import pandas as pd
import numpy as np
from tqdm import tqdm
tqdm.pandas()

class Dedup():
    def __init__(self, es):
        self.es = es
        self.img2vec = utils.Img2Vec()
        self.index_names = {"K12":K12_INDEX_DATA, "prepg":PREPG_INDEX_DATA, "preug":PREUG_INDEX_DATA}

    def dedup_driver(self, df, goal):
        df_extract = df[["id", "_version", "question_code", "subtype", "content"]]
        df_extract["extracted_field"] = df_extract["content"].progress_apply(
            utils.extract_content_field)

        df_extract["question_text"] = df_extract["extracted_field"].progress_apply(
            lambda x: x[0])

        df_extract["correct_answer_text"] = df_extract["extracted_field"].progress_apply(
            lambda x: x[1])

        df_extract["whole_answer_body"] = df_extract["extracted_field"].progress_apply(
            lambda x: x[2])

        # 2.1 Fetch Image URLs
        df_extract["question_urls"] = df_extract["question_text"].progress_apply(
            utils.fetch_urls)
        df_extract["answer_urls"] = df_extract["correct_answer_text"].progress_apply(
            utils.fetch_urls)
        df_extract["is_image_present"] = df_extract.progress_apply(lambda x: True if sum(
            [len(x["question_urls"]), len(x["answer_urls"])]) > 0 else False, axis=1)

        # 3.0 Fetch Dense Representation
        df_extract = df_extract.progress_apply(
            self.img2vec.find_image_dense_representation, axis=1)

        # print("df_extract with image dense vecs: \n", df_extract.head())

        # 3.
        input_process = utils.Proccessing(grail_df=df_extract)
        grail_dataframe = input_process.start_processing()

        grail_dataframe = grail_dataframe.where(
            pd.notnull(grail_dataframe), None)
        grail_dataframe["is_question_image_dense_present"] = grail_dataframe["question_image_dense"].progress_apply(
            lambda x: False if x is None else True)
        grail_dataframe["is_answer_image_dense_present"] = grail_dataframe["answer_image_dense"].progress_apply(
            lambda x: False if x is None else True)

        grail_dataframe["question_image_dense"] = grail_dataframe.progress_apply(
            lambda x: np.zeros((512,), dtype="int8") if x["is_question_image_dense_present"] == False else x["question_image_dense"],
            axis=1)
        grail_dataframe["answer_image_dense"] = grail_dataframe.progress_apply(
            lambda x: np.zeros((512,), dtype="int8") if x["is_answer_image_dense_present"] == False else x["answer_image_dense"],
            axis=1)

        grail_dataframe["is_question_text_present"] = grail_dataframe.question_info_clean_complete.progress_apply(
            lambda x: True if len(x) > 0 else False)

        
        grail_dataframe_2 = grail_dataframe[
            [
                "question_code", "is_question_text_present", "question_info_clean_complete",
                "is_question_image_dense_present", "question_image_dense",
                "is_answer_image_dense_present", "answer_image_dense"
            ]
        ]

        # 5. Text , Text + IMG and Only IMG sections
        list_production = grail_dataframe_2.values.tolist()
        del grail_dataframe_2

        print("len of grail_dataframe_2: ", len(list_production), "\n", list_production)

        item = list_production[0]
        
        # es, es_index, question_code, is_question_text_present, question_text, 
        #    is_question_image_dense_present, question_image_dense,
        #    is_answer_image_dense_present, answer_image_dense,
        #    expected_output, not_es_search_codes
        expected_output = []
        not_es_search_codes = []

        expected_output = utils.search(
            self.es, self.index_names.get(goal,None),
            item[0],item[1],item[2],item[3],item[4],item[5],item[6],
            expected_output, not_es_search_codes
            )

        print("expected_output list: ", len(expected_output))

        candidate_df = pd.DataFrame(expected_output,
                                    columns=['original_question_code', 'duplicate_question_code', "Laven_ratios",
                                            "original_question_text", "duplicate_question_text", "image_field",
                                            "original_dense_vector", "duplicate_dense_vector"
                                            ])

        candidate_df_text = candidate_df[~candidate_df.Laven_ratios.isnull()]
        candidate_df_image = candidate_df[candidate_df.Laven_ratios.isnull()]
        print(candidate_df_text.shape,
            candidate_df_image.shape, candidate_df.shape)

        if len(candidate_df_text)>0:
            candidate_df_text["tuple_org_dup_text"] = candidate_df_text.progress_apply(
                lambda x: (x["original_question_text"], x["duplicate_question_text"]), axis=1)

            candidate_df_text["same"] = candidate_df_text.progress_apply(
                lambda x: True if (x["original_question_code"] == x["duplicate_question_code"]) else False, axis=1)

            # 6.

            candidate_df_85 = candidate_df_text #[candidate_df_text["Laven_ratios"] > 0.85]

            candidate_df_85["clean_laven_ratios"] = candidate_df_85.progress_apply(
                utils.clean_laven_ratios, axis=1)
            candidate_df_85["same"] = candidate_df_85.progress_apply(
                lambda x: True if (x["original_question_code"] == x["duplicate_question_code"]) else False, axis=1)
            candidate_df_85 = candidate_df_85[candidate_df_85["same"] != True]
            # candidate_df_85.rename(columns = {'transformer_scores':'transformer_score'}, inplace = True)
            candidate_df_85["true_labels"] = 1

            candidate_df_85["transformer_score"] = 0  # TODO: Remove this line

            candidate_df_85["pred_label"] = candidate_df_85.progress_apply(
                utils.check_duplicate_exact, axis=1)
            candidate_df_85_exact = candidate_df_85[candidate_df_85["pred_label"] == 1]
            candidate_df_85_similar = candidate_df_85[candidate_df_85["pred_label"] == 0]

            return {
                "original":candidate_df_85.iloc[0]["original_question_code"],
                "exact":candidate_df_85_exact.duplicate_question_code.tolist(),
                "similar":candidate_df_85_similar.duplicate_question_code.tolist()
                }

        elif len(candidate_df_image)>0:
            # For Image
            candidate_df_image["cosine_similarity"] = candidate_df_image.progress_apply(
                lambda x: cosine_similarity(x["original_dense_vector"].reshape(
                    (1, 512)), x["duplicate_dense_vector"].reshape((1, 512))).item(),
                axis=1
            )

            candidate_df_image_exact = candidate_df_image[(candidate_df_image.cosine_similarity > 0.99) & (candidate_df_image.duplicate_question_text.isnull())]
            candidate_df_image_similar = candidate_df_image[(candidate_df_image.cosine_similarity > 0.99)]
            
            return {
                "original":candidate_df_image.iloc[0]["original_question_code"],
                "exact":candidate_df_image_exact.duplicate_question_code.tolist(),
                "similar":candidate_df_image_similar.duplicate_question_code.tolist()
                }
        else:
            return {"original":item[0],"exact":[],"similar":[]}
