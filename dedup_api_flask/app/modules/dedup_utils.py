import pandas as pd
import re
import ast
import torch
import torch.nn as nn
import torchvision.models as models
import torchvision.transforms as transforms
import numpy as np
import cv2
import tqdm

from PIL import Image
import io
from urllib.request import urlopen
import pickle

from bs4 import BeautifulSoup
from pylatexenc.latex2text import LatexNodes2Text

import Levenshtein
import pandas as pd
import numpy as np
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from sklearn.metrics.pairwise import cosine_similarity

stop_words = set(stopwords.words('english'))

stop_words.remove('not')
stop_words.remove("which")
stop_words.remove("what")
stop_words.remove("why")
stop_words.remove("how")

porter = PorterStemmer()



def extract_content_field(x):
    try:
        content = ast.literal_eval(x)
    except:
        content = x

    try:
        question_details = content["question_details"]
        tag = list(question_details.keys())[0]
        question_text = question_details[tag]["question_txt"]
    except:
        # print(content)
        question_text = ""
    try:
        whole_answer = question_details[tag]["answers"]
        whole_answer_body = []
        # print(whole_answer)
        if not whole_answer:
            whole_answer_body = []
            correct_answer = ""

        else:
            try:
                for answer in whole_answer:
                    if answer["is_correct"] == True:
                        correct_answer = answer["body"]
                        whole_answer_body.append(correct_answer)

                    whole_answer_body.append(answer["body"])
            except:
                try:
                    for answer in whole_answer[0]:
                        if answer["is_correct"] == True:
                            correct_answer = answer["body"]
                            whole_answer_body.append(correct_answer)

                        whole_answer_body.append(answer["body"])
                except:
                    whole_answer_body = []
                    correct_answer = ""
    except:
        whole_answer_body = []
        correct_answer = ""

    try:
        question_text_answer = (question_text, correct_answer, whole_answer_body)
    except:
        question_text_answer = (question_text, "", whole_answer_body)

    return question_text_answer

# 2.1 Fetch Image URLs
def fetch_urls(text):
    if type(text)!=str or len(text)==0:
        return []
    else:
        return re.findall(r"src\s*=\s*[\"|'](.+?)['|\"]",text)

# 3.
class Proccessing:
    def __init__(self, ca_df=pd.DataFrame(), prod_df=pd.DataFrame(), grail_df=pd.DataFrame()):
        self.ca_df = ca_df
        self.prod_df = prod_df
        self.grail_df = grail_df
        self.prod_flag = False
        self.ca_flag = False
        self.grail_flag = False
        # self.file_name = file_name

    @staticmethod
    def Parse_Question_Text(question_info_object):
        try:
            question_info_object = question_info_object['en']
        except:
            pass
        try:
            qcontent = re.sub('&nbsp;', ' ', question_info_object).replace('&there4;', ' ')
            return qcontent
        except:
            return question_info_object

    @staticmethod
    def clean(text):
        text = str(text)
        if not text:
            return "No explanation_given"
        else:
            soup = BeautifulSoup(text, 'html.parser')
            soup.prettify()
            soup_text = soup.get_text()
            soup_text = soup_text.replace("\\\\", "\\")
            soup_text = soup_text.replace("\\n", " ")
            try:
                soup_text = LatexNodes2Text().latex_to_text(soup_text)
            except:
                print("Exception in clean method, soup_text",soup_text)
                soup_text = soup_text.strip('\"').replace('\\\\', '\\').replace('\\"', '\"').replace('\\n',
                                                                                                              '\n').replace(
                    "\\r", "\r").replace("\\t", "\t")
                try:
                    soup_text = LatexNodes2Text().latex_to_text(soup_text)
                except:
                    pass

            soup_text = soup_text.strip('\"')
            soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            soup_text = re.sub(r"(^Q.\w+.:)", '', soup_text)
            soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            soup_text = re.sub(r"(^Ans\w+.)", '', soup_text)
            soup_text = soup_text.replace("\n", " ")
            soup_text = soup_text.replace("\r", " ")
            soup_text = soup_text.replace("  ", " ")
            soup_text = soup_text.replace("   ", " ")
            soup_text = re.sub(r'\n', "", soup_text)
            soup_text = re.sub(r'\r', "", soup_text)

        return soup_text

    @staticmethod
    def cleaning_content(qcontent):
        try:
            qcontent = re.sub('<[^>]*>', ' ', qcontent).replace('&nbsp;', ' ').replace('&there4;', ' ').replace('&#39;',
                                                                                                                '').replace(
                '<sup>', '').replace('</sup>', '').replace('<br>', '').replace('<br />', '')
        except Exception as e:
            print("Exception in cleaning content: ",e.__str__())
        return qcontent

    @staticmethod
    def get_symbols(mstring):
        try:
            mstring = str(mstring)
            list_symbols = []
            list_items = mstring.split()
            for item in list_items:
                if (len(item) == 1):
                    if ((re.match('[a-z]', item) == None) and (item != ' ') and (
                            item not in ['.', ',', '\'', ';', '?', '\u2061'])):
                        list_symbols.append(item)

            list_symbols = list(set(list_symbols))
            return list_symbols
        except:
            print(mstring)
            return []

    @staticmethod
    def clean_data(string, list_symbols):
        try:
            string = string.split(' ')
            unwanted = []
            for i in string:
                if (len(i) == 1):
                    if (ord(i) > 128 and i not in list_symbols):
                        unwanted.append(i)
            unwanted = list(unwanted)
            for i in unwanted:
                string.remove(i)
            string = ' '.join(string)
            return string
        except:
            return string

    @staticmethod
    def remove_unicode_content(qcontent):
        try:
            qcontent = re.sub('\u2061', '', qcontent)
            return qcontent
        except:
            return qcontent

    @staticmethod
    def remove_page_margin(content):
        if content.startswith("<!--"):
            return ""
        else:
            return content

    def process_dataframe(self, all_contents_new):        
        all_contents_new['question_info_clean'] = all_contents_new['question_text'].progress_apply(
            lambda x: self.Parse_Question_Text(x))

        all_contents_new.dropna(subset=['question_info_clean'], inplace=True)

        all_contents_new['question_info_clean'] = all_contents_new['question_info_clean'].progress_apply(lambda x: self.clean(x))

        all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean'].apply(
            lambda x: self.cleaning_content(x))

        all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(
            lambda x: self.remove_unicode_content(x))

        all_contents_new = all_contents_new.reset_index()

        all_contents_new['symbols'] = all_contents_new['question_info_clean_complete'].apply(
            lambda x: self.get_symbols(x))
        list_of_list = all_contents_new['symbols'].tolist()
        flat_list = [item for sublist in list_of_list for item in sublist]
        list_symbols = list(set(flat_list))

        all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(
            lambda x: self.clean_data(x, list_symbols))

        all_contents_new['answer_info'] = all_contents_new['correct_answer_text'].apply(
            lambda x: self.Parse_Question_Text(x))

        all_contents_new['correct_option'] = all_contents_new['answer_info'].apply(lambda x: self.clean(x))

        all_contents_new['correct_option'] = all_contents_new['correct_option'].apply(
            lambda x: self.remove_page_margin(x))

        # all_contents_new_removed_dup = all_contents_new[~all_contents_new['question_code'].str.contains("DUP")]

        # all_contents_new_columns = all_contents_new_removed_dup[["question_code","subject","is_test","is_practice","created_at","exams","is_prev_year","question_info_clean_complete","correct_option","question_type","question_images","answer_images"]]

        all_contents_new["question_info_clean_complete"] = all_contents_new["question_info_clean_complete"].apply(
            lambda x: str(x))
        all_contents_new["correct_option"] = all_contents_new["correct_option"].apply(lambda x: str(x))

        # all_contents_new["question_answer_clean"] = all_contents_new["question_info_clean_complete"] +" "+ all_contents_new["correct_option"]

        # all_contents_new_columns["is_test"].fillna(0, inplace=True)
        # all_contents_new_columns["is_practice"].fillna(0, inplace=True)

        return all_contents_new

    def start_processing(self):
        if self.ca_df.empty:
            pass
            # print("emptyDF")

        else:
            content_dataframe = self.process_dataframe(self.ca_df)
            content_dataframe.to_json(r"new_content_data", orient='records',index=False)
            self.ca_flag = True

        if self.prod_df.empty:
            pass
            # print("emptyDF")
        else:
            prod_dataframe = self.process_dataframe(self.prod_df)
            prod_dataframe.to_json(r"new_production_data", orient='records',index=False)
            self.prod_flag = True

        if self.grail_df.empty:
            print("emptyDF")
        else:            
            grail_dataframe = self.process_dataframe(self.grail_df)
            return grail_dataframe        

        return (self.ca_flag, self.prod_flag, self.grail_flag)

class Img2Vec():

    def __init__(self, cuda=False, model='resnet-18', layer='default', layer_output_size=512, log_path=None):
        """ Img2Vec
        :param cuda: If set to True, will run forward pass on GPU
        :param model: String name of requested model
        :param layer: String or Int depending on model. layer = 'layer_name' or int   
                      which layer of the model to extract the output from.   default: 'avgpool'
                      See more docs: https://github.com/christiansafka/img2vec.git
        :param layer_output_size: Int depicting the output size of the requested layer
        """
        self.device = torch.device("cuda" if cuda else "cpu")
        self.layer_output_size = layer_output_size
        self.model_name = model
        
        self.model, self.extraction_layer = self._get_model_and_layer(model, layer)

        self.model = self.model.to(self.device)

        self.model.eval()

        self.scaler = transforms.Resize((224, 224)) # Scale()
        self.normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                              std=[0.229, 0.224, 0.225])
        self.to_tensor = transforms.ToTensor()

        self.log_path = log_path
        self.failed_urls = []

    def get_vec(self, img, tensor=False):
        """ Get vector embedding from PIL image
        :param img: PIL Image or list of PIL Images
        :param tensor: If True, get_vec will return a FloatTensor instead of Numpy array
        :returns: Numpy ndarray
        """
        convert_img_channel = lambda x: cv2.cvtColor(x, cv2.COLOR_BGRA2BGR) if len(x.shape) > 2 and x.shape[2] == 4 else (cv2.cvtColor(x, cv2.COLOR_GRAY2BGR) if len(x.shape)==2 else x)
        convert_img_to_BGR = lambda x: Image.fromarray(convert_img_channel(np.array(x)))
        
        if type(img) == list:
            a = [self.normalize(self.to_tensor(self.scaler(convert_img_to_BGR(im)))) for im in img]
            
            images = torch.stack(a).to(self.device) 
            if self.model_name == 'alexnet':
                my_embedding = torch.zeros(len(img), self.layer_output_size)
            else:
                my_embedding = torch.zeros(len(img), self.layer_output_size, 1, 1)

            def copy_data(m, i, o):
                my_embedding.copy_(o.data)

            h = self.extraction_layer.register_forward_hook(copy_data)
            h_x = self.model(images)
            h.remove()

            if tensor:
                return my_embedding
            else:
                if self.model_name == 'alexnet':
                    return my_embedding.numpy()[:, :]
                else:
                    print(my_embedding.numpy()[:, :, 0, 0].shape)
                    return my_embedding.numpy()[:, :, 0, 0]
        else:
            
            image = self.normalize(self.to_tensor(self.scaler(convert_img_to_BGR(img)))).unsqueeze(0).to(self.device)

            if self.model_name == 'alexnet':
                my_embedding = torch.zeros(1, self.layer_output_size)
            else:
                my_embedding = torch.zeros(1, self.layer_output_size, 1, 1)

            def copy_data(m, i, o):
                my_embedding.copy_(o.data)

            h = self.extraction_layer.register_forward_hook(copy_data)
            h_x = self.model(image)
            h.remove()

            if tensor:
                return my_embedding
            else:
                if self.model_name == 'alexnet':
                    return my_embedding.numpy()[0, :]
                else:
                    return my_embedding.numpy()[0, :, 0, 0]

    def _get_model_and_layer(self, model_name, layer):
        """ Internal method for getting layer from model
        :param model_name: model name such as 'resnet-18'
        :param layer: layer as a string for resnet-18 or int for alexnet
        :returns: pytorch model, selected layer
        """
        if model_name == 'resnet-18':
            model = models.resnet18(pretrained=True)
            if layer == 'default':
                layer = model._modules.get('avgpool')
                self.layer_output_size = 512
            else:
                layer = model._modules.get(layer)

            return model, layer

        elif model_name == 'alexnet':
            model = models.alexnet(pretrained=True)
            if layer == 'default':
                layer = model.classifier[-2]
                self.layer_output_size = 4096
            else:
                layer = model.classifier[-layer]

            return model, layer

        else:
            raise KeyError('Model %s was not found' % model_name)

    def preprocess_image(self, img):
        try:
            desired_size = 224
            old_size = img.size  # old_size[0] is in (width, height) format

            ratio = float(desired_size)/max(old_size)
            new_size = tuple([int(x*ratio) for x in old_size])

            img = img.resize(new_size, Image.ANTIALIAS)

            new_img = Image.new("RGB", (desired_size, desired_size), (255,255,255))
            new_img.paste(img, ((desired_size-new_size[0])//2,
                                        (desired_size-new_size[1])//2))
            return new_img
        except Exception as e:
            raise(e.__str__())

    def download_and_preprocess_images(self, qcode,ques,ans):
        question_images, answer_images = [], []
        
        for url in ques:
            try:
                response = urlopen(url,timeout=2)
                img = response.read()
                img = Image.open(io.BytesIO(img)).convert("RGBA")
                question_images.append(self.preprocess_image(img))
            except Exception as e:
                print(f"Exception in Image Processing({qcode}, {url}): {e.__str__()}")
                self.failed_urls.append([qcode,url,e.__str__()])
        for url in ans:
            try:
                response = urlopen(url,timeout=2)
                img = response.read()
                img = Image.open(io.BytesIO(img)).convert("RGBA")
                answer_images.append(self.preprocess_image(img))
            except Exception as e:
                print(f"Exception in Image Processing({qcode}, {url}): {e.__str__()}")
                self.failed_urls.append([qcode,url,e.__str__()])
        return question_images, answer_images

    def find_image_dense_representation(self, row):
        question_code = row["question_code"]
        if row["is_image_present"]:
            question_images, answer_images = row["question_urls"], row["answer_urls"]
            question_images, answer_images = self.download_and_preprocess_images(question_code, question_images, answer_images)
            # question_images, answer_images = question_images[:2], answer_images[:2]
            row["question_image_dense"] = np.array([self.get_vec(img) for img in question_images]).mean(axis=0,dtype="float16")
            row["answer_image_dense"] = np.array([self.get_vec(img) for img in answer_images]).mean(axis=0,dtype="float16")
        else:
            row["question_image_dense"] = None
            row["answer_image_dense"] = None
        return row

    def dump_failed_urls(self):
        with open(self.log_path,"wb") as file:
            pickle.dump(self.failed_urls,file)

# ------------------------------------------- Find Candidate Questions -------------------------------------------

# 5.
def lavenstein_ratio(set_a, set_b):
    return Levenshtein.ratio(set_a, set_b)

def fetch_Laven(result_df):
    if str(result_df.duplicate_question_text) != ' ':
        return lavenstein_ratio(str(result_df.original_question_text), str(result_df.duplicate_question_text))
    else:
        return 0

def calculate_laven_ratios(formated_output):
    laven_df = pd.DataFrame(formated_output,
                            columns=['original_question_code', 'original_question_text', 'duplicate_question_code',
                                     'duplicate_question_text', "image_field", "original_dense_vector", "dense_vector"])
    laven_df['Laven_ratios'] = laven_df.apply(fetch_Laven, axis=1)
    laven_df = laven_df[laven_df['Laven_ratios'] > 0.79]
    if laven_df.empty:
        return []
    else:
        laven_df_quest = laven_df[
            ['original_question_code', 'duplicate_question_code', "Laven_ratios", "original_question_text",
             "duplicate_question_text", "image_field", "original_dense_vector", "dense_vector"]]
        laven_list = laven_df_quest.values.tolist()
        return laven_list

def find_dups(finalresult, question_code, image_field):
    ids = []
    if image_field:
        for result, _ in finalresult:
            if result['question_code'] == question_code:
                continue
            ids.append((result['question_code'], result['question'], image_field, result[image_field]))
    else:
        for result, _ in finalresult:
            if result['question_code'] == question_code:
                continue
            ids.append((result['question_code'], result['question'], np.nan, []))        
    return ids

def only_text(es, es_index, question_text, question_code):
    query = str(question_text)
    # length = len(query)
    field = "question"
    complex_query = {
        "size": 50,
        "query":{
            "bool":{
                # "filter": [
                #     {"term":{"is_question_text_present":True}},
                #     {"term":{"is_question_image_dense_present":False}},
                #     {"term":{"is_answer_image_dense_present":False}}
                # ],
                "must":{
                    "match": {
                        field: {
                            "query": query
                        }
                    }
                }
            }
        }
    }
    try:
        search_res = es.search(index=es_index, body=complex_query, request_timeout=100)
        return search_res
    except Exception as e:
        print("Exception in candidate search(Only Text):",question_code,e.__str__())
        return None

def text_with_image(es, es_index, question_text, image_field, question_code):
    query = str(question_text)
    # length = len(query)
    field = "question"
    complex_query = {
        "size": 50,
        "query":{
            "bool":{
                "filter": [
                    {"term":{"is_question_text_present":True}},
                    {"term":{"is_{}_present".format(image_field):True}}
                ],
                "must":{
                    "match": {
                        field: {
                            "query": query
                        }
                    }
                }
            }
        }
    }
    try:
        search_res = es.search(index=es_index, body=complex_query, request_timeout=100)
        return search_res
    except Exception as e:
        print("Exception in candidate search(Text+Image):",question_code,e.__str__())
        return None

def only_image(es, es_index, image_field, dense_vector, question_code):
    complex_query ={
        "size": 50,
        "query": {
            "script_score": {
                "query":{
                    "bool":{
                        "filter":{
                            "term":{"is_{}_present".format(image_field):True}
                        }
                    }
                },
                "script": {
                    "source": "cosineSimilarity(params.queryVector, doc['{}']) + 1.0".format(image_field),
                    "params": {
                        "queryVector": dense_vector if type(dense_vector)==list else dense_vector.tolist()
                    }
                },
                "min_score": 1.90
            }
        }
    }
    try:
        search_res = es.search(index=es_index, body=complex_query, request_timeout=100)
        return search_res
    except Exception as e:
        print("Exception in candidate search(Only Image):",question_code,e.__str__())
        return None

def search(es, es_index, question_code, is_question_text_present, question_text, 
           is_question_image_dense_present, question_image_dense,
           is_answer_image_dense_present, answer_image_dense,
           expected_output, not_es_search_codes): 
    # -------- Find image field and dense vector from question-answer ---------------------
    if is_question_image_dense_present==True:
        image_field = "question_image_dense"
        dense_vector = question_image_dense
    elif is_answer_image_dense_present==True:
        image_field = "answer_image_dense"
        dense_vector = answer_image_dense
    else:
        image_field = None
        dense_vector = None
    # ---------------- only text is present -------------------------------------------
    if is_question_text_present==True and image_field is None:
        search_result = only_text(es, es_index, question_text, question_code)
        if search_result:
            finalresult = []
            for res in search_result['hits']['hits']:
                result = res['_source']
                score = res['_score']
                finalresult.append([result, score])
            # print(finalresult)
            output = find_dups(finalresult, question_code, image_field)
            if len(output) >= 1:
                formated_output = [(question_code, question_text, x[0], x[1], x[2], [], x[3]) for x in output]
                formated_laven = calculate_laven_ratios(formated_output)
                expected_output.extend(formated_laven)
            else:
                return []
        else:
            not_es_search_codes.append(question_code)
            
    # ---------------- text + image is present -------------------------------------------
    elif is_question_text_present==True and image_field is not None:
        search_result = text_with_image(es, es_index, question_text, image_field, question_code)
        if search_result:
            finalresult = []
            for res in search_result['hits']['hits']:
                result = res['_source']
                score = res['_score']
                finalresult.append([result, score])
            output = find_dups(finalresult, question_code, image_field)
            if len(output) >= 1:
                formated_output = [(question_code, question_text, x[0], x[1], x[2], dense_vector, x[3]) for x in output]
                formated_laven = calculate_laven_ratios(formated_output)
                expected_output.extend(formated_laven)
            else:
                return []
        else:
            not_es_search_codes.append(question_code)

    # ---------------- only image is present -------------------------------------------
    elif is_question_text_present==False and image_field is not None:
        search_result = only_image(es, es_index, image_field, dense_vector, question_code)
        if search_result:
            finalresult = []
            for res in search_result['hits']['hits']:
                result = res['_source']
                score = res['_score']
                finalresult.append([result, score])
            output = find_dups(finalresult, question_code, image_field)
            if len(output) >= 1:
                formated_output = [(question_code, x[0], np.nan, np.nan, x[1], x[2], dense_vector, x[3]) for x in output]
                expected_output.extend(formated_output)
            else:
                return []
        else:
            not_es_search_codes.append(question_code)

    else:
        not_es_search_codes.append(question_code)
    
    return expected_output

## ------------------------------------------- Categorize into Exact and Similar Groups -------------------------------------------

# def get_transformer_score(df):
#     original_question_text, duplicate_question_text = df[0], df[1]
#     query_embedding = embedder.encode([original_question_text])
#     another_query_embedding = embedder.encode([duplicate_question_text])
#     distances = cosine(query_embedding, another_query_embedding)
#     real_distance = round((1 - distances) * 100, 2)
#     return real_distance

## read stop words -- moved to config

def make_lower(text):
    return text.lower()

def remove_punct(text):
    punctuations = r'''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    no_punct = ""
    for char in text:
        if char not in punctuations:
            no_punct = no_punct + char
    return no_punct

def remove_stop_words(text):
    word_tokens = word_tokenize(text)
    filtered_sentence = []
    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w)
    string = ' '.join(word for word in filtered_sentence)
    return string

def clean_laven_ratios(df):
    dup_cleaned_question_text = make_lower(df["duplicate_question_text"])
    cleaned_question_text = make_lower(df["original_question_text"])

    dup_cleaned_question_text = remove_punct(dup_cleaned_question_text)
    cleaned_question_text = remove_punct(cleaned_question_text)

    dup_cleaned_question_text = remove_stop_words(dup_cleaned_question_text)
    cleaned_question_text = remove_stop_words(cleaned_question_text)

    clean_ratio = lavenstein_ratio(cleaned_question_text, dup_cleaned_question_text)
    return clean_ratio

# 7.
def check_duplicate_exact(df):
    original_question_text = df["original_question_text"]
    duplicate_question_text = df["duplicate_question_text"]

    dup_cleaned_question_text = remove_stop_words(duplicate_question_text)
    cleaned_question_text = remove_stop_words(original_question_text)

    dup_cleaned_question_text = remove_punct(dup_cleaned_question_text)
    cleaned_question_text = remove_punct(cleaned_question_text)

    dup_cleaned_question_text = make_lower(dup_cleaned_question_text)
    cleaned_question_text = make_lower(cleaned_question_text)

    original_question_text_list = cleaned_question_text.split(" ")
    duplicate_question_text_list = dup_cleaned_question_text.split(" ")

    x = []
    y = []

    for index, value in enumerate(original_question_text_list):
        try:
            if (value != duplicate_question_text_list[index]):
                value = porter.stem(value)
                x.append(value)
        except:
            value = porter.stem(value)
            x.append(value)

    for index, value in enumerate(duplicate_question_text_list):
        try:
            if (value != original_question_text_list[index]):
                value = porter.stem(value)
                y.append(value)
        except:
            value = porter.stem(value)
            y.append(value)

    rem_cleaned_question_text = ' '.join(word for word in x)
    rem_dup_cleaned_question_text = ' '.join(word for word in y)

    clean_ratio = lavenstein_ratio(rem_cleaned_question_text, rem_dup_cleaned_question_text)
    
    # ~~~~~~~~~~~~~~~~~~~~~~ Image Similarity ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if str(df["image_field"])=="nan":
        image_cosine_score = None
    else:
        image_cosine_score = cosine_similarity(
            df["original_dense_vector"].reshape((1,512)),
            df["duplicate_dense_vector"].reshape((1,512))
        ).item()
    
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if (df["clean_laven_ratios"] >= 0.98):
        if (df["clean_laven_ratios"] < 0.91):
            if image_cosine_score:
                return 0 if image_cosine_score > 0.99 else -1
            else:
                return 0
        if df["clean_laven_ratios"] >= 1.0:
            if image_cosine_score:
                return 1 if image_cosine_score > 0.99 else 0
            else:
                return 1
        if (rem_cleaned_question_text == "") or (rem_cleaned_question_text == " ") or (
                rem_dup_cleaned_question_text == "") or (rem_dup_cleaned_question_text == " "):
            if image_cosine_score:
                return 1 if image_cosine_score > 0.99 else 0
            else:
                return 1
        if clean_ratio >= 0.80:
            if image_cosine_score:
                return 1 if image_cosine_score > 0.99 else 0
            else:
                return 1
        else:
            if image_cosine_score:
                return 0 if image_cosine_score > 0.99 else -1
            else:
                return 0

    if (df["clean_laven_ratios"] < 0.91):
        if image_cosine_score:
            return 0 if image_cosine_score > 0.99 else -1
        else:
            return 0

    if (rem_cleaned_question_text == "") or (rem_cleaned_question_text == " ") or (
            rem_dup_cleaned_question_text == "") or (rem_dup_cleaned_question_text == " "):
        if image_cosine_score:
            return 1 if image_cosine_score > 0.99 else 0
        else:
            return 1


    if (clean_ratio >= 0.84):
        if image_cosine_score:
            return 1 if image_cosine_score > 0.99 else 0
        else:
            return 1
    else:
        if image_cosine_score:
            return 0 if image_cosine_score > 0.99 else -1
        else:
            return 0
