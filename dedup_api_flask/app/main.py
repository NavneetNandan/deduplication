from flask import Flask
from routers.handles import cqi_dedup

def main():
    app = Flask("CQI_DEDUP")
    app.register_blueprint(cqi_dedup,url_prefix="/dsl/cqi")
    app.run(host='0.0.0.0', debug=True)
    
if __name__ == "__main__":
    main()