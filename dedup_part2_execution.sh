cd /home/sshuser/brijesh/deduplication/deduplication/dedup_part_2_1/ && make build
/usr/bin/spark-submit --master yarn --deploy-mode cluster --name "CQI Deduplication Batch Pipeline Part 2.1" \
--conf spark.pyspark.python=/usr/bin/anaconda/envs/py36/bin/python3 \
--conf spark.jars.packages="org.elasticsearch:elasticsearch-hadoop:7.6.1,org.mongodb.spark:mongo-spark-connector_2.11:2.4.2,graphframes:graphframes:0.8.1-spark2.4-s_2.11" \
--conf spark.driver.memory=18g --conf spark.executor.instances=5 --conf spark.executor.memory=5g \
--conf spark.yarn.executor.memoryOverhead=500m --conf spark.executor.cores=5 \
--conf spark.yarn.max.executor.failures=40 --conf spark.yarn.executor.failuresValidityInterval=1h \
--conf spark.rpc.message.maxSize=1024 --conf spark.kryoserializer.buffer.max=1g --conf spark.driver.maxResultSize=8g \
--conf spark.yarn.maxAppAttempts=1 --conf spark.serializer="org.apache.spark.serializer.KryoSerializer" \
--conf spark.kryoserializer.buffer.max=128m \
--py-files /home/sshuser/brijesh/deduplication/deduplication/dedup_part_2_1/dist/utilities.zip /home/sshuser/brijesh/deduplication/deduplication/dedup_part_2_1/dist/main.py

cd /home/sshuser/brijesh/deduplication/deduplication/dedup_part_2_2/ && make build
/usr/bin/spark-submit --master yarn --deploy-mode cluster --name "CQI Deduplication Batch Pipeline Part 2.2" \
--conf spark.pyspark.python=/usr/bin/anaconda/envs/py36/bin/python3 \
--conf spark.jars.packages="org.elasticsearch:elasticsearch-hadoop:7.6.1,org.mongodb.spark:mongo-spark-connector_2.11:2.4.2,graphframes:graphframes:0.8.1-spark2.4-s_2.11" \
--conf spark.driver.memory=16g --conf spark.executor.instances=4 --conf spark.executor.memory=13g \
--conf spark.yarn.executor.memoryOverhead=1.1g --conf spark.executor.cores=5 --conf spark.yarn.max.executor.failures=40 \
--conf spark.yarn.executor.failuresValidityInterval=1h --conf spark.rpc.message.maxSize=1024 \
--conf spark.kryoserializer.buffer.max=1g --conf spark.driver.maxResultSize=8g --conf spark.yarn.maxAppAttempts=1 \
--conf spark.serializer="org.apache.spark.serializer.KryoSerializer" --conf spark.kryoserializer.buffer.max=128m \
--py-files /home/sshuser/brijesh/deduplication/deduplication/dedup_part_2_2/dist/utilities.zip /home/sshuser/brijesh/deduplication/deduplication/dedup_part_2_2/dist/main.py
