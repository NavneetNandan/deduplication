import time
import os
import sys
from pyspark.sql import SparkSession
import json
import time
from datetime import datetime, timedelta
import pandas as pd
import pyspark.sql.functions as F
import pyspark.sql.types as t
from pyspark.sql import Row
from elasticsearch import Elasticsearch, helpers

spark = (SparkSession
.builder
.appName('CQI Deduplication Batch Pipeline')
.getOrCreate())

VERSION = "batch_v3"
es_index = "cg-duplication-questions" + "_" + str.lower(VERSION)
directory = "wasb:///user/sshuser/data_dedup_modularized"
all_data_dump = os.path.join(directory,VERSION,'cg-processed-questions.parquet')

es = Elasticsearch(["10.141.11.88","10.141.11.89","10.141.11.90"],timeout=30, max_retries=5, retry_on_timeout=True)

result = helpers.scan(
            es,
            query={
                "query":{"match_all":{}}
                },
            size=10000,scroll = '1h',index= es_index
        )

temp = []
chunk_number = 0

for x in result:
    temp.append(x["_source"])

    if len(temp)==60000:
        
        print("len",len(temp))

        grail_dataframe = spark.createDataFrame(Row(**y) for y in temp)

        if chunk_number == 0:
            grail_dataframe.write.mode("overwrite").parquet(
                all_data_dump
            )
        else:
            grail_dataframe.write.mode("append").parquet(
                all_data_dump
            )

        # del temp
        del grail_dataframe
        
        chunk_number += 1
        temp = []

if len(temp)<60000:
    grail_dataframe = spark.createDataFrame(Row(**y) for y in temp)

    grail_dataframe.write.mode("append").parquet(
        all_data_dump
    )

