## How to Submit this Job??

1. Run `make build` if __*dist*__ directory is missing or code is changed
2. __Submit Spark Job using given command:__ `cd dist && /usr/bin/spark-submit --deploy-mode cluster --conf spark.pyspark.python=/usr/bin/anaconda/envs/py36/bin/python3 --conf spark.jars.packages="org.elasticsearch:elasticsearch-hadoop:7.6.1,org.mongodb.spark:mongo-spark-connector_2.11:2.4.2,graphframes:graphframes:0.8.1-spark2.4-s_2.11" --conf spark.driver.memory=12g --conf spark.rpc.message.maxSize=1024 --conf spark.driver.maxResultSize=4g --conf spark.yarn.maxAppAttempts=1 --py-files utilities.zip main.py`

*Note: Please Review and Change the `--conf` options as per your cluster or execution environment*