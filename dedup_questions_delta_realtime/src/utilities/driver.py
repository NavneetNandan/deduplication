import pyspark.sql.functions as F

import warnings
warnings.filterwarnings("ignore")

from utilities.config import PATHS, DATABASES, VERSION, KAFKA_CONFIG

import utilities.delta_utils as du

class Dedup():
    def __init__(self, spark, kafka_env):
        # self.directory = PATHS.get("base")

        self.spark = spark
        self.kafka_config = KAFKA_CONFIG.get(kafka_env,"staging")

        # self.status_filters_clustering = status_filters_clustering
        # self.mongo_config = DATABASES.get("mongo")
        # self.es_config = DATABASES.get("ES")
        # self.es_nodes = DATABASES.get("ES_CLUSTER_NODES")
        # self.es = Elasticsearch(self.es_nodes,timeout=30, max_retries=5, retry_on_timeout=True)
        # self.es_content_bank = Elasticsearch(DATABASES.get("CQI_CB_ES"),timeout=30, max_retries=5, retry_on_timeout=True)

        # self.status_filters = status_filters
        # self.mongo_config["spark.mongodb.input.collection"] = "learning_objects"
        # self.lo_coll = self.mongo_config.copy()
        
        # self.grade = grade
        # self.all_data_dump = os.path.join(self.directory,VERSION,'cg-processed-questions.parquet')
        # self.candidate_pairs_dump = os.path.join(self.directory,VERSION,'cg-candidate_pairs.parquet')
        # self.candidate_pairs_exact = os.path.join(self.directory,VERSION,'cg-candidate_pairs_exact.parquet')
        # self.candidate_pairs_exact_temp = os.path.join(self.directory,VERSION,'cg-candidate_pairs_exact_temp.parquet')
        # self.cluster_exact_file = os.path.join(self.directory,VERSION,'cg_exact_duplicates_clusters.parquet')
        # self.cluster_similar_file = os.path.join(self.directory,VERSION,'cg_similar_duplicates_clusters.parquet')
        # self.not_present_in_CB = os.path.join(self.directory,VERSION,'missing_cb.parquet')
        # self.not_present_in_LM = os.path.join(self.directory,VERSION,'missing_lm.parquet')
        # self.joined_q_level_df_with_cluster = os.path.join(self.directory,VERSION,'joined_q_level_df_with_cluster.parquet')
        # self.checkpoint_dir = os.path.join(self.directory,VERSION,"checkpoint/")
        # self.kafka_checkpoint_path = os.path.join(self.directory,VERSION,"kafka_checkpoint.parquet")
        
        # self.es_index = "cg-duplication-questions_batch_v3_reindexed" #"cg-duplication-questions" + "_" + str.lower(VERSION) 
        # self.final_index_name = "cg-results_dedup" + "_" + str.lower(VERSION)

    def driver(self):
        # 0. Fetch Delta from Kafka queue
        # change_df = du.fetch_delta_changes_from_kafka(self.spark, self.kafka_config.copy(), self.kafka_checkpoint_path).persist(StorageLevel.MEMORY_AND_DISK)

        change_df = self.spark.readStream.format("kafka").options(**self.kafka_config).load()

        # offsets_df = change_df.select("partition", "offset").groupBy("partition").agg(F.max("offset").alias("offset")).persist(StorageLevel.MEMORY_AND_DISK)

        change_df = change_df.withColumn(
            "value", du.decoder(F.col("value"))
        ).select(
            "value.*", "offset", "partition"
        )
        
        # delta_df = change_df.orderBy(["id", "latest_version"]).groupBy(change_df.id).agg(
        #     F.last(F.col("question_code")).alias("question_code"),
        #     F.last(F.col("optype")).alias("optype"), F.last(
        #         F.col("latest_status")).alias("latest_status"),
        #     F.last(F.col("latest_version")).alias("latest_version"), F.last(
        #         F.col("fullDocument")).alias("fullDocument"),
        #     F.max(F.col("change_in_content")).alias("change_in_content"),
        #     F.max(F.col("offset")).alias("offset")
        # )

        delta_df = change_df
        
        # print("count delta df:",delta_df.count())
        # delta_df.groupBy("optype").agg({"id":"count"}).show()
        # print("kafka checkpoint df:")
        # offsets_df.show()

        # change_df.unpersist()

        query = delta_df.writeStream.outputMode("append").foreachBatch(du.process_batch).start()

        query.awaitTermination()