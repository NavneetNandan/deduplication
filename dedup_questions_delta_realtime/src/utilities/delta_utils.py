import json
import pyspark.sql.functions as F
import pyspark.sql.types as t
from pyspark.sql import Row
from pyspark import StorageLevel
from elasticsearch import Elasticsearch, helpers
from tqdm import tqdm
from utilities.config import PATHS, DATABASES, VERSION, KAFKA_CONFIG

import re
from bs4 import BeautifulSoup
from pylatexenc.latex2text import LatexNodes2Text
# import json
# from elasticsearch import helpers
import math 
import Levenshtein
import json
from datetime import datetime

import utilities.utils as utils
import utilities.image_vectorization as IV
# import utilities.select_candidates as sc
# import utilities.spark_variables as sv


schema = t.StructType([
    t.StructField("fullDocument", t.StringType(), False),
    t.StructField("optype", t.StringType(), False),
    t.StructField("id", t.IntegerType(), False),
    t.StructField("question_code", t.StringType(), False),
    t.StructField("latest_status", t.StringType(), False),
    t.StructField("latest_version", t.IntegerType(), False),
    t.StructField("change_in_content", t.BooleanType(), False)
])

es = Elasticsearch(DATABASES.get("ES_CLUSTER_NODES"),timeout=30, max_retries=5, retry_on_timeout=True)
es_index = DATABASES.get("ES_INDEX_NAME")
es_config = DATABASES.get("ES")

@F.udf(schema)
def decoder(s):
    loaded_json = json.loads(s.decode('utf-8'))

    _id = int(loaded_json.get("id"))
    question_code = loaded_json.get("question_code")
    optype = loaded_json.get("optype")
    latest_status = loaded_json.get("latest_status")
    latest_version = loaded_json.get("latest_version")
    change_in_content = bool(loaded_json.get("change_in_content"))
    fullDoc = json.dumps(loaded_json.get("fullDocument"))

    return t.Row(
        "fullDocument", "optype", "id", "question_code", "latest_status", "latest_version", "change_in_content"
    )(fullDoc, optype, _id, question_code, latest_status, latest_version, change_in_content)

# def find_offsets(spark, kafka_config, kafka_checkpoint_path):
#     try:
#         offsets_df = spark.read.parquet(kafka_checkpoint_path)
#         offsets_dict = offsets_df.rdd.map(lambda x: (x[0],x[1])).collectAsMap()
#         print("offsets_dict:",offsets_dict)
#     except Exception as e:
#         print("Exception in read offsets:",e.__str__())
#         kafka_config["startingOffsets"] = "earliest" #""" {"cqi_deduplication_cg_delta":{"0":90000,"1":90000,"2":90000}} """ 
#         print("kafka_config:",kafka_config)
#         return kafka_config

#     topic_name = kafka_config.get("subscribe")
#     print("topic name:",topic_name)
    
#     #""" {"cqi_deduplication_cg_delta":{"0":1020911,"1":1020911,"2":1020911}} """
#     kafka_config["startingOffsets"] = json.dumps({topic_name: {str(k):v for k,v in offsets_dict.items()}})
#     print("kafka_config:",kafka_config)
#     return kafka_config

# def fetch_delta_changes_from_kafka(spark, kafka_config, kafka_checkpoint_path):
    # kafka_config = find_offsets(spark, kafka_config, kafka_checkpoint_path)
    # kafka_config["startingOffsets"] = """ {"cqi_deduplication_cg_delta":{"0":90000,"1":90000,"2":90000}} """ #"earliest"
    # kafka_config["endingOffsets"] = """ {"cqi_deduplication_cg_delta":{"0":103075,"1":102881,"2":102635}} """ #"earliest"
    
    # change_df = spark.readStream.format("kafka").options(**kafka_config).load()
    #     # change_df.show(5)
    # except Exception as e:
    #     print("Exception in read kafka:",e.__str__())
    #     kafka_config["startingOffsets"] = "earliest" #""" {"cqi_deduplication_cg_delta":{"0":90000,"1":90000,"2":90000}} """ 
    #     change_df = spark.read.format("kafka").options(**kafka_config).load()
    
    # return change_df


def delete_doc_generator(ids,index_name):
    print("Bulk Delete:",len(ids))
    for _id in ids:
        yield {
            "_index": index_name,
            "_type": "_doc",
            "_op_type": "delete",
            "_id": _id
        }
    raise StopIteration

def delete_doc_driver(delete_ids):
    op = helpers.bulk(es, delete_doc_generator(delete_ids,es_index),raise_on_error= False,stats_only = True)
    print("delete summary:\ndeleted successfully:",op[0],"deletion failed:",op[1])
    return 

@F.udf(t.StringType())
def field_with_version(field,version):
    return str(field)+"__"+str(version)

def update_doc_generator(docs, index_name):
    print("Bulk update on ES:",len(docs))
    for doc in docs:
        yield {
            "_index": index_name,
            "_type": "_doc",
            "_op_type": "update",
            "_id": doc["id"],
            "_source": {
                "script": {
                    "source": "ctx._source.question_version = params.latest_version; ctx._source.question_code_with_version = params.qcode_with_version; ctx._source.question_id_with_version = params.id_with_version; ctx._source.status = params.latest_status; ctx._source.timestamp = params.timestamp;",
                    "lang": "painless",
                    "params": {
                        "latest_version": doc["latest_version"],
                        "qcode_with_version": doc["qcode_with_version"],
                        "id_with_version": doc["id_with_version"],
                        "latest_status": doc["latest_status"],
                        "timestamp": datetime.now()
                    }
                }
            }
        }
    raise StopIteration

def update_doc_driver(update_version_df):
    update_dicts = list(map(lambda row: row.asDict(),update_version_df.select("id","latest_version","latest_status","id_with_version","qcode_with_version").collect()))

    op = helpers.bulk(
        es,
        update_doc_generator(
            update_dicts, es_index
        ),
        raise_on_error=False,
        yield_ok = False
    )
    
    return [int(x.get("update",{}).get("_id")) for x in op[1]]

def Parse_Question_Text(question_info_object):
    try:
        qcontent = re.sub('&nbsp;', ' ', question_info_object).replace('&there4;', ' ')
        return qcontent
    except:
        return question_info_object

def clean(text):
    try:
        text = str(text)
        if not text:
            return "No explanation_given"
        else:
            text = re.sub(r"<gdiv.*?>(.|\r|\n)*?<\/gdiv>","",text)
            text = re.sub(r"(<\/gdiv>)","",text)
            soup = BeautifulSoup(text, 'html.parser')
            soup.prettify()
            soup_text = soup.get_text()
            soup_text = soup_text.replace("\\\\", "\\")
            soup_text = soup_text.replace("\\n", " ")
            try:
                soup_text = LatexNodes2Text(keep_comments=True,math_mode="with-delimiters").latex_to_text(soup_text,tolerant_parsing=True)
                if soup_text[-1]=="$":
                    soup_text = soup_text[:-1]
            except:
                print(soup_text)
                soup_text = soup_text.strip('\"').replace('\\\\', '\\').replace('\\"', '\"').replace('\\n','\n').replace("\\r", "\r").replace("\\t", "\t")
                try:
                    soup_text = LatexNodes2Text(keep_comments=True,math_mode="with-delimiters").latex_to_text(soup_text,tolerant_parsing=True)
                    if soup_text[-1]=="$":
                        soup_text = soup_text[:-1]
                except:
                    pass

            soup_text = soup_text.strip('\"')
            # soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            # soup_text = re.sub(r"(^Q.\w+.:)", '', soup_text)
            # soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            # soup_text = re.sub(r"(^Ans\w+.)", '', soup_text)
            soup_text = soup_text.replace("\n", " ")
            soup_text = soup_text.replace("\r", " ")
            soup_text = soup_text.replace("↵", "")
            soup_text = soup_text.replace("  ", " ")
            soup_text = soup_text.replace("   ", " ")
            soup_text = re.sub(r'\n', "", soup_text)
            soup_text = re.sub(r'\r', "", soup_text)
    except Exception as e:
        print(e.__str__())

    return soup_text

def remove_page_margin(content):
    if content.startswith("<!--"):
        return ""
    else:
        return content

def clean_answer_options_driver(answer_text):
    return remove_page_margin(clean(Parse_Question_Text(answer_text.strip())))

def fetch_urls(text):
    try:
        if type(text)!=str or len(text)==0:
            return []
        else:
            return re.findall(r"src\s*=\s*[\"|'](.+?)['|\"]",text)
    except Exception as e:
        print(e.__str__())
        return []


schema = t.StructType([
    t.StructField("subtype", t.StringType(), True),
    t.StructField("question_text", t.StringType(), True),
    t.StructField("whole_answer_body", t.StringType(), True),
    t.StructField("correct_options", t.StringType(), False),
    t.StructField("learning_maps", t.ArrayType(t.StringType()), True),
    t.StructField("question_urls", t.ArrayType(t.StringType()), True),
    t.StructField("answer_urls", t.ArrayType(t.StringType()), True),
    t.StructField("avg_hygiene_score", t.FloatType(), True)
    ])

def parse_question_details(qd):
    key = [k for k,v in qd.items() if v is not None]
    key = key[0] if len(key)>0 else "key"

    question_txt = qd.get(key,{}).get("question_txt","")
    answer_options = qd.get(key,{}).get("answers",[])
    
    if any(type(x)==list for x in answer_options):
        answer_options = [y for x in answer_options for y in x]
    
    whole_answer_body, correct_options = [], []
    for x in answer_options:
        option = str(x.get("body","")) # Handles Subjective Numericals
        if x.get("is_correct",False)==True:
            correct_options.append(option)
        
        whole_answer_body.append(option)

    return question_txt, whole_answer_body, correct_options

@F.udf(schema)
def return_relevant_fields(content):
    try:
        true = True
        false = False
        lo = json.loads(content)

        subtype = lo.get("subtype","")
        qid = lo.get("id","")
        content = lo.get("content",{})

        ## Question Details
        qd = content.get("question_details")
        aq = content.get("associated_questions")
        
        question_text, whole_answer_body, correct_options = [], [], []

        if qd:
            qt,at,cao = parse_question_details(qd)
            question_text.append(qt)
            whole_answer_body.extend(at)
            correct_options.extend(cao)
        
        if aq:
            for x in aq:
                qt,at,cao = parse_question_details(x.get('content',{}).get('question_details',{}))
                question_text.append(qt)
                whole_answer_body.extend(at)
                correct_options.extend(cao)
        
        ## URLs
        question_urls = fetch_urls(" ".join(question_text))
        # answer_urls = fetch_urls(" ".join(whole_answer_body))
        answer_urls = fetch_urls(" ".join(correct_options))
        
        ## question and whole answer body
        question_text = " ".join(sorted(map(clean_answer_options_driver,question_text))).strip()
        whole_answer_body = " ".join(sorted(map(clean_answer_options_driver,whole_answer_body))).strip()
        correct_options = " ".join(sorted(map(clean_answer_options_driver,correct_options))).strip()
        
        ## Question Meta
        qm = content.get("question_meta_tags",[])
        lms = []
        for x in qm:
            lms.extend(x.get("learning_maps",[]))
            
        lms = list(set(lms))
        
        avg_hygiene_score = -1.0

    except Exception as e:
        print("Exception in return_relevant_fields: (question id:",qid,")",e.__str__())
        subtype = None
        question_text = None
        whole_answer_body = None
        correct_options = None
        lms = None
        question_urls, answer_urls = None,None
        avg_hygiene_score = -1.0
        
    return t.Row('subtype','question_text', 'whole_answer_body',"correct_options","learning_maps","question_urls","answer_urls","avg_hygiene_score")(subtype,question_text,whole_answer_body,correct_options,lms,question_urls,answer_urls,avg_hygiene_score)

def process_fulldocument_and_fetch_relevant_content(df):
    df = df.withColumn(
        "fullDocument",return_relevant_fields(F.col("fullDocument"))
    ).select(
        "id",F.col("latest_version").alias("_version"),F.col("latest_status").alias("status"),"question_code","fullDocument.*"
    )
    df = df.filter(df.subtype!="LinkedComprehension")
    df = df.dropna("any")
    return df



def process_batch(df,epoch_id):

    df = df.persist()
    insert_df = None

    print(
        "Processing till offset (max of offsets of all partitions):",
        df.select("partition", "offset").groupBy("partition").agg(F.max("offset").alias("offset")).show() #.agg({"offset": "max"}).collect()[0][0]
        )

    # 0.1 delete los from ES
    delete_los_df = df.filter(df.optype=="delete").persist()
    if len(delete_los_df.take(1))>0:
        delete_ids = delete_los_df.select("id").rdd.flatMap(lambda x:x).collect()
        print("epoch_id:",epoch_id,"length of delete_ids:",len(delete_ids))
        delete_doc_driver(delete_ids)
    delete_los_df.unpersist()
        
    # 0.2 update los on ES
    update_version_df = df.filter(
        (df.optype == "update") & (df.change_in_content==False)
    ).persist()

    if len(update_version_df.take(1))>0:
        update_version_df = update_version_df.withColumn(
            "id_with_version", field_with_version(F.col("id"),F.col("latest_version"))
        )
        update_version_df = update_version_df.withColumn(
            "qcode_with_version", field_with_version(F.col("question_code"),F.col("latest_version"))
        )
        
        missing_ids = update_doc_driver(update_version_df)
        print("missing ids:",len(missing_ids))
        
        # 0.3 insert los on ES
        insert_df = update_version_df.select('id', 'question_code', 'optype', 'latest_status', 'latest_version', 'fullDocument', 'change_in_content',"offset").filter(update_version_df.id.isin(missing_ids)).persist()
    

    change_in_los_df = df.filter(
            (df.optype == "insert") | (
                (df.optype == "update") & (df.change_in_content == True)
            )
        ).persist()
    
    if insert_df and len(insert_df.take(1))>0:
        if len(change_in_los_df.take(1))>0:
            insert_df = insert_df.union(change_in_los_df.select(insert_df.columns)).persist()
        else:
            pass
    elif len(change_in_los_df.take(1))>0:
        insert_df = change_in_los_df.select('id', 'question_code', 'optype', 'latest_status', 'latest_version', 'fullDocument', 'change_in_content',"offset").persist()
    else:
        insert_df = None

    if insert_df and len(insert_df.take(1))>0:
        # 1.
        union_df = process_fulldocument_and_fetch_relevant_content(insert_df).persist()
        
        try:
            input_process = utils.Proccessing(grail_df=union_df)
            grail_dataframe = input_process.start_processing()
            if type(grail_dataframe) == tuple:
                return 
        except Exception as e:
            print("*"*10,"Exception in Process Text:",e.__str__(),"*"*10)
            return
        
        # 2 Compute meta fields
        grail_dataframe = grail_dataframe.withColumn(
            "question_id_with_version",
            F.concat(
                F.col("id").cast("string"),
                F.lit("__"),
                F.col("_version").cast("string")
            )
        )
        grail_dataframe = grail_dataframe.withColumn(
            "question_code_with_version",
            F.concat(
                F.col("question_code"),
                F.lit("__"),
                F.col("_version").cast("string")
            )
        )

        # 3 Fetch Dense Representation
        grail_dataframe = grail_dataframe.withColumn(
            "dense_vectors",IV.find_image_dense_representation(
                    F.col("question_code"),F.col("question_urls"),F.col("answer_urls")
                )
            ).persist()
        
        grail_dataframe = grail_dataframe.withColumn("timestamp",F.current_timestamp())

        grail_dataframe = grail_dataframe.select(
            "id", F.col("_version").alias("question_version"), "status", "question_code", "subtype",
            "learning_maps", "avg_hygiene_score",
            "question_id_with_version", "question_code_with_version",
            "is_question_text_present", F.col("question_info_clean_complete").alias("question"),
            "is_answer_text_present", "whole_answer_body","correct_options",
            "timestamp", 'question_urls', 'answer_urls', 
            "dense_vectors.*"
        )

        ## 4 Save on ES
        utils.driver_ingest_on_es(es, es_index, grail_dataframe, es_config)
        print("Count of Inserted Questions (Sum of insert, update with change in content and missing questions):",grail_dataframe.count())
        print()

        update_version_df.unpersist()
        change_in_los_df.unpersist()
        union_df.unpersist()
        grail_dataframe.unpersist()
    else:
        return

# Current Committed Offsets: {KafkaV2[Subscribe[cqi_deduplication_cg_delta]]: {"cqi_deduplication_cg_delta":{"2":1073724,"1":1072927,"0":1131416}}}
# Current Available Offsets: {KafkaV2[Subscribe[cqi_deduplication_cg_delta]]: {"cqi_deduplication_cg_delta":{"2":1073738,"1":1072949,"0":1131430}}}
