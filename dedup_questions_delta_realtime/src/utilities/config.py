appName = "CQI-Deduplication"
VERSION = "batch_v3" # Keep this in Small Letters only

PATHS = {
	"base": "wasb:///user/sshuser/data_dedup_modularized"
}

DATABASES = {
	"mongo":{
		"spark.mongodb.input.uri":"mongodb://ro_dsl:EHJpUwVO2vgMuk@tech-contentgrail-mongo-prod-01:27017,tech-contentgrail-mongo-prod-02:27017,tech-contentgrail-mongo-prod-03:27017,tech-contentgrail-mongo-prod-04:27017/?authSource=contentgrail&replicaSet=cg-mongo-prod",
		"spark.mongodb.input.database": "contentgrail",
		"spark.mongodb.input.collection": "",
		"spark.mongodb.input.readPreference.name": "primaryPreferred"
	},
	"ES": {
		"es.nodes": "10.141.11.88,10.141.11.89,10.141.11.90",
		"es.port" : '9200',
		"es.nodes.wan.only": "true",
		"es.resource": ""
	},
	"ES_CLUSTER_NODES": ["10.141.11.88","10.141.11.89","10.141.11.90"],
	"ES_INDEX_NAME": "cg-duplication-questions_batch_v3", #"cg-duplication-questions_test_index", "cg-duplication-questions_batch_v3_reindexed", "cg-duplication-questions_batch_v3_for_annotation"
	"CQI_CB_ES": "10.144.131.98",
}

KAFKA_CONFIG = {
	"prod": {
		"kafka.bootstrap.servers": '10.144.131.17:9092,10.144.131.18:9092,10.144.131.19:9092',
		"subscribe": 'cqi_deduplication_cg_delta_prod',
		"maxOffsetsPerTrigger": 50000,
		# "startingOffsets": "earliest",
		# "endingOffsets": "latest",
		# "enable.auto.commit": True,
	},
	"staging": {
		"kafka.bootstrap.servers": '10.144.20.35:9092,10.144.20.37:9092,10.144.20.38:9092',
		"subscribe": 'cqi_deduplication_cg_delta', # cqi_deduplication_cg_delta_staging
		"maxOffsetsPerTrigger": 50000,
		# "startingOffsets": "earliest",
		# "kafka.group.id": "",
		"startingOffsets": """ {"cqi_deduplication_cg_delta":{"2":6950302,"1":6951291,"0":7010335}} """
		# "endingOffsets": "latest",
		# "enable.auto.commit": True
	}
}


# "startingOffsets": """ {"cqi_deduplication_cg_delta":{"0":1020911,"1":1020911,"2":1020911}} """

# 2021-03-19 13:34:52.477
# 2021-03-19 14:41:05.263
# 2021-03-19 14:41:05.263