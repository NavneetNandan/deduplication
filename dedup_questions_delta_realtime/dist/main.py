import time
import os
import sys
from pyspark.sql import SparkSession

if os.path.exists('utilities.zip'):
    sys.path.insert(0, 'utilities.zip')
else:
    sys.path.insert(0, './utilities')

from utilities.driver import Dedup

if __name__ == '__main__':
    spark_session = (SparkSession
    .builder
    .appName('CQI Deduplication Incremental Pipeline')
    .getOrCreate())
    
    kafka_env = "staging" # staging or prod
    obj = Dedup(spark_session, kafka_env)
    obj.driver()