from sqlalchemy import create_engine,text
import sys
sys.path.append('/home/ubuntu')
import pandas as pd
from bs4 import BeautifulSoup
import ast
import re
import html
import math
import numpy as np
import math
import json
from pprint import pprint
from elasticsearch import Elasticsearch,helpers
es = Elasticsearch("10.144.20.4")
#es.indices.create('cqi-duplicate-contentadmin')
num_threads = 100
expected_output = []
#from lsh import cache, minhash
#hasher = minhash.MinHasher(seeds=1000, char_ngram=5, hashbytes=4)
#lshcache = cache.Cache(num_bands=200, hasher=hasher)
import Levenshtein
import csv
#import networkx as nx
import threading

from sqlalchemy import create_engine,text
import pandas as pd
from bs4 import BeautifulSoup
import ast
import re
import html
import math
import numpy as np
import time
import threading
num_threads = 500
expected_output = []
from elasticsearch import Elasticsearch,helpers
#es = Elasticsearch("10.140.10.7")
#from lsh import cache, minhash
#hasher = minhash.MinHasher(seeds=1000, char_ngram=5, hashbytes=4)
#lshcache = cache.Cache(num_bands=200, hasher=hasher)
import Levenshtein
import csv
#import networkx as nx
#from connection import DBConnection


'''connection_obj = DBConnection()
connection = connection_obj.get_ca_connection()
# engine = create_engine("mysql+pymysql://root:P0o9i8u7lpkojihu@content-admin-production-read-replica.crdfxi46vdem.ap-southeast-1.rds.amazonaws.com/content_admin")
# connection = engine.connect()

question_answer_df = pd.read_sql_query('select q.id,q.question_code,q.questioninfo, a.answerinfo,a.is_correct from questions q inner join answers a on a.question_id = q.id where a.is_correct = true',connection)
print("length question_answer_df",question_answer_df.question_code.nunique())
question_answer_df.to_csv("content_admin_uncleaned_data.csv")
question_test_mode = pd.read_csv("/home/ubuntu/cqi/questionHygiene/Views/CA2019-03-27data.csv")
print("length question_test_mode",question_answer_df.question_code.nunique())
question_test_mode.question_mode.fillna(0,inplace = True)

def is_practice(x):
    if x == "Practice":
        return 1
    else:
        return 0

def is_test(x):
    if x == "Test":
        return 1
    else:
        return 0

question_test_mode['is_practice'] = question_test_mode.question_mode.apply(lambda x: is_practice(x))

question_test_mode['is_test'] = question_test_mode.question_mode.apply(lambda x: is_test(x))

def tag_exams(x):
    if x !='{nan}':
        main_list = []
        list_of_exams = list(ast.literal_eval(x))
        for exam in list_of_exams:
            dicti = {"exam_id":exam}
            main_list.append(dicti)
        return main_list
    else:
        return [{'exam_id': -1}]

question_test_mode['exams'] = question_test_mode.exams_tagged.apply(lambda x:tag_exams(x))

question_test_mode.is_previous_year_qn.fillna(False,inplace = True)

question_test_mode.is_previous_year_qn.replace(False, 0, inplace=True)

question_test_mode.rename(index=str, columns={"is_previous_year_qn": "is_prev_year"},inplace=True)


question_answer_df.rename(index=str, columns={"questioninfo": "question_info","answerinfo":"answers"},inplace=True)

question_answer_test_df = pd.merge(question_answer_df,question_test_mode,left_on="question_code",right_on="qcode")

print("length question_answer_test_df",question_answer_df.question_code.nunique())

question_answer_test_df_5 = question_answer_test_df[["question_code","question_info","answers","is_practice","created_at","is_test","exams","is_prev_year"]]


cqi_engine = create_engine('mysql+pymysql://dslembibe:Dslembibe$1234@dslmysql.crdfxi46vdem.ap-southeast-1.rds.amazonaws.com/cqi')
cqi_connection = cqi_engine.connect()


question_learningmap_df  = pd.read_sql('question_learning_map',cqi_engine)
print("question_learningmap_df",question_learningmap_df.question_code.nunique())
question_learningmap_df.to_csv("question_learning_map_CA.csv")


all_contents = pd.merge(question_answer_test_df_5,question_learningmap_df,on="question_code",how='left')
print(print("total_overall",all_contents.question_code.nunique()))
all_contents_new = all_contents.groupby('question_code').agg({'question_info':'first', 'answers': 'first','subject':'first','is_test':'first','is_practice':'first','created_at':'first','exams':'first','is_prev_year':'first'})
print(all_contents_new.subject.value_counts())
all_contents.to_csv("all_uncleaned_text.csv")
def Parse_Question_Text(question_info_object):
    qcontent = re.sub('&nbsp;',' ',question_info_object).replace('&there4;', ' ')
    return qcontent


def clean(text):
    text = str(text)
    if not text:
        return "No explanation_given"
    else:
        soup = BeautifulSoup(text,'html.parser')
        soup.prettify()
        text = soup.get_text()
        text = text.replace("\n"," ")
        text = text.replace("\r"," ")
        text = text.replace("  "," ")
        text = text.replace("   "," ")
        return text

def cleaning_content(qcontent):
    ##remove html/mathml tags
    try:
        qcontent = re.sub('<[^>]*>',' ',qcontent).replace('&nbsp;', ' ').replace('&there4;', ' ').replace('&#39;','').replace('<sup>','').replace('</sup>','').replace('<br>','').replace('<br />','')
    except Exception as e:
        pass
    return qcontent

def get_symbols(mstring):
    list_symbols=[]
    list_items = mstring.split()
    for item in list_items:
        if(len(item)==1):
            if((re.match('[a-z]',item) == None) and (item!=' ') and (item not in ['.',',','\'',';','?','\u2061'])):
                list_symbols.append(item)
                
    list_symbols = list(set(list_symbols))
    return list_symbols


def clean_data(string):
    string = string.split(' ')
    unwanted = []
    for i in string:
        if(len(i) == 1):
            if(ord(i)>128 and i not in list_symbols):
                unwanted.append(i)
    unwanted = list(unwanted)
    for i in unwanted:
        string.remove(i)
    string = ' '.join(string)
    return string


all_contents_new['question_info_clean'] = all_contents_new['question_info'].apply(lambda x: Parse_Question_Text(x))

all_contents_new.dropna(subset=['question_info_clean'],inplace=True)

all_contents_new['question_info_clean'] = all_contents_new['question_info_clean'].apply(lambda x: clean(x))

all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean'].apply(lambda x: cleaning_content(x))

def remove_unicode_content(qcontent):
    qcontent = re.sub('\u2061','',qcontent)
    return qcontent

all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(lambda x: remove_unicode_content(x))

all_contents_new = all_contents_new.reset_index()

all_contents_new['symbols'] = all_contents_new['question_info_clean_complete'].apply(lambda x: get_symbols(x))
list_of_list = all_contents_new['symbols'].tolist()
flat_list = [item for sublist in list_of_list for item in sublist]
list_symbols  = list(set(flat_list))

all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(lambda x: clean_data(x))

all_contents_new['answer_info'] = all_contents_new['answers'].apply(lambda x: Parse_Question_Text(x))

all_contents_new['correct_option'] = all_contents_new['answer_info'].apply(lambda x: clean(x))


def remove_page_margin(content):
    if content.startswith("<!--"):
        return ""
    else:
        return content


all_contents_new['correct_option'] = all_contents_new['correct_option'].apply(lambda x: remove_page_margin(x))

all_contents_new_removed_dup = all_contents_new[~all_contents_new['question_code'].str.contains("DUP")]

all_contents_new_columns = all_contents_new_removed_dup[["question_code","subject","is_test","is_practice","created_at","exams","is_prev_year","question_info_clean_complete","correct_option"]]

all_contents_new_columns["question_answer_clean"] = all_contents_new_columns["question_info_clean_complete"] +" "+ all_contents_new_columns["correct_option"]

all_contents_new_columns["is_test"].fillna(0, inplace=True)
all_contents_new_columns["is_practice"].fillna(0, inplace=True)

jsonfile = all_contents_new_columns.to_json(r"new_content_data",orient='records')



with open('new_content_data') as f:
    data = json.load(f)

k = ({'_type':'my_type', '_index':'cqi-duplicate-contentadmin','question_code':quest_data['question_code'],'question_ans_clean':quest_data['question_answer_clean'],'question':quest_data['question_info_clean_complete'],'correct_option':quest_data['correct_option'],'is_test':quest_data['is_test'],'is_practice':quest_data['is_practice'],'subject':quest_data['subject'],'created_at':quest_data['created_at'],'exams':quest_data['exams'],'is_prev_year':quest_data['is_prev_year']}
     for quest_data in data)

helpers.bulk(es,k)'''

all_contents_new_columns = pd.read_json('new_grail_data', orient='records')
all_contents_new_columns = all_contents_new_columns[all_contents_new_columns["subject"] == "Physics"]
print(all_contents_new_columns.columns)
list_production = all_contents_new_columns.values.tolist()
print(len(list_production))
def lavenstein_ratio(set_a, set_b):
    return Levenshtein.ratio(set_a, set_b)

def fetch_Laven(result_df):
    if str(result_df.duplicate_question_text) != ' ':
        return lavenstein_ratio(str(result_df.original_question_text),str(result_df.duplicate_question_text))
    else:
        return 0
def calculate_laven_ratios(formated_output):
    laven_df = pd.DataFrame(formated_output,columns=['original_question_code','original_question_text','duplicate_question_code','duplicate_question_text'])
    laven_df['Laven_ratios'] = laven_df.apply(fetch_Laven,axis=1)
    laven_df = laven_df[laven_df['Laven_ratios']>0.59]
    if laven_df.empty:
        return []
    else:
        laven_df_quest = laven_df[['original_question_code','duplicate_question_code',"Laven_ratios"]]
        laven_list = laven_df_quest.values.tolist()
        return laven_list

def find_dups(query,finalresult,length,question_code):
    ids=[]
    for result,score in finalresult:
        if result['question_code'] == question_code:
            continue
        ids.append((result['question_code'],result['question_ans_clean'],result['question'],result['correct_option']))
    return ids


def search(question_text,answer_text,question_code,expected_output):

    #search_query = { "query": {"match_phrase_prefix": {"question_ans_clean": {"query": query, "slop": 15, "max_expansions": 5}}}}
    if answer_text:
        query = str(question_text) + " " + str(answer_text)
        length = len(query)
         #complex_query['query']['match'].append({'question_ans_clean': {'query': question_answer_clean}})
        field = "question_ans_clean"
    else:
        query = question_text
        length = len(query)
        #complex_query['query']['match'].append({'question': {'query': question_answer_clean}})
        field = "question"
    complex_query = {
        "size" : 50,
        "query": {
            "match": {
                field: {
                    "query": query
                }
            }
        }
    }
    search = es.search(index='cqi-duplicate-grail-staging', doc_type='my_type', body=complex_query)
    print(question_code)
    finalresult = []
    for res in search['hits']['hits']:
        result = res['_source']
        score = res['_score']
        finalresult.append([result, score])
    output = find_dups(query, finalresult, length, question_code)
    if len(output) >= 1:
        formated_output = [(question_code,question_text,x[0],x[2]) for x in output]
        formated_laven = calculate_laven_ratios(formated_output)
        expected_output.extend(formated_laven)

    else:
        return 

threads = [threading.Thread(target=search, args=(items[9],items[0],items[7],expected_output)) for items in list_production]
    
for i in range(0,int(len(threads)/num_threads)+1):
    [ti.start() for ti in threads[i*num_threads:(i+1)*num_threads]]
    [ti.join() for ti in threads[i*num_threads:(i+1)*num_threads]]

candidate_df = pd.DataFrame(expected_output,columns=['original_question_code','duplicate_question_code',"Laven_ratios"])
candidate_df.to_csv("multi_quest_CA.csv")
main_question_text= pd.merge(all_contents_new_columns, candidate_df, left_on = 'question_code', right_on = 'original_question_code')
print("1st")
main_question_text = main_question_text.drop(columns=['question_code','created_at','exams','is_prev_year','is_practice','is_test'])
main_question_text = main_question_text.rename(index=str, columns={"question_answer_clean": "original_question_answer_clean","question_info_clean_complete":"original_question_info_clean_complete","correct_option":"original_correct_option"})
duplicate_question_text = pd.merge(all_contents_new_columns, main_question_text, left_on = 'question_code', right_on = 'duplicate_question_code')
print("2nd")
duplicate_question_text = duplicate_question_text.rename(index=str, columns={"question_answer_clean": "duplicate_question_answer_clean","question_info_clean_complete":"duplicate_question_info_clean_complete","correct_option":"duplicate_correct_option"})

duplicate_question_text = duplicate_question_text.drop(columns=['question_code','created_at','exams','is_prev_year','is_practice','is_test'])
duplicate_question_text.to_csv("multi_quest_grail_phy.csv")
def fetch_Laven_ratios(result_df):
    return lavenstein_ratio(str(result_df.original_question_answer_clean),str(result_df.duplicate_question_answer_clean))


def fetch_jaccard_ratios(result_df):
    shingles_a = shingles(result_df.original_question_answer_clean)
    shingles_b = shingles(result_df.duplicate_question_answer_clean)
    jaccard_sim = jaccard(shingles_a, shingles_b)
    return jaccard_sim
    
def fetch_Minhash_ratios(result_df):
    fingerprint_a = set(hasher.fingerprint(str(result_df.original_question_answer_clean).encode('utf8')))
    fingerprint_b = set(hasher.fingerprint(str(result_df.duplicate_question_answer_clean).encode('utf8')))
    minhash_sim = len(fingerprint_a & fingerprint_b) / len(fingerprint_a | fingerprint_b)
    return minhash_sim

def shingles(text, char_ngram=5):
    text = str(text)
    return set(text[head:head + char_ngram] for head in range(0, len(text) - char_ngram))


def lavenstein_ratio(set_a, set_b):
    return Levenshtein.ratio(set_a, set_b)


def jaccard(set_a, set_b):
    intersection = set_a & set_b
    union = set_a | set_b
    if len(union) != 0:
        return len(intersection) / len(union)
    else:
        return 0

#duplicate_question_text['Minhash_ratios'] = duplicate_question_text.apply(fetch_Minhash_ratios,axis=1)
#print("minhash")
#duplicate_question_text['jaccard_ratios'] = duplicate_question_text.apply(fetch_jaccard_ratios,axis=1)
#print("jaccard")
#duplicate_question_text['Laven_ratios'] = duplicate_question_text.apply(fetch_Laven_ratios,axis=1)
print("laven")
#duplicate_question_text.to_csv("multi_grail.csv")

'''f = pd.read_csv("mutli_o.csv")

df_maths = df[df["subject_x"] == "Mathematics"]
print(df_maths.shape)
df_maths.to_csv("Maths.csv")
df_phy = df[df["subject_x"] == "Physics"]
print(df_phy.shape)
df_phy.to_csv("physics.csv")

df_chem = df[df["subject_x"] == "Chemistry"]
print(df_chem.shape)
df_chem.to_csv("chemistry.csv") 



df_bio = df[df["subject_x"] == "Biology"]
print(df_bio.shape)
df_bio.to_csv("biology.csv")'''