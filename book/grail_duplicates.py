from sqlalchemy import create_engine,text
import sys
sys.path.append('/home/ubuntu')
import pandas as pd
from bs4 import BeautifulSoup
import ast
import re
import html
import math
import numpy as np
import math
import json
from pprint import pprint
from elasticsearch import Elasticsearch,helpers
es = Elasticsearch("10.144.20.4")
num_threads = 100
expected_output = []
import Levenshtein
import csv
import threading
import numpy as np
import time
import networkx as nx
import csv
from itertools import combinations
import operator

def lavenstein_ratio(set_a, set_b):
    return Levenshtein.ratio(set_a, set_b)

def fetch_Laven(result_df):
    if str(result_df.duplicate_question_text) != ' ':
        return lavenstein_ratio(str(result_df.original_question_text),str(result_df.duplicate_question_text))
    else:
        return 0

def calculate_laven_ratios(formated_output):
    laven_df = pd.DataFrame(formated_output,columns=['original_question_code','original_question_text','duplicate_question_code','duplicate_question_text'])
    laven_df['Laven_ratios'] = laven_df.apply(fetch_Laven,axis=1)
    laven_df = laven_df[laven_df['Laven_ratios']>0.59]
    if laven_df.empty:
        return []
    else:
        laven_df_quest = laven_df[['original_question_code','duplicate_question_code',"Laven_ratios"]]
        laven_list = laven_df_quest.values.tolist()
        return laven_list

def find_dups(query,finalresult,length,question_code):
    ids=[]
    for result,score in finalresult:
        if result['question_code'] == question_code:
            continue
        ids.append((result['question_code'],result['question_ans_clean'],result['question'],result['correct_option']))
    return ids


def search(question_text,answer_text,question_code,expected_output):
    if answer_text:
        query = str(question_text) + " " + str(answer_text)
        length = len(query)
        field = "question_ans_clean"
    else:
        query = question_text
        length = len(query)
        field = "question"
    complex_query = {
        "size" : 50,
        "query": {
            "match": {
                field: {
                    "query": query
                }
            }
        }
    }
    search = es.search(index='cqi-duplicate-grail', doc_type='my_type', body=complex_query,request_timeout=30)
    finalresult = []
    for res in search['hits']['hits']:
        result = res['_source']
        score = res['_score']
        finalresult.append([result, score])
    output = find_dups(query, finalresult, length, question_code)
    if len(output) >= 1:
        formated_output = [(question_code,question_text,x[0],x[2]) for x in output]
        formated_laven = calculate_laven_ratios(formated_output)
        expected_output.extend(formated_laven)

    else:
        return 

def compute_dups_maths(multi_prod_maths,subject):
    duplicate_pairs_list = []
    for index, row in multi_prod_maths.iterrows():
        if row["Laven_ratios"]>0.59:
            if (row["original_correct_option"]!= None) or (row["original_correct_option"] != ' '):
                laven_ratio_answer = lavenstein_ratio(str(row["original_correct_option"]),str(row['duplicate_correct_option']))
                if laven_ratio_answer>=0.80:
                    duplicate_pairs_list.append((row["original_question_code"],row["duplicate_question_code"]))
            else:
                duplicate_pairs_list.append((row["original_question_code"],row["duplicate_question_code"]))
                
    G_dup = nx.Graph()
    G_dup.add_edges_from(duplicate_pairs_list)
    clusters = list(nx.connected_component_subgraphs(G_dup))
    cluster_outcsv_90 = open('duplicate_question_clusters_production_elastic_search_'+str(subject)+".csv",'w')
    cluster_writer_90 = csv.writer(cluster_outcsv_90)
    cluster_writer_90.writerow(['cluster_question_codes'])
    for i in range(len(clusters)):
        cluster_writer_90.writerow([', '.join(clusters[i].nodes())])
    cluster_outcsv_90.close()




def start_finding_dup():
    book_df = pd.read_json("new_grail_book",orient="records")
    print(book_df.shape)
    book_data = pd.read_csv("question_codes_list.csv")
    question_code_list = book_data["question_codes"].tolist()
    new_book_df = book_df[book_df["question_code"].isin(question_code_list)]
    print(new_book_df.shape)
    all_contents_new_columns = new_book_df[new_book_df["question_images"].apply(lambda x: x[0]["image_0"]=="No images")]
    all_contents_new_columns = all_contents_new_columns[all_contents_new_columns["answer_images"].apply(lambda x: x[0]["image_0"]=="No images")]
    print(all_contents_new_columns.columns)
    list_production = all_contents_new_columns.values.tolist()
    print(len(list_production))
    threads = [threading.Thread(target=search, args=(items[10],items[1],items[8],expected_output)) for items in list_production]
    for i in range(0,int(len(threads)/num_threads)+1):
        [ti.start() for ti in threads[i*num_threads:(i+1)*num_threads]]
        [ti.join() for ti in threads[i*num_threads:(i+1)*num_threads]]
    
    candidate_df = pd.DataFrame(expected_output,columns=['original_question_code','duplicate_question_code',"Laven_ratios"])

    candidate_df.to_csv("multi_quest_CA_book.csv")

    #old_candidate_df = pd.read_csv("multi_quest_CA.csv")
    #newly_merged_df = pd.concat( [old_candidate_df, candidate_df],ignore_index=True)
    #newly_merged_df.to_csv("multi_quest_CA.csv")

    main_question_text= pd.merge(book_df, candidate_df, left_on = 'question_code', right_on = 'original_question_code')
    print("1st")
    main_question_text = main_question_text.drop(columns=['question_code','created_at','exams','is_prev_year','is_practice','is_test'])
    main_question_text = main_question_text.rename(index=str, columns={"question_answer_clean": "original_question_answer_clean","question_info_clean_complete":"original_question_info_clean_complete","correct_option":"original_correct_option","answer_images":"original_answer_images","question_images":"original_question_images","question_type":"original_question_type","subject":"original_subject"})
    duplicate_question_text = pd.merge(book_df, main_question_text, left_on = 'question_code', right_on = 'duplicate_question_code')
    print("2nd")
    duplicate_question_text = duplicate_question_text.rename(index=str, columns={"question_answer_clean": "duplicate_question_answer_clean","question_info_clean_complete":"duplicate_question_info_clean_complete","correct_option":"duplicate_correct_option","answer_images":"duplicate_answer_images","question_images":"duplicate_question_images","question_type":"duplicate_question_type","subject":"duplicate_subject"})
    duplicate_question_text = duplicate_question_text.drop(columns=['question_code','created_at','exams','is_prev_year','is_practice','is_test'])
    duplicate_question_text.to_csv("multi_grail.csv")

    '''phy_df = duplicate_question_text[duplicate_question_text["original_subject"]=="Physics"]
    maths_df = duplicate_question_text[duplicate_question_text["original_subject"]=="Mathematics"]
    chem_df = duplicate_question_text[duplicate_question_text["original_subject"]=="Chemistry"]
    bio_df = duplicate_question_text[duplicate_question_text["original_subject"]=="Biology"]'''

    '''compute_dups_maths(phy_df,"physics")
    compute_dups_maths(maths_df,"maths")
    compute_dups_maths(chem_df,"chemistry")
    compute_dups_maths(bio_df,"biology")'''
    compute_dups_maths(duplicate_question_text,"dummy")








start_finding_dup()










