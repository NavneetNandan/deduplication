from flask import Flask, jsonify,request, Response,abort,make_response,json,current_app
import question_dup
import fetch_text_from_code
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/find_annotation',methods=['POST','GET'])
def get_Duplicates():
    input_data = request.get_json()
    question_code = input_data.get('question_id', None)
    question_text = input_data.get('question_content', None)
    source = input_data.get('source_db', None)
    answer_text = input_data.get('answer', None)
    subject = input_data.get('subject', None)

    
    # Do processing with input_data here..
    if not question_text or question_text == "":
       question_code_list = [i.strip() for i in question_code]
       print(question_code_list)
       try:
           print("i am in try")
           total_question_codes = fetch_text_from_code.iterate_question_codes(question_code_list,source)
           print(total_question_codes)
       except:
           #response = {"response":[{"output":"There was some error while fetching data from database"}],"status":"error"}
           #print(response)
           print("i am in except")
           
       response = question_dup.find_by_code(total_question_codes,source,subject)

       
       if not response:
           print({"response":[{"output":"There are No Duplicates for this question"}],"status":"success"})
       return jsonify({"status":"success","message":"","data":response})
    else:
        response = question_dup.start_finding_dup(question_code, question_text,answer_text,source,subject)
        if response["status"] == "error" and response["output"] == "There is some problem with elastic search":
            response = question_dup.start_finding_dup(question_code, question_text, answer_text,source,subject)
        response_array =[]
        response_array.append(response)
        return jsonify({"status":"success","message":"","data":{"response":response_array}})


@app.route('/ingest_dups',methods=['POST','GET'])
def get_data_for_ingestion():
    input_data = request.get_json()
    query_code = input_data.get('query_code', None)
    dup_code = input_data.get('dup_code', None)
    annotator_name = input_data.get('annotator_name', None)
    print("input_data",input_data)
    print("query_code",query_code)
    return jsonify({"status":"success","message":"Duplicate Codes are ingested in database Successfully"})


if __name__ == '__main__':
    app.run(debug=False , host = "0.0.0.0", port="9005")
