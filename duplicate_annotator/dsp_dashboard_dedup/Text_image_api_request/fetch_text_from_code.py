import pandas as pd
from sqlalchemy import create_engine,text
engine = create_engine("mysql+pymysql://root:P0o9i8u7lpkojihu@content-admin-production-read-replica.crdfxi46vdem.ap-southeast-1.rds.amazonaws.com/content_admin")
connection = None

production_engine = create_engine("postgresql://finalstep:postgres.embibe@2016@postgres-production-reborn-read-replica.crdfxi46vdem.ap-southeast-1.rds.amazonaws.com/finalstep_theone")
prod_connection = None
import ast

ssl = {'ssl': {'ca': 'BaltimoreCyberTrustRoot.crt.pem'}}
grail_engine = create_engine("mysql+pymysql://mysqladmin@embibemysqlsvr-stg:Embibe@123456!@embibemysqlsvr-stg.mysql.database.azure.com/contentgrail",connect_args=ssl)
grail_connection = None



def iterate_question_codes(list_of_question_code,source):
    global connection
    connection = engine.connect()
    global prod_connection
    prod_connection = production_engine.connect()
    global grail_connection
    grail_connection = grail_engine.connect()

    final_result = []
    grail_list = []
    if source == "content-admin":
        for question_code in list_of_question_code:
            result_tuple = get_data_from_content_admin(question_code)
            final_result.append(result_tuple)
        connection.close()
        return final_result
    elif source == "production":
        for question_code in list_of_question_code:
            result_tuple = get_data_from_production(question_code)
            final_result.append(result_tuple)
        prod_connection.close()
        return final_result

    elif source == "content-grail":
        for question_code in list_of_question_code:
            result_tuple = get_data_from_content_grail(question_code)
            if result_tuple[2]=="":
                final_result.append((question_code,"",""))
            else:
                grail_list.append(result_tuple)
                grail_df = pd.DataFrame(grail_list,columns=["question_code","question_text","answers","correctness"])
                correct_answer = get_answer(grail_df)
                final_tuple = (result_tuple[0],result_tuple[1],correct_answer)
                final_result.append(final_tuple)
        grail_connection.close()
        return final_result


        
def get_data_from_content_admin(question_code):
    try:
        result = connection.execute('select q.id,q.question_code,q.questioninfo, a.answerinfo,a.is_correct from questions q inner join answers a on a.question_id = q.id where a.is_correct = true AND q.question_code = %s ',(question_code))
        print(result)
        for row in result:
            question_text = row[2]
            answer_text = row[3]
        return (question_code,question_text,answer_text)
    except:
        return (question_code,"","")

def get_data_from_production(question_code):
    try:
        result = prod_connection.execute('select q1.question_code, q1.m_version, q1.answers,q1.question_info from (select cv.question_code,cv.version as m_version,cv.answers as answers,cv.bodies as question_info from content_qvers cv,(select question_code, max(version) as max_version from content_qvers group by question_code) as t where t.max_version = cv.version and cv.question_code=t.question_code) as q1 where question_code = %s ',(str(question_code)))
        for row in result:
            question_text = row[3]
            answer_text = row[2]
        return (question_code,question_text,answer_text)
    except:
        return (question_code,"","")


def get_data_from_content_grail(question_code):
    try:
        result = grail_connection.execute("SELECT distinct(question_code),version,subtype,content->'$.question_details.en.question_txt' as question_text,purpose,content->'$.question_details.en.answers[*].body' as answers,content->'$.question_details.en.answers[*].is_correct' as correctness,content->'$.question_meta_tags.topics_learn_path_code' as exams,content->'$.question_meta_tags.topics_learn_path_name' as subjects FROM learning_objects lo WHERE type='Question' and question_code is not NUll and version = (select max(version) from learning_objects where question_code = lo.question_code) AND question_code = %s ",(question_code))
        for row in result:
            question_text = row[3]
            answer_text = row[5]
            correctness = row[6]
        return (question_code,question_text,answer_text,correctness)
    except:
        print("Question_____code ",question_code)
        return (question_code,"","","")



def get_answer(df):
    df['correctness'].fillna("False",inplace=True)
    df['correctness_1'] = df.apply(lambda x : boolean_eval(x.correctness), axis = 1)
    df['answers'].fillna('[]',inplace=True)
    df['answers_1'] = df['answers'].apply(lambda x: get_answer_string(x))
    df["combined_answers"] = df.apply(join_answers, axis=1)
    return df["combined_answers"].iloc[:1].values[0]

def boolean_eval(bool_string):
    try:
        bool_string = bool_string.replace('t','T')
        bool_string = bool_string.replace('f','F')
        
        bool_string = bool_string.replace('null','False')
    except:
        print(bool_string)
    return ast.literal_eval(bool_string)

def get_answer_string(x):
    try:
        return ast.literal_eval(x)
    except:
        print("I am printing")
        return ""

def join_answers(row):
    try:
        answers_list = []
        if len(row["correctness_1"]) == 0:
            return ""
    
        for idx, val in enumerate(row["correctness_1"]):
            if val == True:
                answers_list.append(row["answers_1"][idx])
        s = ' '
        return s.join(answers_list)
    except:
        print(row)