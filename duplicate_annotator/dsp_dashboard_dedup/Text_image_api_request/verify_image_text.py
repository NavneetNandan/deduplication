from elasticsearch import Elasticsearch,helpers
es=Elasticsearch([{'host':'10.144.20.4','timeout':500}])
import question_dup
import Levenshtein
import ast

def get_verified_image_text(question_text, answer_text,source,subject,image_dup_list,question_image_url,org_question_code):
    cleaned_question_text,cleaned_answer_text = question_dup.preprocessing(question_text,answer_text,source,subject)
    if not image_dup_list:
        return {"input": {'question_id':org_question_code,'question_content': cleaned_question_text + " " + cleaned_answer_text,"question_image_url":question_image_url},"output": [],"status":"success"}

    image_text_result = get_question_answer_for_dup(image_dup_list,source)
    final_output = []
    for quest_detail in image_text_result:
        dup_question_ans = str(quest_detail[1]) + " " +str(quest_detail[2])
        org_quest_ans = cleaned_question_text + " " + cleaned_answer_text
        ratio = lavenstein_ratio(org_quest_ans,dup_question_ans)
        
        if ratio >=0.60:
            final_output.append({"duplicate_code":quest_detail[0],"duplicate_question": quest_detail[1],"duplicate_answer": quest_detail[2],"weighted_laven_minhash": ratio,"duplicate_question_image":quest_detail[3]})
        
        
    return {"input": {'question_id':org_question_code,'question_content': cleaned_question_text + " " + cleaned_answer_text,"question_image_url":question_image_url},"output": final_output,"status":"success"}

    
def lavenstein_ratio(set_a, set_b):
    return Levenshtein.ratio(set_a, set_b)

def search(question_code,source):
    complex_query = {
        "query": {
            "match": {
                "question_code": {
                    "query": question_code
                }
            }
        }
    }

    try:
        if source == 'content-admin':
            index_name = 'cqi-duplicate-contentadmin'
        elif source == 'production':
            index_name = 'cqi-duplicate-production'
        elif source == "content-grail":
            index_name = 'cqi-duplicate-grail'
        
        search = es.search(index=index_name, doc_type='my_type', body=complex_query)
        question = search['hits']['hits'][0]['_source']["question"]
        answer = search['hits']['hits'][0]['_source']["correct_option"]
        duplicate_question_image_url = ast.literal_eval(search["hits"]["hits"][0]["_source"]["question_images"])[0]["image_0"]
        return (question_code,question,answer,duplicate_question_image_url)
    except:
        return 0

def get_question_answer_for_dup(image_dup_list,source):
    result = []
    for question in image_dup_list:
        question_code = question.split(".")[0]
        tuple_quest_answer= search(question_code,source)
        result.append(tuple_quest_answer)
    
    return result



