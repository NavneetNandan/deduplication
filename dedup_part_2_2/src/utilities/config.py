appName = "CQI-Deduplication"
VERSION = "batch_v3" # Keep this in Small Letters only

PATHS = {
	"base": "wasb:///user/sshuser/data_dedup_modularized"
}

DATABASES = {
	"mongo":{
		"spark.mongodb.input.uri":"mongodb://ro_dsl:EHJpUwVO2vgMuk@tech-contentgrail-mongo-prod-01:27017,tech-contentgrail-mongo-prod-02:27017,tech-contentgrail-mongo-prod-03:27017,tech-contentgrail-mongo-prod-04:27017/?authSource=contentgrail&replicaSet=cg-mongo-prod",
		"spark.mongodb.input.database": "contentgrail",
		"spark.mongodb.input.collection": "",
		"spark.mongodb.input.readPreference.name": "primaryPreferred"
	},
	"ES": {
		"es.nodes": "10.141.11.88,10.141.11.89,10.141.11.90",
		"es.port" : '9200',
		"es.nodes.wan.only": "true",
		"es.resource": ""
	},
	"ES_CLUSTER_NODES": ["10.141.11.88","10.141.11.89","10.141.11.90"],
	"CQI_CB_ES": "10.144.131.98"
}