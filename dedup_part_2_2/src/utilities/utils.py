import pyspark.sql.functions as F
import pyspark.sql.types as t
from pyspark.sql import Row

import re
from bs4 import BeautifulSoup
from pylatexenc.latex2text import LatexNodes2Text
import math 
import Levenshtein
from datetime import datetime,timezone
from elasticsearch import Elasticsearch, helpers

import utilities.select_candidates as sc

def fetch_questions_index_and_store_on_HDFS(spark,es,es_index,all_data_dump):
    result = helpers.scan(
        es,
        query={
            "query":{"match_all":{}},
            "_source": {
                "exclude": ["timestamp"]
            }
            },
        size=10000,scroll = '2h',index= es_index
    )

    temp = []
    chunk_number = 0

    for x in result:
        temp.append(x["_source"])

        if len(temp)==60000:
            print("len",len(temp))
            grail_dataframe = spark.createDataFrame(Row(**y) for y in temp)

            if chunk_number == 0:
                grail_dataframe.write.mode("overwrite").parquet(
                    all_data_dump
                )
            else:
                grail_dataframe.write.mode("append").parquet(
                    all_data_dump
                )

            del grail_dataframe
            
            chunk_number += 1
            temp = []

    if len(temp)<60000:
        grail_dataframe = spark.createDataFrame(Row(**y) for y in temp)

        grail_dataframe.write.mode("append").parquet(
            all_data_dump
        )
    
        del grail_dataframe

def Parse_Question_Text(question_info_object):
    try:
        qcontent = re.sub('&nbsp;', ' ', question_info_object).replace('&there4;', ' ')
        return qcontent
    except:
        return question_info_object

def clean(text):
    try:
        text = str(text)
        if not text:
            return "No explanation_given"
        else:
            text = re.sub(r"<gdiv.*?>(.|\r|\n)*?<\/gdiv>","",text)
            text = re.sub(r"(<\/gdiv>)","",text)

            soup = BeautifulSoup(text, 'html.parser')
            soup.prettify()
            soup_text = soup.get_text()
            soup_text = soup_text.replace("\\\\", "\\")
            soup_text = soup_text.replace("\\n", " ")
            try:
                soup_text = LatexNodes2Text(keep_comments=True,math_mode="with-delimiters").latex_to_text(soup_text,tolerant_parsing=True) # %
                if soup_text[-1]=="$":
                    soup_text = soup_text[:-1]
            except:
                print(soup_text)
                soup_text = soup_text.strip('\"').replace('\\\\', '\\').replace('\\"', '\"').replace('\\n','\n').replace("\\r", "\r").replace("\\t", "\t")
                try:
                    soup_text = LatexNodes2Text(keep_comments=True,math_mode="with-delimiters").latex_to_text(soup_text,tolerant_parsing=True)
                    if soup_text[-1]=="$":
                        soup_text = soup_text[:-1]
                except:
                    pass

            soup_text = soup_text.strip('\"')
            # soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            # soup_text = re.sub(r"(^Q.\w+.:)", '', soup_text)
            # soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            # soup_text = re.sub(r"(^Ans\w+.)", '', soup_text)
            soup_text = soup_text.replace("\n", " ")
            soup_text = soup_text.replace("\r", " ")
            soup_text = soup_text.replace("  ", " ")
            soup_text = soup_text.replace("   ", " ")
            soup_text = re.sub(r'\n', "", soup_text)
            soup_text = re.sub(r'\r', "", soup_text)
    except Exception as e:
        print(e.__str__())

    return soup_text

def remove_page_margin(content):
    if content.startswith("<!--"):
        return ""
    else:
        return content

def clean_answer_options_driver(text):
    return remove_page_margin(clean(Parse_Question_Text(text.strip())))

def fetch_urls(text):
    try:
        if type(text)!=str or len(text)==0:
            return []
        else:
            return re.findall(r"src\s*=\s*[\"|'](.+?)['|\"]",text)
    except Exception as e:
        print(e.__str__())
        return []

    
schema = t.StructType([
    t.StructField("question_text", t.StringType(), False),
    t.StructField("whole_answer_body", t.StringType(), False),
    t.StructField("correct_options", t.StringType(), False),
    t.StructField("learning_maps", t.ArrayType(t.StringType()), False),
    t.StructField("question_urls", t.ArrayType(t.StringType()), False),
    t.StructField("answer_urls", t.ArrayType(t.StringType()), False)
    ])

def parse_question_details(qd):
    key = [k for k,v in qd.items() if v is not None]
    key = key[0] if len(key)>0 else "key"

    question_txt = qd.get(key,{}).get("question_txt","")
    answer_options = qd.get(key,{}).get("answers",[])
    
    if any(type(x)==list for x in answer_options):
        answer_options = [y for x in answer_options for y in x]
    
    whole_answer_body, correct_options = [], []
    for x in answer_options:
        option = x.get("body","")
        if x.get("is_correct",False)==True:
            correct_options.append(option)
        
        whole_answer_body.append(option)

    return question_txt, whole_answer_body, correct_options

@F.udf(schema)
def return_relevant_fields(content):
    try:
        true = True
        false = False
        content = eval(content)
        
        ## Question Details
        qd = content.get("question_details")
        aq = content.get("associated_questions")

        question_text, whole_answer_body, correct_options = [], [], []

        if qd:
            qt,at,cao = parse_question_details(qd)
            question_text.append(qt)
            whole_answer_body.extend(at)
            correct_options.extend(cao)
        
        if aq:
            for x in aq:
                qt,at,cao = parse_question_details(x.get('content',{}).get('question_details',{}))
                question_text.append(qt)
                whole_answer_body.extend(at)
                correct_options.extend(cao)
        ## URLs
        question_urls = fetch_urls(" ".join(question_text))
        # answer_urls = fetch_urls(" ".join(whole_answer_body))
        answer_urls = fetch_urls(" ".join(correct_options))
        
        ## question and whole answer body
        question_text = " ".join(sorted(map(clean_answer_options_driver,question_text))).strip()
        whole_answer_body = " ".join(sorted(map(clean_answer_options_driver,whole_answer_body))).strip()
        correct_options = " ".join(sorted(map(clean_answer_options_driver,correct_options))).strip()
        
        ## Question Meta
        qm = content.get("question_meta_tags",[])
        lms = []
        for x in qm:
            lms.extend(x.get("learning_maps",[]))
        lms = list(set(lms))

    except Exception as e:
        print(e.__str__())
        question_text = ""
        whole_answer_body = ""
        correct_options = ""
        lms = []
        question_urls, answer_urls = [],[]

    return t.Row(
        'question_text', 'whole_answer_body', "correct_options", "learning_maps", "question_urls", "answer_urls"
        )(
            question_text, whole_answer_body, correct_options, lms, question_urls, answer_urls
        )

def driver_fetch_relevant(df):
    df = df.withColumn(
    "content",F.to_json(F.col("content"))
    ).withColumn(
        "content",return_relevant_fields(F.col("content"))
    ).select(
        "id","_version","status","question_code","subtype","content.*"
    )
    return df


def get_learning_objects(spark, lo_coll, lo_type, status_filters):
    print("lo_coll:",lo_coll["spark.mongodb.input.collection"])

    projection = {
                '$project': {
                    'id': 1,
                    '_version': 1,
                    'question_code': 1,
                    'subtype': 1,
                    'status':1,
                    'content.question_details.en.answers.body': 1,
                    'content.question_details.en.question_txt': 1,
                    'content.question_details.en.answers.is_correct': 1,
                    # 'content.question_details.hi.answers.body': 1,
                    # 'content.question_details.hi.question_txt': 1,
                    # 'content.question_details.hi.answers.is_correct': 1,
                    'content.question_meta_tags.learning_maps':1
                }
            }
    
    projection_PCQ = {
                '$project': {
                    'id': 1,
                    '_version': 1,
                    'question_code': 1,
                    'subtype': 1,
                    'status':1,
                    'content.question_details.en.answers.body': 1,
                    'content.question_details.en.question_txt': 1,
                    'content.question_details.en.answers.is_correct': 1,
                    # 'content.question_details.hi.answers.body': 1,
                    # 'content.question_details.hi.question_txt': 1,
                    # 'content.question_details.hi.answers.is_correct': 1,
                    'content.question_meta_tags.learning_maps':1,
                    
                    'content.associated_questions.content.question_details.en.question_txt':1,
                    'content.associated_questions.content.question_details.en.answers.body':1,
                    'content.associated_questions.content.question_details.en.answers.is_correct':1
                }
            }

    pipeline1 = [
        {
            '$match': {
                'type': lo_type,
                # '_updated': {
                #     '$gte': {'$date':datetime(2021, 3, 27, 0, 0, 0, tzinfo=timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")}
                # },
                'status': {
                    '$in': status_filters
                },
                "subtype":{
                    "$nin":["MultipleDropDown","LinkedComprehension","PassageComprehension"]
                },
                # "id": {"$nin":[6653147,5310018]}, # faulty learning object schema
                "language":"en"
            }
        }, projection
    ]
    pipeline2 = [
        {
            '$match': {
                'type': lo_type,
                # '_updated': {
                #     '$gte': {'$date':datetime(2021, 3, 27, 0, 0, 0, tzinfo=timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")}
                # },
                'status': {
                    '$in': status_filters
                },
                "subtype":{
                    "$eq":"MultipleDropDown"
                },
                "language":"en"
            }
        }, projection
    ]
    pipeline_PCQ = [
        {
            '$match': {
                'type': lo_type,
                # '_updated': {
                #     '$gte': {'$date':datetime(2021, 3, 27, 0, 0, 0, tzinfo=timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")}
                # },
                'status': {
                    '$in': status_filters
                },
                "subtype":{
                    "$eq":"PassageComprehension"
                },
                "content.associated_questions.subtype":{"$ne":"MultipleDropDown"},
                "language":"en"
            }
        }, projection_PCQ
    ]


    df1 = spark.read.format("mongo").options(**lo_coll).option("pipeline",pipeline1).load()
    df2 = spark.read.format("mongo").options(**lo_coll).option("pipeline",pipeline2).load()
    df3 = spark.read.format("mongo").options(**lo_coll).option("pipeline",pipeline_PCQ).load()

    dfs = []
    for df in [df1,df2,df3]:
        if df and len(df.take(1))>0:
            print("entered the loop")
            dfs.append(df)

    union_df = spark.createDataFrame([],t.StructType([]))
    
    if len(dfs)>0:
        union_df = driver_fetch_relevant(dfs[0])
        for df in dfs[1:]:
            df = driver_fetch_relevant(df)
            union_df = union_df.union(df)

    return union_df
    
# def get_grade_wise_learning_objects(spark, lm_coll, lo_coll, lo_type, grades, a_id=None, status_filters=None):
#     lms = get_lm_codes(spark, lm_coll, grades)
#     print("Count of Relevant LM Codes:",len(lms))
#     data = get_learning_objects(spark, lo_coll, lo_type, lms, grades, a_id, status_filters)
#     return data

# 3.
class Proccessing:
    def __init__(self, grail_df=None, ca_df=None, prod_df=None):
        self.ca_df = ca_df
        self.prod_df = prod_df
        self.grail_df = grail_df
        self.prod_flag = False
        self.ca_flag = False
        self.grail_flag = False

    @staticmethod
    @F.udf(t.StringType())
    def Parse_Question_Text(question_info_object):
        try:
            question_info_object = question_info_object['en']
        except:
            pass
        try:
            qcontent = re.sub('&nbsp;', ' ', question_info_object).replace('&there4;', ' ')
            return qcontent
        except:
            return question_info_object

    @staticmethod
    @F.udf(t.StringType())
    def clean(text):
        try:
            text = str(text)
            if not text:
                return "No explanation_given"
            else:
                text = re.sub(r"<gdiv.*?>(.|\r|\n)*?<\/gdiv>","",text)
                text = re.sub(r"(<\/gdiv>)","",text)

                soup = BeautifulSoup(text, 'html.parser')
                soup.prettify()
                soup_text = soup.get_text()
                soup_text = soup_text.replace("\\\\", "\\")
                soup_text = soup_text.replace("\\n", " ")
                try:
                    soup_text = LatexNodes2Text(keep_comments=True,math_mode="with-delimiters").latex_to_text(soup_text,tolerant_parsing=True)
                    if soup_text[-1]=="$":
                        soup_text = soup_text[:-1]
                except:
                    print(soup_text)
                    soup_text = soup_text.strip('\"').replace('\\\\', '\\').replace('\\"', '\"').replace('\\n','\n').replace("\\r", "\r").replace("\\t", "\t")
                    try:
                        soup_text = LatexNodes2Text(keep_comments=True,math_mode="with-delimiters").latex_to_text(soup_text,tolerant_parsing=True)
                        if soup_text[-1]=="$":
                            soup_text = soup_text[:-1]
                    except:
                        pass

                soup_text = soup_text.strip('\"')
                # soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
                # soup_text = re.sub(r"(^Q.\w+.:)", '', soup_text)
                # soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
                # soup_text = re.sub(r"(^Ans\w+.)", '', soup_text)
                soup_text = soup_text.replace("\n", " ")
                soup_text = soup_text.replace("\r", " ")
                soup_text = soup_text.replace("↵", "")
                soup_text = soup_text.replace("  ", " ")
                soup_text = soup_text.replace("   ", " ")
                soup_text = re.sub(r'\n', "", soup_text)
                soup_text = re.sub(r'\r', "", soup_text)
        except Exception as e:
            print(e.__str__())

        return soup_text

    @staticmethod
    @F.udf(t.StringType())
    def cleaning_content(qcontent):
        try:
            qcontent = re.sub('<[^>]*>', ' ', qcontent).replace('&nbsp;', ' ').replace('&there4;', ' ').replace('&#39;',
                                                                                                                '').replace(
                '<sup>', '').replace('</sup>', '').replace('<br>', '').replace('<br />', '')
        except Exception as e:
            print("Exception in cleaning content: ",e.__str__())
        return qcontent

    @staticmethod
    @F.udf(t.ArrayType(t.StringType()))
    def get_symbols(mstring):
        try:
            mstring = str(mstring)
            list_symbols = []
            list_items = mstring.split()
            for item in list_items:
                if (len(item) == 1):
                    if ((re.match('[a-z]', item) == None) and (item != ' ') and (
                            item not in ['.', ',', '\'', ';', '?', '\u2061'])):
                        list_symbols.append(item)

            list_symbols = list(set(list_symbols))
            return list_symbols
        except:
            print(mstring)
            return []

    @staticmethod
    @F.udf(t.StringType())
    def clean_data(string, list_symbols):
        try:
            string = string.split(' ')
            unwanted = []
            for i in string:
                if (len(i) == 1):
                    if (ord(i) > 128 and i not in list_symbols):
                        unwanted.append(i)
            unwanted = list(unwanted)
            for i in unwanted:
                string.remove(i)
            string = ' '.join(string)
            return string
        except:
            return string

    @staticmethod
    @F.udf(t.StringType())
    def remove_unicode_content(qcontent):
        try:
            qcontent = re.sub('\u2061', '', qcontent)
            return qcontent
        except:
            return qcontent

    @staticmethod
    @F.udf(t.StringType())
    def remove_page_margin(content):
        if content.startswith("<!--"):
            return ""
        else:
            return content
    

    def process_dataframe(self, df):        
        # df = df.withColumn("question_info_clean",self.Parse_Question_Text(F.col("question_text")))

        # df = df.withColumn("question_info_clean",self.clean(F.col("question_info_clean")))
        
        df = df.withColumn("question_info_clean_complete",self.cleaning_content(F.col("question_text")))
        
        df = df.dropna(subset=("question_info_clean_complete"))

        df = df.withColumn("question_info_clean_complete",self.remove_unicode_content(F.col("question_info_clean_complete")))

        df = df.withColumn("symbols",self.get_symbols(F.col("question_info_clean_complete")))
        
        flat_list = df.select("symbols").rdd.flatMap(lambda x: x[0]).collect()
        list_symbols = list(set(flat_list))
        list_symbols = F.array([F.lit(i) for i in list_symbols])
        
        df = df.withColumn("question_info_clean_complete",self.clean_data(F.col("question_info_clean_complete"),list_symbols))                

        # df = df.withColumn("answer_info",self.Parse_Question_Text(F.col("whole_answer_body")))
        
        # df = df.withColumn("whole_answer_body",self.clean(F.col("whole_answer_body")))
        
        # df = df.withColumn("whole_answer_body",self.remove_page_margin(F.col("whole_answer_body")))
        
        df = df.withColumn("is_question_text_present",F.when(F.col("question_info_clean_complete")=="",False).otherwise(True))
        df = df.withColumn("is_answer_text_present",F.when(F.col("correct_options")=="",False).otherwise(True))

        return df

    def start_processing(self):
        
        # if self.grail_df and len(self.grail_df.take(1))==0:
        #     print("emptyDF")
        # else:            
        grail_dataframe = self.process_dataframe(self.grail_df)

        return grail_dataframe        

        # return (self.ca_flag, self.prod_flag, self.grail_flag)

# 4.
def create_index_with_mapping(es, idx_name):
    mapping = {
        "settings" : {
            "number_of_shards": 10,
            "number_of_replicas": 0,
            "index.routing.allocation.total_shards_per_node": 2
        },
        "mappings": {
            "properties": {
                "question_code": {
                    "type": "text" 
                },
                "question": {
                    "type": "text"
                },
                "question_version": {
                    "type": "integer"
                },
                "status": {
                    "type": "text"
                },
                "question_code_with_version": {
                    "type": "text"
                },
                "question_id_with_version": {
                    "type": "text"
                },
                "subtype": {
                    "type": "text"
                },
                "learning_maps": {
                    "type": "text"
                },
                "question_urls": {
                    "type": "text"
                },
                "answer_urls": {
                    "type": "text"
                },
                "question_image_dense": {
                    "type": "dense_vector",
                    "dims": 512
                },
                "answer_image_dense": {
                    "type": "dense_vector",
                    "dims": 512
                },
                "is_question_image_dense_present": {
                    "type": "boolean"
                },
                "is_answer_image_dense_present": {
                    "type": "boolean"
                },
                "is_question_text_present": {
                    "type": "boolean"
                },
                "is_answer_text_present": {
                    "type": "boolean"
                },
                "whole_answer_body": {
                    "type": "text"
                },
                "correct_options": {
                    "type": "text"
                },
                "timestamp": {
                    "type": "date"
                },
                "avg_hygiene_score": {
                    "type": "float"
                }
            }
        }
    }
    resp = es.indices.create(idx_name,body=mapping,ignore=400)
    print(resp)

def driver_ingest_on_es(es, index_name, dataframe, write_config):
    try:
        create_index_with_mapping(es,index_name)

        es_write_config = write_config.copy()
        es_write_config["es.resource"] = "{}/_doc".format(index_name)
        es_write_config["es.mapping.id"] = "id"
        es_write_config["es.read.field.as.array.include"] = 'question_urls,answer_urls,question_image_dense,answer_image_dense,learning_maps'
        
        # es_write_config["es.write.operation"] = "upsert" # TEMP
        # dataframe.write.format("org.elasticsearch.spark.sql").options(**es_write_config).mode("append").save() # TEMP

        dataframe.write.format("org.elasticsearch.spark.sql").options(**es_write_config).mode("overwrite").save()
    except Exception as e:
        print(e.__str__())


# 8.
def get_answer(df, correct_ans_lookup):
    original_question_code = df["original_question_code"]
    duplicate_question_code = df["duplicate_question_code"]
    try:
        org_ans = correct_ans_lookup.get(original_question_code,"")
    except:
        print("exception: ", df["question_code"])
        org_ans = ""
    try:
        dup_ans = correct_ans_lookup.get(duplicate_question_code,"")
    except:
        print("exception: ", df["question_code"])
        dup_ans = ""
    return (org_ans, dup_ans)

# 9.
def laven_ans_ratio(df):
    correct_option_x = df["correct_option_x"]
    correct_option_y = df["correct_option_y"]
    if (correct_option_x == "") or (correct_option_y == ""):
        return 1
    return Levenshtein.ratio(correct_option_x, correct_option_y)

def make_answer_lower(text):
    try:
        if math.isnan(text):
            return ""
    except:
        return text.lower()

def find_representatives(df,level):
    field_name = "is_{}_level_rep".format(level)
    representative = df.groupby(["cluster_id",level],as_index=False).first()
    df.loc[df["index"].isin(representative["index"].tolist()),field_name] = True
    df[field_name].fillna(False,inplace=True)
    return df

split_tlpn_schema = t.StructType([
    t.StructField("goal", t.StringType(), True),
    t.StructField("grade", t.StringType(), True),
    t.StructField("subject", t.StringType(), True),
    t.StructField("unit", t.StringType(), True),
    t.StructField("chapter", t.StringType(), True),
    t.StructField("topic", t.StringType(), True),    
])

@F.udf(split_tlpn_schema)
def split_tlpn(learnpath_name):
    lm_level_values = learnpath_name.split("--")
    if len(lm_level_values)!=6:
        print("entities more than 6:",lm_level_values)
        goal = None
        grade = None
        subject = None
        unit = None
        chapter = None
        topic = None
    else:
        goal = lm_level_values[0]
        grade = lm_level_values[1]
        subject = lm_level_values[2]
        unit = lm_level_values[3]
        chapter = lm_level_values[4]
        topic = lm_level_values[5]

    return t.Row(
        "goal","grade","subject","unit","chapter","topic"
    )(goal,grade,subject,unit,chapter,topic)

def curried_find_version(qid_to_qversion_broadcast):
    schema = t.ArrayType(t.StringType())
    
    def find_version(question_id):
        question_id = set(question_id)
        qid_to_qversion = qid_to_qversion_broadcast.value
        exact_questions_with_version = [str(int(x))+"__"+str(qid_to_qversion.get(int(x),[0])[0]) for x in question_id] # 0th element of the list is qVersion
        return exact_questions_with_version
    
    return F.udf(find_version, schema)

def post_processing_calculate_similarity_score(org_qText, org_aText, org_qImgPresent, org_aImgPresent, org_qTxtPresent, org_aTxtPresent,dup_qText, dup_aText, dup_qImgPresent, dup_aImgPresent, dup_qTxtPresent, dup_aTxtPresent):
    score = 0
    score_count = 0

    if org_qTxtPresent or dup_qTxtPresent:
        q_clean_laven_ratio = sc.clean_laven_ratios_py(dup_qText,org_qText)
        q_clean_ratio = sc.clean_ratios(dup_qText,org_qText)
        score += (q_clean_laven_ratio+q_clean_ratio)/2
        score_count += 1
    
    if org_aTxtPresent or dup_aTxtPresent:
        a_clean_laven_ratio = sc.clean_laven_ratios_py(dup_aText,org_aText)
        a_clean_ratio = sc.clean_ratios(dup_aText,org_aText)
        score += (a_clean_laven_ratio+a_clean_ratio)/2
        score_count += 1

    if org_qImgPresent or dup_qImgPresent:
        score += 0.99
        score_count += 1
    
    if org_aImgPresent or dup_aImgPresent:
        score += 0.99
        score_count += 1
    
    return score/score_count if score_count!=0 else 0


def populate_similarity_scores_for_all_duplicates(qid_to_qinfo_broadcast):
    schema = t.ArrayType(t.FloatType())

    def find_similarity_scores(question_id, exact_questions, exact_duplicates_with_scores, similarity_scores):
        if exact_duplicates_with_scores and similarity_scores:
            score_lookup = dict(zip(exact_duplicates_with_scores,similarity_scores))
        else:
            score_lookup = dict()

        scores = []
        qid_to_qinfo = qid_to_qinfo_broadcast.value
        org_qText, org_aText, org_qImgPresent, org_aImgPresent, org_qTxtPresent, org_aTxtPresent = qid_to_qinfo.get(question_id,["","","",False,False,False,False])[1:]

        for x in exact_questions:
            if x==question_id:
                scores.append(1.0)
            else:
                score = score_lookup.get(x,None)
                if score:
                    scores.append(score)
                else:
                    dup_qText, dup_aText, dup_qImgPresent, dup_aImgPresent, dup_qTxtPresent, dup_aTxtPresent = qid_to_qinfo.get(x,["","","",False,False,False,False])[1:]
                    score = post_processing_calculate_similarity_score(org_qText, org_aText, org_qImgPresent, org_aImgPresent, org_qTxtPresent, org_aTxtPresent,dup_qText, dup_aText, dup_qImgPresent, dup_aImgPresent, dup_qTxtPresent, dup_aTxtPresent)
                    scores.append(score)

        return scores

    return F.udf(find_similarity_scores,schema)
    
        
    

# def curried_find_similar(similar_clusters_broadcast,qid_to_qversion_broadcast):
#     cluster_schema = t.StructType(
#                     [
#                         t.StructField("similar_questions", t.ArrayType(t.IntegerType()), False),
#                         t.StructField("similar_questions_with_version", t.ArrayType(t.StringType()), False),
#                         t.StructField("exact_questions_with_version", t.ArrayType(t.StringType()), False)
#                     ]
#                 )

    
#     def find_similar_clusters(cluster_id, question_id):
#         question_id = set(question_id)
#         set_sim_clusters = similar_clusters_broadcast.value
#         qid_to_qversion = qid_to_qversion_broadcast.value
#         sims = []
        
#         for sim_cluster in set_sim_clusters:
#             if question_id.issubset(sim_cluster):
#                 sims.extend(sim_cluster.difference(question_id))

#         similar_questions = list(set(sims))
#         similar_questions_with_version = [str(int(x))+"__"+str(qid_to_qversion.get(int(x),0)) for x in similar_questions]
#         exact_questions_with_version = [str(int(x))+"__"+str(qid_to_qversion.get(int(x),0)) for x in question_id]
        
#         return Row(
#             "similar_questions","similar_questions_with_version","exact_questions_with_version"
#         )(similar_questions,similar_questions_with_version,exact_questions_with_version)
    
#     return F.udf(find_similar_clusters, cluster_schema)
