import pandas as pd
import re
import ast
import Levenshtein
from bs4 import BeautifulSoup
from elasticsearch import Elasticsearch,helpers
import os

es = Elasticsearch('10.144.20.4')
import json
from itertools import groupby
import image_similarity
from pylatexenc.latex2text import LatexNodes2Text
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize 
from nltk.stem import PorterStemmer


stop_words = set(stopwords.words('english')) 


def find_dups(query,finalresult,length,question_code):
    if not question_code:
        ids=[]
        for result,score in finalresult:
            ids.append((result['question_code'],result['question_ans_clean'],result['question'],result['correct_option'],result['subject']))
        return ids
    else:
        ids=[]
        for result,score in finalresult:
            if result['question_code'] == question_code:
                continue
            ids.append((result['question_code'],result['question_ans_clean'],result['question'],result['correct_option'],result['subject']))
        return ids

    


def search_grail(question_text,answer_text,question_code):
    if answer_text:
        query = question_text
        length = len(query)
    else:
        query = str(question_text)
        length = len(query)

    complex_query = {
        "size" : 50,
        "query": {
            "match": {
                "question": {
                    "query": query
                }
            }
        }
    }

    try:
        search = es.search(index='cqi-duplicate-grail-mongo', doc_type='my_type', body=complex_query,request_timeout=1)
        finalresult = []
        for res in search['hits']['hits']:
            result = res['_source']
            score = res['_score']
            finalresult.append([result, score])
    
        output = find_dups(query, finalresult, length, question_code)
    
        if len(output) >= 1:
            return {"output":output,"status":"success"}

        else:
            return {"output":[],"status":"empty"}
    except:
        return {"output":[],"status":"error"}


def sep_duplicate_in_rows(dup_list):
    possible_dup_df = pd.DataFrame(dup_list)
    possible_dup_df.columns = ['duplicate_code','duplicate_text','duplicate_question','duplicate_answer','subject']
    return possible_dup_df

def fetch_Levenshtein(result_df,question):
    return Levenshtein.ratio(str(question),str(result_df['duplicate_question']))

def fetch_Levenshtein_answer(result_df,answer_text):
    if answer_text == None or answer_text == "" or answer_text == "nan" or answer_text == " " or result_df['duplicate_answer'] == "" or result_df['duplicate_answer'] == None or result_df['duplicate_answer'] == " " or result_df['duplicate_answer'] == "nan":
        return 1.0
    return Levenshtein.ratio(str(answer_text),str(result_df['duplicate_answer']))

def make_lower(text):
    return text.lower()

def remove_punct(text):
    punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    no_punct = ""
    for char in text:
        if char not in punctuations:
            no_punct = no_punct + char
    return no_punct
def remove_stop_words(text):
    word_tokens = word_tokenize(text)
    filtered_sentence = [] 
    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w) 
    string = ' '.join(word for word in filtered_sentence) 
    return string

def fetch_Minhash_ratios(result_df,question_answer_clean):
    fingerprint_a = set(hasher.fingerprint(str(question_answer_clean).encode('utf8')))
    fingerprint_b = set(hasher.fingerprint(str(result_df.duplicate_text).encode('utf8')))
    minhash_sim = len(fingerprint_a & fingerprint_b) / len(fingerprint_a | fingerprint_b)
    return minhash_sim


def shingles(text, char_ngram=5):
    text = str(text)
    return set(text[head:head + char_ngram] for head in range(0, len(text) - char_ngram))


def lavenstein_ratio(set_a, set_b):
    return Levenshtein.ratio(set_a, set_b)



def find_all_ratios(original_dup_text_dup_df, question_text,answer_text):

    original_dup_text_dup_df['lavenstein_ratio'] = original_dup_text_dup_df.apply(fetch_Levenshtein,args=(question_text,), axis=1)
    original_dup_text_dup_df['lavenstein_ratio_answer'] = original_dup_text_dup_df.apply(fetch_Levenshtein_answer,args=(answer_text,), axis=1)

    #original_dup_text_dup_df['Minhash_ratios'] = original_dup_text_dup_df.apply(fetch_Minhash_ratios,args=(question_answer_clean,), axis=1)
    original_dup_text_dup_df["weighted_laven_minhash"] = original_dup_text_dup_df.lavenstein_ratio

    return original_dup_text_dup_df


def compute_dups_bio(extra_pair_df,question_text,answer_text,is_book):
    exact_pairs_list = []
    similar_pairs_with_different_values = []
    related_question_different_answer = []
    cleaned_question_text =  make_lower(question_text)
    cleaned_question_text = remove_punct(cleaned_question_text)
    cleaned_question_text = remove_stop_words(cleaned_question_text)
    if is_book:
        for index, row in extra_pair_df.iterrows():
            dup_cleaned_question_text =  make_lower(row["duplicate_question"])
            dup_cleaned_question_text = remove_punct(dup_cleaned_question_text)
            dup_cleaned_question_text = remove_stop_words(dup_cleaned_question_text)
            clean_ratio = lavenstein_ratio(cleaned_question_text,dup_cleaned_question_text)

            if clean_ratio >=0.96:
                if row["lavenstein_ratio"]>=0.96:
                    if row["lavenstein_ratio_answer"] >=0.90:
                        exact_pairs_list.append(row)
                elif (row["lavenstein_ratio"]>=0.90) and (row["lavenstein_ratio"]<0.96):
                    if row["lavenstein_ratio_answer"]>=0.90:
                        similar_pairs_with_different_values.append(row)
            elif (clean_ratio <=0.95) and (clean_ratio>=0.85):
                related_question_different_answer.append(row)
    else:
        for index, row in extra_pair_df.iterrows():
            if row["lavenstein_ratio"]>=0.96:
                if row["lavenstein_ratio_answer"] >=0.90:
                    exact_pairs_list.append(row)
                elif (row["lavenstein_ratio"]>=0.90) and (row["lavenstein_ratio"]<0.96):
                    if row["lavenstein_ratio_answer"]>=0.90:
                        similar_pairs_with_different_values.append(row)

    return (exact_pairs_list,similar_pairs_with_different_values,related_question_different_answer)



def compute_duplicates(extra_pair_df,question_text,answer_text,is_book):
    list_of_subjects = extra_pair_df['subject'].values.tolist()
    list_of_subjects = max(set(list_of_subjects), key=list_of_subjects.count)
    return compute_dups_bio(extra_pair_df,question_text,answer_text,is_book)



def start_finding_dup(question_code, question_text, answer_text,source,subject,is_book):

    if (subject == "physics") or (subject == "chemistry") :
        print("Subject",subject)
        img_exists = check_image_exists(question_text, answer_text)
        print(img_exists)
        if img_exists:
            image_text_dup = image_similarity.get_Image_Duplicates(question_text, answer_text,source,subject,question_code)
            return image_text_dup

    question_text,answer_text = preprocessing(question_text,answer_text,source,subject)
    if source == 'content-admin':
        dup_list = search(question_text,answer_text,question_code)
    elif source == 'production':
        dup_list = search_prod(question_text,answer_text,question_code)
    elif source == "content-grail":
        dup_list = search_grail(question_text,answer_text,question_code)

    if dup_list["status"]=="success":
        dup_list = dup_list["output"]

    elif dup_list["status"]=="empty":
        return {"input": {'question_id': question_code,'question_text':question_text,"answer_text":answer_text},"output": [],"status":"success"}

    elif dup_list["status"]=="error":
        return {"input": {'question_id': question_code,'question_text':question_text,"answer_text":answer_text},"output": "Something went Wrong with Elastic_Search Please Try again after sometime","status":"error"}


    finalize_df = sep_duplicate_in_rows(dup_list)
    original_dup_ratio_df = find_all_ratios(finalize_df, question_text,answer_text)

    exact_pairs_list,similar_pairs_with_different_values,related_question_different_answer = compute_duplicates(original_dup_ratio_df,question_text,answer_text,is_book)
    duplicate_pairs_df = pd.DataFrame(exact_pairs_list)
    duplicate_pairs_df['is_type']='exact'

    duplicate_pairs_df = duplicate_pairs_df.reindex(columns=["duplicate_code","duplicate_question","duplicate_answer","is_type","weighted_laven_minhash"])
    duplicate_pairs_df.sort_values(['weighted_laven_minhash'], ascending=[False],inplace=True)
    duplicate_pairs_df.rename(columns={"weighted_laven_minhash":"score"},inplace=True)
    exact_records_d_list = []
    exact_records_d = duplicate_pairs_df.to_dict('records')
    exact_records_d_list.extend(exact_records_d)

    duplicate_pairs_with_different_values_df = pd.DataFrame(similar_pairs_with_different_values)
    duplicate_pairs_with_different_values_df['is_type']='similar'
    duplicate_pairs_with_different_values_df = duplicate_pairs_with_different_values_df.reindex(columns=["duplicate_code","duplicate_question","duplicate_answer","is_type","weighted_laven_minhash"])
    duplicate_pairs_with_different_values_df.sort_values(['weighted_laven_minhash'], ascending=[False],inplace=True)
    duplicate_pairs_with_different_values_df.rename(columns={"weighted_laven_minhash":"score"},inplace=True)
    similar_records_d = duplicate_pairs_with_different_values_df.to_dict('records')
    exact_records_d_list.extend(similar_records_d)
    
    related_pairs_df = pd.DataFrame(related_question_different_answer)
    related_pairs_df['is_type']='related'
    related_pairs_df = related_pairs_df.reindex(columns=["duplicate_code","duplicate_question","duplicate_answer","is_type","weighted_laven_minhash"])
    related_pairs_df.sort_values(['weighted_laven_minhash'], ascending=[False],inplace=True)
    related_pairs_df.rename(columns={"weighted_laven_minhash":"score"},inplace=True)
    related_records_d = related_pairs_df.to_dict('records')
    exact_records_d_list.extend(related_records_d)
    output_data = {"input": {'question_id': question_code,'question_text': question_text,'answer_text':answer_text},"output": exact_records_d_list,"status":"success"}
    
    return output_data


def preprocessing(question_text, answer_text,source,subject):
    parsed_question_text = Parse_Question_Text(question_text,source)
    cleaned_question_text = clean(parsed_question_text)
    removed_tags_question_text = cleaning_content(cleaned_question_text)
    removed_unicode_question_text = remove_unicode_content(removed_tags_question_text)
    flat_list = get_symbols(removed_unicode_question_text)
    list_symbols  = list(set(flat_list))
    cleaned_question = clean_data(removed_unicode_question_text,list_symbols)

    

    if answer_text:
        if source == 'content-admin' or source == "content-grail":
            parsed_answer_text = Parse_Question_Text(answer_text,source)
            cleaned_answer_text = clean(parsed_answer_text)
            removed_unicode_answer_text = remove_unicode_content(cleaned_answer_text)
            return (cleaned_question,removed_unicode_answer_text)
        else:
            parsed_answer_text = Parse_answer_Text(answer_text)
            cleaned_answer_text = clean(parsed_answer_text[0])
            removed_unicode_answer_text = remove_unicode_content(cleaned_answer_text)
            return (cleaned_question,removed_unicode_answer_text)

    else:
        return (cleaned_question,"")



def check_image_exists(question_text,answer_text):
    if ("img" in question_text) or ("img" in answer_text):
        return True
    else:
        return False

def Parse_Question_Text(question_info_object,source):
    if source == "content-admin" or source == "content-grail":
        qcontent = re.sub('&nbsp;',' ',question_info_object).replace('&there4;', ' ')
        return qcontent
    else:
        print("Nikesh",question_info_object)
        try:
            text = question_info_object['en']
            qcontent = re.sub('&nbsp;',' ',text).replace('&there4;', ' ')
            return qcontent
        except:
            qcontent = re.sub('&nbsp;',' ',question_info_object).replace('&there4;', ' ')
            return qcontent


def Parse_answer_Text(answerinfo_object):
    #answerinfo_object = ast.literal_eval(answerinfo_object)
    explan = None
    correct_option = None
    list_of_options = []
    for each_answer in answerinfo_object:
        option_text = None
        option_is_correct = None
        explanation = None
        try:
            if 'body' in each_answer.keys():
                option_text = each_answer['body']['en']
                option_text = re.sub('&nbsp;',' ',option_text).replace('&there4;', ' ').replace('&#39;','')

            elif 'bodies' in each_answer.keys():
                option_text = each_answer['bodies']['en']
                option_text = re.sub('&nbsp;',' ',option_text).replace('&there4;', ' ').replace('&#39;','')
            list_of_options.append(option_text)
        except Exception as e:
            pass
        try:
            option_is_correct = each_answer['correct']
            option_is_correct = re.sub('&nbsp;',' ',option_is_correct).replace('&there4;', ' ').replace('&#39;','').replace('<sup>','').replace('</sup>','').replace('<br>','').replace('<br />','')
        except Exception as e:
            pass
        try:
            explanation = each_answer['explanation']
            explanation = re.sub('&nbsp;',' ',explanation).replace('&there4;', ' ').replace('&#39;','').replace('<sup>','').replace('</sup>','').replace('<br>','').replace('<br />','')
        except Exception as e:
            pass
        
        if option_is_correct == True:
            try:
                explan = explanation['en']
                
                explan = re.sub('&nbsp;',' ',explan).replace('&there4;', ' ').replace('&#39;','').replace('<sup>','').replace('</sup>','').replace('<br>','').replace('<br />','')
                
            except Exception as e:
                pass
            correct_option = option_text
            
    return [correct_option,list_of_options,explan]


def clean(text):
    if not text:
        return "No explanation_given"
    else:
        soup = BeautifulSoup(text,'html.parser')
        soup.prettify()
        text = soup.get_text()
        text = text.replace("\\\\","\\")
        text = text.replace("\\n"," ")
        try:
            latex_text = LatexNodes2Text().latex_to_text(text)
        except:
            new_generated_text = soup_text.strip('\"').replace('\\\\','\\').replace('\\"','\"').replace('\\n','\n').replace("\\r","\r").replace("\\t","\t")
            latex_text = LatexNodes2Text().latex_to_text(new_generated_text)

        latex_text = latex_text.strip('\"')
        latex_text = re.sub(r"(^Q\w+.)", '', latex_text)
        latex_text = re.sub(r"(^Q.\w+.:)", '', latex_text)
        latex_text = re.sub(r"(^Q\w+.)", '', latex_text)
        latex_text = re.sub(r"(^Ans\w+.)", '', latex_text)
        latex_text = latex_text.replace("\n"," ")
        latex_text = latex_text.replace("\r"," ")
        latex_text = latex_text.replace("  "," ")
        latex_text = latex_text.replace("   "," ")
        latex_text = re.sub(r'\n',"",latex_text)
        latex_text = re.sub(r'\r',"",latex_text)

    return  latex_text


def cleaning_content(qcontent):
    ##remove html/mathml tags
    try:
        qcontent = re.sub('<[^>]*>',' ',qcontent).replace('&nbsp;', ' ').replace('&there4;', ' ').replace('&#39;','').replace('<sup>','').replace('</sup>','').replace('<br>','').replace('<br />','')
    except Exception as e:
        pass
    return qcontent

def get_symbols(mstring):
    list_symbols=[]
    list_items = mstring.split()
    for item in list_items:
        if(len(item)==1):
            if((re.match('[a-z]',item) == None) and (item!=' ') and (item not in ['.',',','\'',';','?','\u2061'])):
                list_symbols.append(item)
                
    list_symbols = list(set(list_symbols))
    return list_symbols

def clean_data(string,list_symbols):
    string = string.split(' ')
    unwanted = []
    for i in string:
        if(len(i) == 1):
            if(ord(i)>128 and i not in list_symbols):
                unwanted.append(i)
    unwanted = list(unwanted)
    for i in unwanted:
        string.remove(i)
    string = ' '.join(string)
    return string


def remove_unicode_content(qcontent):
    qcontent = re.sub('\u2061','',qcontent)
    return qcontent


def find_by_code(list_of_question_code,source,subject):
    response_output = []
    question_code,question,answer,question_image_url,cleaned_options_list,is_book,question_type,answer_image_url,subject = list_of_question_code
    print(question_image_url)
    if question_image_url != "":
        question = question + "img"+question_image_url
    response = start_finding_dup(question_code, question, answer,source,subject,is_book)
    response_output.append(response)
    return {'response':response_output}
    
