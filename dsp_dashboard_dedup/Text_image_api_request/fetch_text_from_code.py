import pandas as pd
import ast
from elasticsearch import Elasticsearch,helpers
#es=Elasticsearch([{'host':'10.144.20.4','timeout':500}])
es=Elasticsearch([{'host':'10.144.20.6','timeout':500}])


def get_question_text(question_code,source):
    question_code = question_code
    source = source
    complex_query = {
        "query": {
            "match": {
                "question_code": {
                    "query": question_code
                }
            }
        }
    }

    try:
        if source == "content-grail":
            index_name = 'cqi-duplicate-grail-mongo'

        search = es.search(index=index_name, doc_type='my_type', body=complex_query,request_timeout=1)
        question = search['hits']['hits'][0]['_source']["question"]
        answer = search['hits']['hits'][0]['_source']["correct_option"]
        if ast.literal_eval(search["hits"]["hits"][0]["_source"]["question_images"])[0]["image_0"] != "No images":
            duplicate_question_image_url = ast.literal_eval(search["hits"]["hits"][0]["_source"]["question_images"])[0]["image_0"]
        else:
            duplicate_question_image_url = ""
        cleaned_options_list =  search['hits']['hits'][0]['_source']["cleaned_options_list"]
        is_book = search['hits']['hits'][0]['_source']["is_book"]
        question_type = search['hits']['hits'][0]['_source']["question_type"]
        if ast.literal_eval(search["hits"]["hits"][0]["_source"]["answer_images"])[0]["image_0"] != "No images":
            duplicate_answer_image_url = ast.literal_eval(search["hits"]["hits"][0]["_source"]["answer_images"])[0]["image_0"]
        else:
            duplicate_answer_image_url = ""
        subject = search['hits']['hits'][0]['_source']["subject"]
        return (question_code,question,answer,duplicate_question_image_url,cleaned_options_list,is_book,question_type,duplicate_answer_image_url,subject)
    except:
        return (question_code,0,0,0,0,0,0,0,0) 
        


