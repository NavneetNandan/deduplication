import re
import urllib3
http = urllib3.PoolManager()
from PIL import Image
import os
import sklearn
import cv2
import ast
import numpy as np
from sklearn.neighbors import NearestNeighbors
import yaml
import tensorflow as tf 
from keras.models import load_model
from elasticsearch import Elasticsearch,helpers
es=Elasticsearch([{'host':'10.144.20.7','timeout':500}])
from scipy.spatial import distance
import threading
num_threads = 500
graph = tf.get_default_graph()
import question_dup
import verify_image_text
import requests
import json
import time




with open('config.yaml') as f:
    yaml_data = yaml.load(f)

maths_encoder = load_model(yaml_data['maths_encoder'])
physics_encoder = load_model(yaml_data['physics_encoder'])
chemistry_encoder = load_model(yaml_data['chemistry_encoder'])
biology_encoder = load_model(yaml_data['biology_encoder'])




def find_Cg_Url(text):

    try:
        urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
        urls = list(set(urls))
        if not urls:
            return 0
        return urls
    except:
        return 0

def remove_Backslash(lis_text):
    if lis_text == 0:
        return 0
    
    main_list = []
    for text in lis_text:
        new_text = text.replace("\\","")
        main_list.append(new_text)
    return main_list[0]

def downloadImage(url,saved_path=yaml_data["question_download_path"]):

    try:
        r = http.request('GET', url, preload_content=False)
        name = url.split("/")[-1]
        
        if name.endswith(".jpg") or name.endswith(".jpeg") or name.endswith(".png"):
            print("i am fine")
            
        else:
            name = name + ".jpg"
            print(name)

        path = saved_path+name
        print("Path",path)
        with open(path ,'wb') as out:
            while True:
                data = r.read(1024) # Download Image and Write 1024 byte
                if not data:
                    print("got break")
                    break
                out.write(data)
        return path  
    except:
        return 0


def convertImageToJpegWithWhiteBackground(image,subject):

    try:
        img = Image.open(image).convert("RGBA")
        card = Image.new("RGB",img.size, (255, 255, 255)) ## Add White background
            
        x, y = img.size
        card.paste(img, (0, 0, x, y), img)
        card.save(image, format="jpeg")
    except:
        return 0



def resizeImgSaveIt(img,subject):
    try:
        im1 = Image.open(img)
        width = 228
        height = 228    
        im5 = im1.resize((width, height), Image.ANTIALIAS)    # best down-sizing filter
        im5.save(img)
    except:
        return 0



def preprocessData(image1_path,subject):

    output = convertImageToJpegWithWhiteBackground(image1_path,subject)
    if output == 0:
        return {"status":"error","reason":"Cannot Convert Image to a Required format"}

    output = resizeImgSaveIt(image1_path,subject)
    if output == 0:
        return {"status":"error","reason":"Cannot resize Image to a Required format"}


def downloadImageUrlPreprocessData(image_url,saved_path,subject):

    image_path = downloadImage(image_url,saved_path)
    if image_path == 0:
       return {"status":"error","reason":"Cannot Download Image"}

    output = preprocessData(image_path,subject)
    try:
        if output["status"] == "error":
            return output
    except:
        return image_path



def getNumpyArray(img1):

    image = cv2.imread(img1)
    return np.array(image)

def search_in_es(gte,lte,search_output):
    
    complex_query = {
            "size":1500,
            "query": {
                "range": {
                    "reshape_code": {
                        "gte":gte,
                        "lte":lte
                    }
                }
            }
        }
    search = es.search(index='nikesh_2', doc_type='my_type', body=complex_query)
    for res in search['hits']['hits']:
        question_code = res['_source']["question_code"]
        ldc = res['_source']["lower_dimension_code"]
        #lower_dimension_code = np.array(ast.literal_eval(ldc))
        search_output.append((question_code,ldc))
    return search_output

def find_dup(dim_1,dim2,question_code,return_able,subject):
    if subject == "physics":
        threshold = 1.50
    if subject == "chemistry":
        threshold = 0.30

    distance = sklearn.metrics.pairwise_distances(np.array(dim_1).reshape((1,512)),np.array(dim2).reshape(1,512),metric="euclidean")
    if distance[0][0] <=threshold:
        return_able.append(question_code)

    

def get_exact_match_local(dim_1,subject):
    start_time = time.time()
    query_data ={
    "size": 500,
    "query": {
        "script_score": {
        "query" : {
        "bool" : {
          "filter" : {
            "term" : {
              "subject" : subject
            }
          }
        }
      },
        "script": {
            "source": "cosineSimilarity(params.queryVector, doc['lower_dimension_code'])",
            "params": {
            "queryVector": dim_1 
                }
            }
            }
        }
        }
    response = requests.get("http://10.144.131.98:9200/cqi-duplicate-grail-image/_search?pretty&pretty",data=json.dumps(query_data),headers={"Content-Type": "application/json"})
    print("--- %s seconds ---" % (time.time() - start_time))
    results = json.loads(response.text)
    search_output = []
    return_able = []
    for res in results['hits']['hits']:
        question_code = res['_source']["question_code"]
        ldc = res['_source']["lower_dimension_code"]
        search_output.append((question_code,ldc))

    start_time = time.time()
    for item in search_output:
        find_dup(dim_1,item[1],item[0],return_able,subject)

    print("--- %s seconds ---" % (time.time() - start_time))
    
    return list(set(return_able))






def findImagesScore(image1,subject):

    if subject == "maths":
        encoder = maths_encoder
        width = yaml_data["width"]
        height = yaml_data["height"]
        encoded_width = yaml_data["encoded_width"]
        encoded_height = yaml_data["encoded_height"]
    elif subject == "physics":
        encoder = physics_encoder
        width = yaml_data["width"]
        height = yaml_data["height"]
        encoded_width = yaml_data["encoded_width"]
        encoded_height = yaml_data["encoded_height"]
    elif subject == "chemistry":
        encoder = chemistry_encoder
        width = yaml_data["width"]
        height = yaml_data["height"]
        encoded_width = yaml_data["encoded_width"]
        encoded_height = yaml_data["encoded_height"]
    elif subject == "biology":
        encoder = biology_encoder
        width = yaml_data["width"]
        height = yaml_data["height"]
        encoded_width = yaml_data["encoded_width"]
        encoded_height = yaml_data["encoded_height"]

        
    numpy1 = getNumpyArray(image1)
    numpy1 = numpy1.astype('float32') / 255.
    with graph.as_default():
        query_code_1 = encoder.predict(numpy1.reshape(1,height, width, 3))
        encoded_query_code_1 = query_code_1.reshape(1, encoded_width*encoded_height*8)
        c = np.zeros(encoded_query_code_1.shape)
        distance_euc = distance.euclidean(encoded_query_code_1, c)
        final_list = get_exact_match_local(encoded_query_code_1[0].tolist(),subject)
        return final_list


def strip_end(text, suffix):
    if not text.endswith(suffix):
        return text
    return text[:len(text)-len(suffix)]

def get_Image_Duplicates(question_text, answer_text,source,subject,org_question_code):

    question_list = find_Cg_Url(question_text)
    question_image_url = remove_Backslash(question_list)
    question_text = strip_end(question_text,"img"+question_image_url)
    if question_image_url != 0:
        question_image_path = downloadImageUrlPreprocessData(question_image_url,yaml_data["question_download_path"],subject)
        try:
            if question_image_path["status"] == "error":
                return jsonify(question_image_path)
        except:
            image_dup_lis = findImagesScore(question_image_path,subject)
            try:
                image_dup_lis.remove(org_question_code+".jpeg")
            except:
                print("I am Not having original Code")

            image_text_output = verify_image_text.get_verified_image_text(question_text, answer_text,source,subject,image_dup_lis,question_image_url,org_question_code)
            return image_text_output


