from flask import Flask, jsonify,request, Response,abort,make_response,json,current_app
import question_dup
import fetch_text_from_code
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/find_dup',methods=['POST','GET'])
def get_Duplicates():
    input_data = request.get_json()
    print(input_data)
    question_code = input_data.get('question_id', None)
    question_text = input_data.get('question_content', "")
    source = input_data.get('source_db', "")
    answer_text = input_data.get('answer', "")
    input_data.get("options",[])
    subject = input_data.get('subject', "")
    if subject == "maths(6-10)":
        subject = "maths"
    if subject == "science(6-10)":
        subject = "science"
    
    
    # Do processing with input_data here..
    if not question_text or question_text == "":
        #question_code_list = [i.strip() for i in question_code]
        try:
           total_question_codes = fetch_text_from_code.get_question_text(question_code,source)
           print(total_question_codes)
           if total_question_codes[1] == 0:
               total_question_codes = fetch_text_from_code.get_question_text(question_code,source)
               if total_question_codes[1] == 0:
                   return jsonify({"data": {"response": [{"input": {'question_id': question_code,'question_text': question_text,'answer_text':answer_text},"output":"Invalid Question code","status":"error"}]}})

        except:
            print(total_question_codes)
            return jsonify({"data":{"response":[{"input": {'question_id': question_code,'question_text': question_text,'answer_text':answer_text},"output": "Something went wrong","status":"error"}]}})
           
           
        response = question_dup.find_by_code(total_question_codes,source,subject)

        if response["response"][0]["status"] == "error":
            response = question_dup.find_by_code(total_question_codes,source,subject)
            if response["response"][0]["status"] == "error":
                return jsonify({"data":response})

        return jsonify({"data":response})
    else:

        response = question_dup.start_finding_dup(question_code, question_text,answer_text,source,subject,1)
        if response["status"] == "error" and response["output"] == "Something went Wrong with Elastic_Search Please Try again after sometime":
            response = question_dup.start_finding_dup(question_code, question_text, answer_text,source,subject,1)
        response_array =[]
        response_array.append(response)
        return jsonify({"data":{"response":response_array}})
        
if __name__ == '__main__':
    app.run(debug=False , host = "0.0.0.0", port="9001")
