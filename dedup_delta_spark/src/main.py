import time
import os
import sys
from pyspark.sql import SparkSession

if os.path.exists('utilities.zip'):
    sys.path.insert(0, 'utilities.zip')
else:
    sys.path.insert(0, './utilities')

from utilities.driver import Dedup

if __name__ == '__main__':
    spark_session = (SparkSession
    .builder
    .appName('CQI Deduplication Batch Pipeline')
    .getOrCreate())
    # .config("spark.jars.packages","org.elasticsearch:elasticsearch-hadoop:7.6.1,org.mongodb.spark:mongo-spark-connector_2.11:2.4.2,graphframes:graphframes:0.8.1-spark2.4-s_2.11")
    # .config("spark.yarn.maxAppAttempts",1)
    # .config("spark.driver.memory","12g")
    # .config("spark.rpc.message.maxSize","1024")
    

    start = time.time()
    
    # goal = "all"
    # status = ["Approved","UAT Accepted","UAT Rejected"] # ,"Published"
    kafka_env = "staging" # staging or prod
    status_filters_clustering = ['Pending Approval', 'Published', 'Approved',
       'UAT Rejected', 'Published UAT Accepted', 'UAT Accepted']
    obj = Dedup(spark_session, status_filters_clustering, kafka_env)
    obj.driver()
    
    end = time.time()

    print("*"*15,"Execution of job took {} seconds".format(end-start),"*"*15)