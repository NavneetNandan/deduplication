import pyspark.sql.functions as F
import pyspark.sql.types as t
from pyspark.sql import Row

import Levenshtein
import pandas as pd
import numpy as np
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from sklearn.metrics.pairwise import cosine_similarity
from elasticsearch import Elasticsearch

stop_words = set(stopwords.words('english'))

stop_words.remove('not')
stop_words.remove("which")
stop_words.remove("what")
stop_words.remove("why")
stop_words.remove("how")

porter = PorterStemmer()
# ------------------------------------------- Find Candidate Questions -------------------------------------------
# 5.
def lavenstein_ratio(set_a, set_b):
    return Levenshtein.ratio(set_a, set_b)

def fetch_Laven(result_df):
    if str(result_df.duplicate_question_text) != ' ':
        return lavenstein_ratio(str(result_df.original_question_text), str(result_df.duplicate_question_text))
    else:
        return 0.0

def fetch_answer_Laven(result_df):
    if str(result_df.duplicate_answer_text) != ' ':
        return lavenstein_ratio(str(result_df.original_answer_text), str(result_df.duplicate_answer_text))
    else:
        return 0.0

def find_image_similarity(row,field):
    if field=="question":
        a = np.array(row["original_question_image_dense"],dtype="float16").reshape((1,512))
        b = np.array(row["duplicate_question_image_dense"],dtype="float16").reshape((1,512))
    else:
        a = np.array(row["original_answer_image_dense"],dtype="float16").reshape((1,512))
        b = np.array(row["duplicate_answer_image_dense"],dtype="float16").reshape((1,512))
    return cosine_similarity(a,b).item()

# TODO: add fields for all images and answer text
def calculate_laven_ratios(formated_output,is_question_text_present,is_answer_text_present):
    laven_df = pd.DataFrame(formated_output,columns=[
        'original_id','original_question_code', 'original_question_text', "original_answer_text", "original_question_image_dense", "original_answer_image_dense",
        'duplicate_id','duplicate_question_code', 'duplicate_question_text', "duplicate_answer_text", "duplicate_question_image_dense", "duplicate_answer_image_dense"
        ])
    if is_question_text_present:
        laven_df['Laven_ratios'] = laven_df.apply(fetch_Laven, axis=1)
        laven_df = laven_df[laven_df['Laven_ratios'] > 0.85]
        if laven_df.empty:
            return []
    else:
        laven_df['Laven_ratios'] = [None]*len(laven_df)

    if is_answer_text_present:
        laven_df['answer_laven_ratios'] = laven_df.apply(fetch_answer_Laven, axis=1)
    else:
        laven_df['answer_laven_ratios'] = [None]*len(laven_df)

    laven_df["question_image_similarity"] = laven_df.apply(lambda x: find_image_similarity(x,"question"), axis=1)
    laven_df["answer_image_similarity"] = laven_df.apply(lambda x: find_image_similarity(x,"answer"), axis=1)
    
    if laven_df.empty:
        return []
    else: # TODO: change cols here as well
        laven_df_quest = laven_df[[
            'original_question_code', 'duplicate_question_code', 'original_id', 'duplicate_id', "Laven_ratios", "answer_laven_ratios", 
            "original_question_text", "duplicate_question_text", "original_answer_text", "duplicate_answer_text",
            "question_image_similarity", "answer_image_similarity"
            ]]
        laven_df_quest = laven_df_quest.astype({"Laven_ratios":float,"answer_laven_ratios":float,"question_image_similarity":float,"answer_image_similarity":float})
        laven_list = laven_df_quest.values.tolist()
        return laven_list

# TODO: fetch all image vectors, fetch answer text as well
def find_dups(finalresult, question_code):
    ids = []
    for result, _ in finalresult:
        if result['question_code'] == question_code:
            continue
        ids.append((result['id'],result['question_code'], result['question'], result['whole_answer_body'], result["question_image_dense"], result["answer_image_dense"]))        
    return ids

def only_text(es, es_index, question_text, question_code,is_answer_text_present):
    query = str(question_text)
    # length = len(query)
    field = "question"
    complex_query = {
        "size": 50,
        "query":{
            "bool":{
                "filter": [
                    {"term":{"is_question_text_present":True}},
                    {"term":{"is_question_image_dense_present":False}},
                    {"term":{"is_answer_image_dense_present":False}},
                    {"term":{"is_answer_text_present":is_answer_text_present}}
                ],
                "must":{
                    "match": {
                        field: {
                            "query": query
                        }
                    }
                }
            }
        }
    }
    try:
        search_res = es.search(index=es_index, body=complex_query, request_timeout=100)
        return search_res
    except Exception as e:
        print("Exception in candidate search(Only Text):",question_code,e.__str__())
        return None

def text_with_image(es, es_index, question_text, image_field, question_code,is_answer_text_present):
    query = str(question_text)
    # length = len(query)
    field = "question"
    complex_query = {
        "size": 50,
        "query":{
            "bool":{
                "filter": [
                    {"term":{"is_question_text_present":True}},
                    {"term":{"is_{}_present".format(image_field):True}},
                    {"term":{"is_answer_text_present":is_answer_text_present}}
                ],
                "must":{
                    "match": {
                        field: {
                            "query": query
                        }
                    }
                }
            }
        }
    }
    try:
        search_res = es.search(index=es_index, body=complex_query, request_timeout=100)
        return search_res
    except Exception as e:
        print("Exception in candidate search(Text+Image):",question_code,e.__str__())
        return None

def only_image(es, es_index, image_field, dense_vector, question_code,is_answer_text_present):
    complex_query ={
        "size": 50,
        "query": {
            "script_score": {
                "query":{
                    "bool":{
                        "filter":[
                            {"term":{"is_{}_present".format(image_field):True}},
                            {"term":{"is_answer_text_present":is_answer_text_present}}
                        ]
                    }
                },
                "script": {
                    "source": "cosineSimilarity(params.queryVector, doc['{}']) + 1.0".format(image_field),
                    "params": {
                        "queryVector": dense_vector if type(dense_vector)==list else dense_vector.tolist()
                    }
                },
                "min_score": 1.90
            }
        }
    }
    try:
        search_res = es.search(index=es_index, body=complex_query, request_timeout=100)
        return search_res
    except Exception as e:
        print("Exception in candidate search(Only Image):",question_code,e.__str__())
        return None

def search(es, es_index, id, question_code, is_question_text_present, question_text, 
           is_answer_text_present, whole_answer_body,
           is_question_image_dense_present, question_image_dense,
           is_answer_image_dense_present, answer_image_dense,
          column_names): 
    # -------- Find image field and dense vector from question-answer ---------------------
    if is_question_image_dense_present==True:
        image_field = "question_image_dense"
    elif is_answer_image_dense_present==True:
        image_field = "answer_image_dense"
    else:
        image_field = None
    # ---------------- only text is present -------------------------------------------
    if is_question_text_present==True and image_field is None:
        search_result = only_text(es, es_index, question_text, question_code, is_answer_text_present)
        if search_result:
            finalresult = []
            for res in search_result['hits']['hits']:
                result = res['_source']
                score = res['_score']
                finalresult.append([result, score])
            output = find_dups(finalresult, question_code)
            if len(output) >= 1:
                formated_output = [(
                    id, question_code, question_text, whole_answer_body, question_image_dense, answer_image_dense,
                    x[0], x[1], x[2], x[3], x[4], x[5]
                    ) for x in output] 
                formated_laven = calculate_laven_ratios(formated_output, True, is_answer_text_present)
#                 return formated_laven
                return [t.Row(*column_names)(*x) for x in formated_laven]
            else:
                return []
        else:
            return []
            
    # ---------------- text + image is present -------------------------------------------
    elif is_question_text_present==True and image_field is not None:
        search_result = text_with_image(es, es_index, question_text, image_field, question_code,is_answer_text_present)
        if search_result:
            finalresult = []
            for res in search_result['hits']['hits']:
                result = res['_source']
                score = res['_score']
                finalresult.append([result, score])
            output = find_dups(finalresult, question_code)
            if len(output) >= 1:
                formated_output = [(
                    id, question_code, question_text, whole_answer_body, question_image_dense, answer_image_dense,
                    x[0], x[1], x[2], x[3], x[4], x[5]
                    ) for x in output] # TODO: add answer text for query ques, add both imgs
                formated_laven = calculate_laven_ratios(formated_output, True, is_answer_text_present)
                return [t.Row(*column_names)(*x) for x in formated_laven]
            else:
                return []
        else:
            return []

    # ---------------- only image is present -------------------------------------------
    elif is_question_text_present==False and image_field is not None:
        dense_vector = question_image_dense if image_field=="question_image_dense" else answer_image_dense
        search_result = only_image(es, es_index, image_field, dense_vector, question_code,is_answer_text_present)
        if search_result:
            finalresult = []
            for res in search_result['hits']['hits']:
                result = res['_source']
                score = res['_score']
                finalresult.append([result, score])
            output = find_dups(finalresult, question_code)
            if len(output) >= 1:
                formated_output = [(
                    id, question_code, question_text, whole_answer_body, question_image_dense, answer_image_dense,
                    x[0], x[1], x[2], x[3], x[4], x[5]
                    ) for x in output] # TODO: add answer text for query ques, add both imgs
                formated_laven = calculate_laven_ratios(formated_output, False, is_answer_text_present)
#                 return formated_laven
                return [t.Row(*column_names)(*x) for x in formated_laven]
            else:
                return []
        else:
            return []

    else:
        return []

def select_candidates_driver(partition, acc, es_nodes, es_index):
#     ------------------------ Configs ------------------------
    es = Elasticsearch(es_nodes)
    column_names = ['original_question_code', 'duplicate_question_code', "original_id", "duplicate_id", "Laven_ratios", "answer_laven_ratios", 
            "original_question_text", "duplicate_question_text", "original_answer_text", "duplicate_answer_text",
            "question_image_similarity", "answer_image_similarity"]
#     ------------------------ Code ----------------------------
    for _,row in enumerate(partition):
        acc += search(es, es_index, row.id, row.question_code, row.is_question_text_present, row.question, 
           row.is_answer_text_present, row.whole_answer_body,
           row.is_question_image_dense_present, row.question_image_dense,
           row.is_answer_image_dense_present, row.answer_image_dense, column_names)

def process_data_select_candidates(df, acc, es_nodes, es_index):
    df.foreachPartition(lambda x: select_candidates_driver(x,acc,es_nodes,es_index))
    return acc.value
## ------------------------------------------- Categorize into Exact and Similar Groups -------------------------------------------
def make_lower(text):
    return text.lower()

def remove_punct(text):
    punctuations = r'''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    no_punct = ""
    for char in text:
        if char not in punctuations:
            no_punct = no_punct + char
    return no_punct

def remove_stop_words(text):
    word_tokens = word_tokenize(text)
    filtered_sentence = []
    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w)
    string = ' '.join(word for word in filtered_sentence)
    return string

@F.udf(t.FloatType())
def clean_laven_ratios(dup,org):
    dup = remove_punct(dup)
    org = remove_punct(org)

    dup = remove_stop_words(dup)
    org = remove_stop_words(org)

    clean_ratio = lavenstein_ratio(org, dup)
    return clean_ratio

# 7.
def check_text_similarity(org,dup,clean_laven_ratios):
        dup = remove_stop_words(dup)
        org = remove_stop_words(org)

        dup = remove_punct(dup)
        org = remove_punct(org)

        dup = make_lower(dup)
        org = make_lower(org)

        org = org.split(" ")
        dup = dup.split(" ")

        x = []
        y = []

        for index, value in enumerate(org):
            try:
                if (value != dup[index]):
                    value = porter.stem(value)
                    x.append(value)
            except:
                value = porter.stem(value)
                x.append(value)

        for index, value in enumerate(dup):
            try:
                if (value != org[index]):
                    value = porter.stem(value)
                    y.append(value)
            except:
                value = porter.stem(value)
                y.append(value)

        org = ' '.join(word for word in x)
        dup = ' '.join(word for word in y)

        clean_ratio = lavenstein_ratio(org, dup)

        if (clean_laven_ratios >= 0.98):
            if (clean_laven_ratios >= 1.0):
                return 1
            if org in [""," "] or dup in [""," "]:
                return 1
            if clean_ratio >= 0.80:
                return 1
            else:
                return 0

        if (clean_laven_ratios < 0.91):
            return 0

        if org in [""," "] or dup in [""," "]:
            return 1

        if (clean_ratio >= 0.84):
            return 1
        else:
            return 0

def check_image_similarity(sim_score):
    if sim_score > 0.99:
        return 1
    else:
        return -1

def find_similarity_by_body(text_sim,img_sim):
    if text_sim is None and img_sim is None:
        return None
    else:
        if text_sim is not None:
            if text_sim==1:
                if img_sim is not None:
                    if img_sim==1:
                        return 1
                    else:
                        return 0
                else:
                    return 1
            else:
                return 0
        else:
            return img_sim # -1 
            # if img_sim==1:
            #     return 1
            # else:
            #     return -1
                        
@F.udf(t.IntegerType())
def check_duplicate_exact(
    Laven_ratios,original_question_text,duplicate_question_text,clean_laven_ratios,question_image_similarity, 
    answer_laven_ratios,original_answer_text,duplicate_answer_text,answer_clean_laven_ratios,answer_image_similarity
):
    if str(Laven_ratios)!="nan":
        question_text_similarity = check_text_similarity(original_question_text,duplicate_question_text,clean_laven_ratios)
    else:
        question_text_similarity = None
    
    if question_image_similarity!=0:
        question_image_similarity = check_image_similarity(question_image_similarity)
    else:
        question_image_similarity = None
    
    if str(answer_laven_ratios)!="nan":
        answer_text_similarity = check_text_similarity(original_answer_text,duplicate_answer_text,answer_clean_laven_ratios)
    else:
        answer_text_similarity = None
    
    if answer_image_similarity!=0:
        answer_image_similarity = check_image_similarity(answer_image_similarity)
    else:
        answer_image_similarity = None
         
    question_body_similarity = find_similarity_by_body(question_text_similarity, question_image_similarity)

    if question_body_similarity is not None:
        if question_body_similarity in [0,-1]:
            return question_body_similarity
        else:
            answer_body_similarity = find_similarity_by_body(answer_text_similarity,answer_image_similarity)
            if answer_body_similarity is not None:
                if answer_body_similarity==-1:
                    return 0
                else:
                    return answer_body_similarity
            else:
                return question_body_similarity
    else:
        return -1
