import pyspark.sql.functions as F
import pyspark.sql.types as t
from pyspark.sql import Row

import torch
import torch.nn as nn
import torchvision.models as models
import torchvision.transforms as transforms
import numpy as np
import tqdm

from PIL import Image
import io
from urllib.request import urlopen
from urllib.parse import urlsplit, urlunsplit, quote
from w3lib.url import safe_url_string


device = torch.device("cpu")
layer_output_size = 512
model_name = "resnet-18"
layer = "default"
model_cache_path = "/tmp/"
model_url_dict = {
    "resnet-18":"https://download.pytorch.org/models/resnet18-5c106cde.pth",
    "alexnet":"https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth"
}
model_url = model_url_dict.get(model_name,"https://download.pytorch.org/models/resnet18-5c106cde.pth")

scaler = transforms.Resize((224, 224)) # Scale()
normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                      std=[0.229, 0.224, 0.225])
to_tensor = transforms.ToTensor()


schema_img2vec = t.StructType([
    t.StructField("question_image_dense", t.ArrayType(t.FloatType()), False),
    t.StructField("answer_image_dense", t.ArrayType(t.FloatType()), False),
    t.StructField("is_question_image_dense_present", t.BooleanType(), False),
    t.StructField("is_answer_image_dense_present", t.BooleanType(), False)])

def get_vec(img, tensor=False):
    """ Get vector embedding from PIL image
    :param img: PIL Image or list of PIL Images
    :param tensor: If True, get_vec will return a FloatTensor instead of Numpy array
    :returns: Numpy ndarray
    """
    if type(img) == list:
        a = [normalize(to_tensor(scaler(im))) for im in img]

        images = torch.stack(a).to(device) 
        if model_name == 'alexnet':
            my_embedding = torch.zeros(len(img), layer_output_size)
        else:
            my_embedding = torch.zeros(len(img), layer_output_size, 1, 1)

        def copy_data(m, i, o):
            my_embedding.copy_(o.data)

        h = extraction_layer.register_forward_hook(copy_data)
        h_x = model(images)
        h.remove()

        if tensor:
            return my_embedding
        else:
            if model_name == 'alexnet':
                return my_embedding.numpy()[:, :]
            else:
                print(my_embedding.numpy()[:, :, 0, 0].shape)
                return my_embedding.numpy()[:, :, 0, 0]
    else:
        image = normalize(to_tensor(scaler(img))).unsqueeze(0).to(device)

        if model_name == 'alexnet':
            my_embedding = torch.zeros(1, layer_output_size)
        else:
            my_embedding = torch.zeros(1, layer_output_size, 1, 1)

        def copy_data(m, i, o):
            my_embedding.copy_(o.data)

        h = extraction_layer.register_forward_hook(copy_data)
        h_x = model(image)
        h.remove()

        if tensor:
            return my_embedding
        else:
            if model_name == 'alexnet':
                return my_embedding.numpy()[0, :]
            else:
                return my_embedding.numpy()[0, :, 0, 0]

def _get_model_and_layer(model_name, layer):
    """ Internal method for getting layer from model
    :param model_name: model name such as 'resnet-18'
    :param layer: layer as a string for resnet-18 or int for alexnet
    :returns: pytorch model, selected layer
    """
    global layer_output_size
    if model_name == 'resnet-18':
        model = models.resnet18()
        model.load_state_dict(torch.hub.load_state_dict_from_url(model_url, model_dir= model_cache_path,progress=False))
        if layer == 'default':
            layer = model._modules.get('avgpool')
            layer_output_size = 512
        else:
            layer = model._modules.get(layer)

        return model, layer

    elif model_name == 'alexnet':
        model = models.alexnet()
        model.load_state_dict(torch.hub.load_state_dict_from_url(model_url, model_dir= model_cache_path,progress=False))

        if layer == 'default':
            layer = model.classifier[-2]
            layer_output_size = 4096
        else:
            layer = model.classifier[(-1)*layer]

        return model, layer

    else:
        raise KeyError('Model %s was not found' % model_name)

model, extraction_layer = _get_model_and_layer(model_name, layer)

model = model.to(device)

model.eval()

def preprocess_image(img):
    try:
        desired_size = 224
        old_size = img.size  # old_size[0] is in (width, height) format

        ratio = float(desired_size)/max(old_size)
        new_size = tuple([int(x*ratio) for x in old_size])

        img = img.resize(new_size, Image.ANTIALIAS)

        new_img = Image.new("RGB", (desired_size, desired_size), (255,255,255))
        new_img.paste(img, ((desired_size-new_size[0])//2,
                                    (desired_size-new_size[1])//2))
        return new_img
    except Exception as e:
        raise(e.__str__())

def iri2uri(iri):
    """
    Convert an IRI to a URI
    """
    uri = ''
    if isinstance(iri, str):
        (scheme, netloc, path, query, fragment) = urlsplit(iri)
        scheme = quote(scheme)
        netloc = netloc.encode('idna').decode('utf-8')
        path = quote(path)
        query = quote(query)
        fragment = quote(fragment)
        uri = urlunsplit((scheme, netloc, path, query, fragment))

    return uri

def download_and_preprocess_images(qcode,ques,ans):
    question_images, answer_images = [], []

    for url in ques:
        try:
            # url = iri2uri(url)
            url = safe_url_string(url)
            response = urlopen(url,timeout=2)
            img = response.read()
            img = Image.open(io.BytesIO(img)).convert("RGBA")
            question_images.append(preprocess_image(img))
        except Exception as e:
            print(f"Exception in Question Image Processing({qcode}, {url}): {e.__str__()}")
            # failed_urls.append([qcode,url,e.__str__()])
            
    for url in ans:
        try:
            # url = iri2uri(url)
            url = safe_url_string(url)
            response = urlopen(url,timeout=2)
            img = response.read()
            img = Image.open(io.BytesIO(img)).convert("RGBA")
            answer_images.append(preprocess_image(img))
        except Exception as e:
            print(f"Exception in Answer Image Processing({qcode}, {url}): {e.__str__()}")
            # failed_urls.append([qcode,url,e.__str__()])
    return question_images, answer_images

@F.udf(schema_img2vec)
def find_image_dense_representation(question_code,question_images,answer_images):
    try:
        if len(question_images)>0 or len(answer_images)>0:
            question_images, answer_images = download_and_preprocess_images(question_code, question_images, answer_images)
            question_image_dense = np.array([get_vec(img) for img in question_images]).mean(axis=0,dtype="float16").tolist()
            answer_image_dense = np.array([get_vec(img) for img in answer_images]).mean(axis=0,dtype="float16").tolist()
        else:
            question_image_dense = []
            answer_image_dense = []

        is_question_image_dense_present = False if str(question_image_dense)=="nan" or len(question_image_dense)==0 else True
        is_answer_image_dense_present = False if str(answer_image_dense)=="nan" or len(answer_image_dense)==0 else True

        if not is_question_image_dense_present:
            question_image_dense = np.zeros((512,),dtype="float16").tolist()
        if not is_answer_image_dense_present:
            answer_image_dense = np.zeros((512,),dtype="float16").tolist()
            
    except Exception as e:
        print(e.__str__())
        question_image_dense = np.zeros((512,),dtype="float16").tolist()
        answer_image_dense = np.zeros((512,),dtype="float16").tolist()
        is_question_image_dense_present = False
        is_answer_image_dense_present = False
        
    return t.Row(
        "question_image_dense","answer_image_dense","is_question_image_dense_present","is_answer_image_dense_present"
    )(question_image_dense,answer_image_dense,is_question_image_dense_present,is_answer_image_dense_present)