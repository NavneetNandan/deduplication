import json
import pyspark.sql.functions as F
import pyspark.sql.types as t
from pyspark.sql import Row
from elasticsearch import Elasticsearch, helpers
from tqdm import tqdm

import re
from bs4 import BeautifulSoup
from pylatexenc.latex2text import LatexNodes2Text
# import json
# from elasticsearch import helpers
import math 
import Levenshtein
import json


schema = t.StructType([
    t.StructField("fullDocument", t.StringType(), False),
    t.StructField("optype", t.StringType(), False),
    t.StructField("id", t.IntegerType(), False),
    t.StructField("question_code", t.StringType(), False),
    t.StructField("latest_status", t.StringType(), False),
    t.StructField("latest_version", t.IntegerType(), False),
    t.StructField("change_in_content", t.BooleanType(), False)
])


@F.udf(schema)
def decoder(s):
    loaded_json = json.loads(s.decode('utf-8'))

    _id = int(loaded_json.get("id"))
    question_code = loaded_json.get("question_code")
    optype = loaded_json.get("optype")
    latest_status = loaded_json.get("latest_status")
    latest_version = loaded_json.get("latest_version")
    change_in_content = bool(loaded_json.get("change_in_content"))
    fullDoc = json.dumps(loaded_json.get("fullDocument"))

    return t.Row(
        "fullDocument", "optype", "id", "question_code", "latest_status", "latest_version", "change_in_content"
    )(fullDoc, optype, _id, question_code, latest_status, latest_version, change_in_content)

def find_offsets(spark, kafka_config, kafka_checkpoint_path):
    try:
        offsets_df = spark.read.parquet(kafka_checkpoint_path)
        offsets_dict = offsets_df.rdd.map(lambda x: (x[0],x[1])).collectAsMap()
        print("offsets_dict:",offsets_dict)
    except Exception as e:
        print("Exception in read offsets:",e.__str__())
        kafka_config["startingOffsets"] = "earliest" #""" {"cqi_deduplication_cg_delta":{"0":90000,"1":90000,"2":90000}} """ 
        print("kafka_config:",kafka_config)
        return kafka_config

    topic_name = kafka_config.get("subscribe")
    print("topic name:",topic_name)
    
    #""" {"cqi_deduplication_cg_delta":{"0":1020911,"1":1020911,"2":1020911}} """
    kafka_config["startingOffsets"] = json.dumps({topic_name: {str(k):v for k,v in offsets_dict.items()}})
    print("kafka_config:",kafka_config)
    return kafka_config

def fetch_delta_changes_from_kafka(spark, kafka_config, kafka_checkpoint_path):
    kafka_config = find_offsets(spark, kafka_config, kafka_checkpoint_path)
    # kafka_config["startingOffsets"] = """ {"cqi_deduplication_cg_delta":{"0":90000,"1":90000,"2":90000}} """ #"earliest"
    # kafka_config["endingOffsets"] = """ {"cqi_deduplication_cg_delta":{"0":103075,"1":102881,"2":102635}} """ #"earliest"
    try:
        change_df = spark.read.format("kafka").options(**kafka_config).load()
        change_df.show(5)
    except Exception as e:
        print("Exception in read kafka:",e.__str__())
        kafka_config["startingOffsets"] = "earliest" #""" {"cqi_deduplication_cg_delta":{"0":90000,"1":90000,"2":90000}} """ 
        change_df = spark.read.format("kafka").options(**kafka_config).load()
    
    return change_df


def delete_doc_generator(ids,index_name):
    for _id in tqdm(ids,desc="bulk delete on ES"):
        yield {
            "_index": index_name,
            "_type": "_doc",
            "_op_type": "delete",
            "_id": _id
        }
    raise StopIteration

def delete_doc_driver(es,index_name,delete_ids):
    op = helpers.bulk(es, delete_doc_generator(delete_ids,index_name),raise_on_error= False,stats_only = True)
    print("delete summary:\ndeleted successfully:",op[0],"deletion failed:",op[1])
    return 

@F.udf(t.StringType())
def field_with_version(field,version):
    return str(field)+"__"+str(version)

def update_doc_generator(docs, index_name):
    for doc in tqdm(docs, desc="bulk update on ES"):
        yield {
            "_index": index_name,
            "_type": "_doc",
            "_op_type": "update",
            "_id": doc["id"],
            "_source": {
                "script": {
                    "source": "ctx._source.question_version = params.latest_version; ctx._source.question_code_with_version = params.qcode_with_version; ctx._source.question_id_with_version = params.id_with_version; ctx._source.status = params.latest_status;",
                    "lang": "painless",
                    "params": {
                        "latest_version": doc["latest_version"],
                        "qcode_with_version": doc["qcode_with_version"],
                        "id_with_version": doc["id_with_version"],
                        "latest_status": doc["latest_status"]
                    }
                }
            }
        }
    raise StopIteration

def update_doc_driver(es, index_name, update_version_df):
    update_dicts = list(map(lambda row: row.asDict(),update_version_df.select("id","latest_version","id_with_version","qcode_with_version").collect()))

    op = helpers.bulk(
        es,
        update_doc_generator(
            update_dicts, index_name
        ),
        raise_on_error=False,
        yield_ok = False
    )
    
    return [int(x.get("update",{}).get("_id")) for x in op[1]]

def Parse_Question_Text(question_info_object):
    try:
        qcontent = re.sub('&nbsp;', ' ', question_info_object).replace('&there4;', ' ')
        return qcontent
    except:
        return question_info_object

def clean(text):
    try:
        text = str(text)
        if not text:
            return "No explanation_given"
        else:
            text = re.sub(r"<gdiv.*?>(.|\r|\n)*?<\/gdiv>","",text)
            text = re.sub(r"(<\/gdiv>)","",text)
            soup = BeautifulSoup(text, 'html.parser')
            soup.prettify()
            soup_text = soup.get_text()
            soup_text = soup_text.replace("\\\\", "\\")
            soup_text = soup_text.replace("\\n", " ")
            try:
                soup_text = LatexNodes2Text(keep_comments=True,math_mode="with-delimiters").latex_to_text(soup_text,tolerant_parsing=True)
                if soup_text[-1]=="$":
                    soup_text = soup_text[:-1]
            except:
                print(soup_text)
                soup_text = soup_text.strip('\"').replace('\\\\', '\\').replace('\\"', '\"').replace('\\n','\n').replace("\\r", "\r").replace("\\t", "\t")
                try:
                    soup_text = LatexNodes2Text(keep_comments=True,math_mode="with-delimiters").latex_to_text(soup_text,tolerant_parsing=True)
                    if soup_text[-1]=="$":
                        soup_text = soup_text[:-1]
                except:
                    pass

            soup_text = soup_text.strip('\"')
            # soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            # soup_text = re.sub(r"(^Q.\w+.:)", '', soup_text)
            # soup_text = re.sub(r"(^Q\w+.)", '', soup_text)
            # soup_text = re.sub(r"(^Ans\w+.)", '', soup_text)
            soup_text = soup_text.replace("\n", " ")
            soup_text = soup_text.replace("\r", " ")
            soup_text = soup_text.replace("  ", " ")
            soup_text = soup_text.replace("   ", " ")
            soup_text = re.sub(r'\n', "", soup_text)
            soup_text = re.sub(r'\r', "", soup_text)
    except Exception as e:
        print(e.__str__())

    return soup_text

def remove_page_margin(content):
    if content.startswith("<!--"):
        return ""
    else:
        return content

def clean_answer_options_driver(answer_text):
    return remove_page_margin(clean(Parse_Question_Text(answer_text.strip())))

def fetch_urls(text):
    try:
        if type(text)!=str or len(text)==0:
            return []
        else:
            return re.findall(r"src\s*=\s*[\"|'](.+?)['|\"]",text)
    except Exception as e:
        print(e.__str__())
        return []


schema = t.StructType([
    t.StructField("subtype", t.StringType(), True),
    t.StructField("question_text", t.StringType(), True),
    t.StructField("whole_answer_body", t.StringType(), True),
    t.StructField("learning_maps", t.ArrayType(t.StringType()), True),
    t.StructField("question_urls", t.ArrayType(t.StringType()), True),
    t.StructField("answer_urls", t.ArrayType(t.StringType()), True)
    ])

def parse_question_details(qd):
    key = [k for k,v in qd.items() if v is not None]
    key = key[0] if len(key)>0 else "key"
    question_txt = qd.get(key,{}).get("question_txt","")
    answer_options = qd.get(key,{}).get("answers",[])
    if any(type(x)==list for x in answer_options):
        answer_options = [y for x in answer_options for y in x]
    whole_answer_body = [x.get("body","") for x in answer_options]

    return question_txt, whole_answer_body

@F.udf(schema)
def return_relevant_fields(content):
    try:
        true = True
        false = False
        lo = json.loads(content)

        subtype = lo.get("subtype","")
        qid = lo.get("id","")
        content = lo.get("content",{})

        ## Question Details
        qd = content.get("question_details")
        aq = content.get("associated_questions")
        
        question_text, whole_answer_body = [], []

        if qd:
            qt,at = parse_question_details(qd)
            question_text.append(qt)
            whole_answer_body.extend(at)
        
        if aq:
            for x in aq:
                qt,at = parse_question_details(x.get('content',{}).get('question_details',{}))
                question_text.append(qt)
                whole_answer_body.extend(at)
        
        ## URLs
        question_urls = fetch_urls(" ".join(question_text))
        answer_urls = fetch_urls(" ".join(whole_answer_body))
        
        ## question and whole answer body
        question_text = " ".join(sorted(map(clean_answer_options_driver,question_text))).strip()
        whole_answer_body = " ".join(sorted(map(clean_answer_options_driver,whole_answer_body))).strip()
        
        ## Question Meta
        qm = content.get("question_meta_tags",[])
        lms = []
        for x in qm:
            lms.extend(x.get("learning_maps",[]))
            
        lms = list(set(lms))


    except Exception as e:
        print("Exception in return_relevant_fields: (question id:",qid,")",e.__str__())
        subtype = None
        question_txt = None
        whole_answer_body = None
        lms = None
        question_urls, answer_urls = None,None
        
    return t.Row('subtype','question_text', 'whole_answer_body',"learning_maps","question_urls","answer_urls")(subtype,question_txt,whole_answer_body,lms,question_urls,answer_urls)

def process_fulldocument_and_fetch_relevant_content(df):
    df = df.withColumn(
        "fullDocument",return_relevant_fields(F.col("fullDocument"))
    ).select(
        "id",F.col("latest_version").alias("_version"),F.col("latest_status").alias("status"),"question_code","fullDocument.*"
    )
    df = df.filter(df.subtype!="LinkedComprehension")
    df = df.dropna("any")
    return df
