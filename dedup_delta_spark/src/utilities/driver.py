import subprocess
import pyspark.sql.functions as F
import pyspark.sql.types as t
from pyspark.sql import Row
from pyspark import StorageLevel
import graphframes as GF
from pyspark.sql.window import Window

import warnings
warnings.filterwarnings("ignore")

from elasticsearch import Elasticsearch, helpers
import os
from tqdm import tqdm

from utilities.config import PATHS, DATABASES, GOALS, FORMAT_REFERENCES, VERSION, KAFKA_CONFIG

import utilities.delta_utils as du
import utilities.utils as utils
import utilities.image_vectorization as IV
import utilities.select_candidates as sc
import utilities.spark_variables as sv


class Dedup():
    def __init__(self, spark, status_filters_clustering, kafka_env):
        self.directory = PATHS.get("base")

        self.spark = spark
        self.kafka_config = KAFKA_CONFIG.get(kafka_env,"staging")

        self.status_filters_clustering = status_filters_clustering
        self.mongo_config = DATABASES.get("mongo")
        self.es_config = DATABASES.get("ES")
        self.es_nodes = DATABASES.get("ES_CLUSTER_NODES")
        self.es = Elasticsearch(self.es_nodes,timeout=30, max_retries=5, retry_on_timeout=True)
        self.es_content_bank = Elasticsearch(DATABASES.get("CQI_CB_ES"),timeout=30, max_retries=5, retry_on_timeout=True)

        # self.status_filters = status_filters
        self.mongo_config["spark.mongodb.input.collection"] = "learning_objects"
        self.lo_coll = self.mongo_config.copy()
        self.mongo_config["spark.mongodb.input.collection"] = "learning_maps"
        self.lm_coll = self.mongo_config.copy()
        self.mongo_config["spark.mongodb.input.collection"] = "learning_map_formats"
        self.lmf_coll = self.mongo_config.copy()

        # self.grade = grade
        self.all_data_dump = os.path.join(self.directory,VERSION,'cg-processed-questions.parquet')
        self.candidate_pairs_dump = os.path.join(self.directory,VERSION,'cg-candidate_pairs.parquet')
        self.candidate_pairs_exact = os.path.join(self.directory,VERSION,'cg-candidate_pairs_exact.parquet')
        self.candidate_pairs_exact_temp = os.path.join(self.directory,VERSION,'cg-candidate_pairs_exact_temp.parquet')
        self.cluster_exact_file = os.path.join(self.directory,VERSION,'cg_exact_duplicates_clusters.parquet')
        self.cluster_similar_file = os.path.join(self.directory,VERSION,'cg_similar_duplicates_clusters.parquet')
        self.not_present_in_CB = os.path.join(self.directory,VERSION,'missing_cb.parquet')
        self.not_present_in_LM = os.path.join(self.directory,VERSION,'missing_lm.parquet')
        self.joined_q_level_df_with_cluster = os.path.join(self.directory,VERSION,'joined_q_level_df_with_cluster.parquet')
        self.checkpoint_dir = os.path.join(self.directory,VERSION,"checkpoint/")
        self.kafka_checkpoint_path = os.path.join(self.directory,VERSION,"kafka_checkpoint.parquet")
        
        self.es_index = "cg-duplication-questions_batch_v3_reindexed" #"cg-duplication-questions" + "_" + str.lower(VERSION) 
        self.final_index_name = "cg-results_dedup" + "_" + str.lower(VERSION)

    def driver(self):
        # 0. Fetch Delta from Kafka queue
        change_df = du.fetch_delta_changes_from_kafka(self.spark, self.kafka_config.copy(), self.kafka_checkpoint_path).persist(StorageLevel.MEMORY_AND_DISK)

        offsets_df = change_df.select("partition", "offset").groupBy("partition").agg(F.max("offset").alias("offset")).persist(StorageLevel.MEMORY_AND_DISK)

        change_df = change_df.withColumn(
            "value", du.decoder(F.col("value"))
        ).select(
            "value.*"
        )
        
        delta_df = change_df.orderBy(["id", "latest_version"]).groupBy(change_df.id).agg(
            F.last(F.col("question_code")).alias("question_code"),
            F.last(F.col("optype")).alias("optype"), F.last(
                F.col("latest_status")).alias("latest_status"),
            F.last(F.col("latest_version")).alias("latest_version"), F.last(
                F.col("fullDocument")).alias("fullDocument"),
            F.max(F.col("change_in_content")).alias("change_in_content")
        ).persist(StorageLevel.MEMORY_AND_DISK)

        
        print("count delta df:",delta_df.count())
        delta_df.groupBy("optype").agg({"id":"count"}).show()
        print("kafka checkpoint df:")
        offsets_df.show()

        change_df.unpersist()

            # 0.1 delete los from ES
        delete_los_df = delta_df.filter(
            (
                delta_df.optype=="delete"
            ) | (
                (
                    delta_df.optype.isin(["update","insert"])
                ) & (
                    delta_df.change_in_content==True
                )
            )
        ).persist(StorageLevel.MEMORY_AND_DISK)
        # print("delete_los_df:",delete_los_df.count())

        delete_ids = delete_los_df.select("id").rdd.flatMap(lambda x:x).collect()
        print("delete_ids:",len(delete_ids))
        du.delete_doc_driver(self.es,self.es_index,delete_ids)
            
            # 0.2 update los on ES
        update_version_df = delta_df.filter(
            (delta_df.optype == "update") & (delta_df.change_in_content==False)
        )
        update_version_df = update_version_df.withColumn(
            "id_with_version", du.field_with_version(F.col("id"),F.col("latest_version"))
        )
        update_version_df = update_version_df.withColumn(
            "qcode_with_version", du.field_with_version(F.col("question_code"),F.col("latest_version"))
        ).persist(StorageLevel.MEMORY_AND_DISK)
        # print("update_version_df:",update_version_df.count())
            
            # 0.3 missing los from ES
        missing_ids = du.update_doc_driver(self.es, self.es_index, update_version_df)
        print("missing ids:",len(missing_ids))
            
            # 0.4 insert los on ES
        insert_df = update_version_df.select('id', 'question_code', 'optype', 'latest_status', 'latest_version', 'fullDocument', 'change_in_content').filter(update_version_df.id.isin(missing_ids))
        change_in_los_df = delete_los_df.filter(delete_los_df.optype.isin(["update","insert"]))
        insert_df = insert_df.union(change_in_los_df)

        # 1.
        union_df = du.process_fulldocument_and_fetch_relevant_content(insert_df).persist(StorageLevel.MEMORY_AND_DISK)
        # union_df = union_df.limit(10000).persist(StorageLevel.MEMORY_AND_DISK)
        print("union_df",union_df.show(5,truncate=False))

        # 3.1 Preprocess text data
        try:
            input_process = utils.Proccessing(grail_df=union_df)
            grail_dataframe = input_process.start_processing()
        except Exception as e:
            print("*"*10,"Exception in Process Text:",e.__str__(),"*"*10)
        
        # 4.0 Compute meta fields
        grail_dataframe = grail_dataframe.withColumn(
            "question_id_with_version",
            F.concat(
                F.col("id").cast("string"),
                F.lit("__"),
                F.col("_version").cast("string")
            )
        )
        grail_dataframe = grail_dataframe.withColumn(
            "question_code_with_version",
            F.concat(
                F.col("question_code"),
                F.lit("__"),
                F.col("_version").cast("string")
            )
        )

        # 3.0 Fetch Dense Representation
        grail_dataframe = grail_dataframe.withColumn(
            "dense_vectors",IV.find_image_dense_representation(
                    F.col("question_code"),F.col("question_urls"),F.col("answer_urls")
                )
            ).persist(StorageLevel.MEMORY_AND_DISK)

        grail_dataframe = grail_dataframe.select(
            "id", F.col("_version").alias("question_version"), "status", "question_code", "subtype",
            "learning_maps",
            "question_id_with_version", "question_code_with_version",
            "is_question_text_present", F.col("question_info_clean_complete").alias("question"), 
            "is_answer_text_present", "whole_answer_body",
            'question_urls', 'answer_urls',"dense_vectors.*"
            # 'question_image_dense', 'answer_image_dense',
            # "is_question_image_dense_present", "is_answer_image_dense_present"
        )

        # 4.1 Store Data on HDFS and ES
        #### 1. HDFS/WASB
        grail_dataframe.write.mode("overwrite").parquet(self.all_data_dump)
        delta_df.unpersist()
        delete_los_df.unpersist()
        update_version_df.unpersist()
        union_df.unpersist()
        
        #### 2. ES
        print("Count of Questions:",grail_dataframe.count())
        utils.driver_ingest_on_es(self.es, self.es_index, grail_dataframe, self.es_config)
        
        # grail_dataframe = self.spark.read.parquet(self.all_data_dump) # TEMP
        # 5. Find Candidates
        # qcode_to_qid_dict_broadcast = self.spark.sparkContext.broadcast(grail_dataframe.rdd.map(lambda x: (x[2],x[0])).collectAsMap())
        # grail_dataframe = grail_dataframe.filter(grail_dataframe.status.isin(self.status_filters_clustering)).persist()

        # print("grail df sameple:",grail_dataframe.show(5))
        # acc = self.spark.sparkContext.accumulator(list(), sv.ListAccumulator())
        # acc_value = sc.process_data_select_candidates(grail_dataframe,acc,self.es_nodes,self.es_index)
        # grail_dataframe.unpersist()
        
        # # 6. Prepare candidate dataframe, calculate ratios and decide similarity
        # candidate_df = self.spark.createDataFrame(acc_value)
        # print("Count of Candidates:",len(acc_value))
        
        # ## Compute Clean Laven Scores
        # candidate_df = candidate_df.withColumn("clean_laven_ratios",sc.clean_laven_ratios(F.col("duplicate_question_text"),F.col("original_question_text")))
        # candidate_df = candidate_df.withColumn("answer_clean_laven_ratios",sc.clean_laven_ratios(F.col("duplicate_answer_text"),F.col("original_answer_text")))

        # ## Find Similarity Flags
        # candidate_df = candidate_df.withColumn("pred_label",sc.check_duplicate_exact(
        #     F.col("Laven_ratios"),F.col("original_question_text"),F.col("duplicate_question_text"),
        #     F.col("clean_laven_ratios"),F.col("question_image_similarity"),F.col("answer_laven_ratios"),
        #     F.col("original_answer_text"),F.col("duplicate_answer_text"),F.col("answer_clean_laven_ratios"),
        #     F.col("answer_image_similarity")
        # ))

        # # # 7. Add Question IDs for Original and Duplicate Questions
        # # @F.udf(t.IntegerType())
        # # def return_question_id(question_code):
        # #     return qcode_to_qid_dict_broadcast.value[question_code]

        # # candidate_df = candidate_df.withColumn("original_id",return_question_id(F.col("original_question_code")))
        # # candidate_df = candidate_df.withColumn("duplicate_id",return_question_id(F.col("duplicate_question_code")))
        
        # candidate_df = candidate_df.dropna(subset=["original_id","duplicate_id"]).persist(StorageLevel.MEMORY_AND_DISK)

        # print("Sample Candidate_df:",candidate_df.show(10,truncate=False))
        # candidate_df.write.mode("overwrite").parquet(self.candidate_pairs_dump)
        # del acc_value

        # candidate_df_exact = candidate_df.filter(candidate_df.pred_label == 1)
        # candidate_df_exact = candidate_df_exact.withColumn("original_id",F.col("original_id").cast("int")).withColumn("duplicate_id",F.col("duplicate_id").cast("int"))
        # candidate_df_exact = candidate_df_exact.select("original_id","duplicate_id")
        # print("candidate_df_exact schema:",candidate_df_exact.printSchema())

        # ## Reload candidate_df_exact, remove deleted/updated/inserted questions, union of old and new candidate_df_exact DFs
        # candidate_df_exact_primary = self.spark.read.parquet(self.candidate_pairs_exact).select("original_id","duplicate_id")
        # candidate_df_exact_primary = candidate_df_exact_primary.withColumn("original_id",F.col("original_id").cast("int")).withColumn("duplicate_id",F.col("duplicate_id").cast("int"))
        # print("candidate_df_exact_primary schema:",candidate_df_exact_primary.printSchema())
        
        # print("delete ids:", len(delete_ids), delete_ids[:5])
        # candidate_df_exact_primary = candidate_df_exact_primary.filter(
        #     (
        #         ~candidate_df_exact_primary.original_id.isin(delete_ids)
        #     )&(
        #         ~candidate_df_exact_primary.duplicate_id.isin(delete_ids)
        #     )
        # )
        
        
        # candidate_df_exact = candidate_df_exact.union(
        #     candidate_df_exact_primary.select(
        #         candidate_df_exact.columns
        #         )
        #     ).distinct().persist()

        # print("count of candidates in master + delta df:",candidate_df_exact.count())
        
        
        # candidate_df_exact.write.mode("overwrite").parquet(self.candidate_pairs_exact_temp)
        
        # candidate_df_exact.write.mode("overwrite").parquet(self.candidate_pairs_exact)
        # print("candidate_df_exact sample:",candidate_df_exact.sample(False, 0.1, seed=0).limit(5).show(5))
        
        # candidate_df.unpersist()
        # offsets_df.write.mode("overwrite").parquet(self.kafka_checkpoint_path)

        # # 8. Clustering Exact and Similar Questions
        # verticesDf_exact= candidate_df_exact.select("original_id").union(candidate_df_exact.select("duplicate_id")).distinct().withColumnRenamed('original_id', 'id').persist()
        # verticesDf_exact.show(5)
        # graph_exact = GF.GraphFrame(verticesDf_exact,candidate_df_exact.select(F.col("original_id").alias("src"),F.col("duplicate_id").alias("dst")))

        # # verticesDf_similar= candidate_df_similar.select("original_id").union(candidate_df_similar.select("duplicate_id")).distinct().withColumnRenamed('original_id', 'id')
        # # graph_similar = GF.GraphFrame(verticesDf_similar,candidate_df_similar.select(F.col("original_id").alias("src"),F.col("duplicate_id").alias("dst")))
        # try:
        #     _ = subprocess.call(["hdfs","dfs","-rm","-f","-r",self.checkpoint_dir]) # remove checkpoint directory
        # except Exception as e:
        #     print("Exception in subprocess call:",e.__str__())

        # self.spark.sparkContext.setCheckpointDir(self.checkpoint_dir)

        # cc_exact = graph_exact.connectedComponents().persist()
        # # cc_similar = graph_similar.connectedComponents().persist() 

        # # 9. Store Clusters on HDFS
        # cc_exact.write.mode("overwrite").parquet(self.cluster_exact_file)

        # verticesDf_exact.unpersist()
        # candidate_df_exact.unpersist()
        # # cc_similar.write.mode("overwrite").parquet(self.cluster_similar_file)

        # # # Temp Code: Remove this later
        # # exact_cluster = cc_exact.groupby("component").agg(F.collect_set("id").alias("exact_questions"))
        # # cc_exact = cc_exact.join(exact_cluster,"component","left")

        # # es_write_config = self.es_config.copy()
        # # es_write_config["es.resource"] = "{}/_doc".format(self.final_index_name)
        # # es_write_config["es.read.field.as.array.include"] = 'exact_questions'
        
        # # cc_exact.write.format(
        # #     "org.elasticsearch.spark.sql"
        # # ).options(**es_write_config).mode("overwrite").save()

        # # ------------------------------------ Post Processing ---------------------------------------------
        
        # # 1. Fetch All Questions Meta From "Question's Index"
        # es_read_config = self.es_config.copy()
        # es_read_config["es.resource"] = "{}/_doc".format(self.es_index)
        # es_read_config["es.read.field.as.array.include"] = "learning_maps"
        # es_read_config["es.read.field.include"] = "id,question_code,status,question_version,question_id_with_version,question_code_with_version,learning_maps,is_question_image_dense_present,is_answer_image_dense_present"
        
        # questions_df = self.spark.read.format(
        #     "org.elasticsearch.spark.sql"
        # ).options(**es_read_config).load().persist()
        
        # questions_df_cols = questions_df.columns
        # try:
        #     qid_idx = questions_df_cols.index("id")
        # except:
        #     qid_idx = 0
        # try:
        #     qversion_idx = questions_df_cols.index("question_version")
        # except:
        #     qversion_idx = 7

        # qid_to_qversion_broadcast = self.spark.sparkContext.broadcast(questions_df.rdd.map(lambda x: (x[qid_idx],x[qversion_idx])).collectAsMap())
        # questions_df = questions_df.filter(questions_df.status.isin(self.status_filters_clustering)).persist()

        # # 2. Prepare all Exact Clusters (or Extend Exact Cluster DF with non-clustered Questions)
        # exact_ques = cc_exact.select("id").rdd.flatMap(lambda x: x).collect()
        # exact_ques = list(set(exact_ques))
        # all_ques = questions_df.select("id").rdd.flatMap(lambda x: x).collect()
        # non_clustered_ques = list(set(all_ques)-set(exact_ques))

        # rdd1 = self.spark.sparkContext.parallelize(non_clustered_ques)
        # row_rdd = rdd1.map(lambda x: Row(x))
        # non_clustered_ques = self.spark.createDataFrame(row_rdd,["id"])

        # non_clustered_ques = non_clustered_ques.withColumn("component",F.col("id"))
        # non_clustered_ques = non_clustered_ques.select("id",F.col("component").cast("long"))

        # cl_df = cc_exact.union(non_clustered_ques.select(cc_exact.columns)) # id, component (cluster id)

        # # 3. GroupBy on Cluster DF to get exact clusters (cluster_id, question_id/set)
        # exact_clusters_df = cl_df.groupby("component").agg(F.collect_set("id").alias("question_id")).select(F.col("component").alias("cluster_id"),"question_id")

        # # 4. Find Exact Cluster with version
        # # similar_clusters = cc_similar.groupby("component").agg(F.collect_set("id").alias("question_id")).select("question_id").rdd.flatMap(lambda x: x).collect()
        # # similar_clusters_broadcast = self.spark.sparkContext.broadcast(list(map(set,similar_clusters)))
        
        # exact_clusters_df = exact_clusters_df.withColumn(
        #     "exact_questions_with_version",utils.curried_find_version(
        #         qid_to_qversion_broadcast
        #     )(
        #         F.col("question_id")
        #     )
        # ).select("cluster_id",F.col("question_id").alias("exact_questions"),"exact_questions_with_version")
        
        # # 5. Attach Cluster Info with Individual Questions and Add Cluster info with Meta df
        # cl_df = cl_df.select("id",F.col("component").alias("cluster_id"))
        # los_with_cluster_info = cl_df.join(exact_clusters_df,"cluster_id")

        # joined_q_level_df_with_cluster = los_with_cluster_info.join(questions_df,"id","left")

        # joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.withColumn("size_exact_cluster",F.size("exact_questions"))
        # # joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.withColumn("size_similar_cluster",F.size("similar_questions"))

        # # 6. Store on ES
        # joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.select(
        #     F.col('id').alias("question_id"), 'learning_maps','cluster_id', 'exact_questions', 'exact_questions_with_version', 'question_code', 
        #     'question_version', 'is_question_image_dense_present', 'is_answer_image_dense_present', 'size_exact_cluster',
        #     'question_id_with_version', 'question_code_with_version'
        # )
        
        # joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.filter(joined_q_level_df_with_cluster.question_id.isin(all_ques))
        # joined_q_level_df_with_cluster = joined_q_level_df_with_cluster.dropDuplicates(["question_id"])

        # es_write_config = self.es_config.copy()
        # es_write_config["es.resource"] = "{}/_doc".format(self.final_index_name)
        # es_write_config["es.read.field.as.array.include"] = 'exact_questions,exact_questions_with_version,learning_maps'
        
        # joined_q_level_df_with_cluster.write.format(
        #     "org.elasticsearch.spark.sql"
        # ).options(**es_write_config).mode("overwrite").save()

        # try:
        #     _ = subprocess.call(["hdfs","dfs","-rm","-f","-r",self.checkpoint_dir]) # remove checkpoint directory
        # except Exception as e:
        #     print("Exception in subprocess call:",e.__str__())