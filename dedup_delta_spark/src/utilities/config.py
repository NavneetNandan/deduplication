appName = "CQI-Deduplication"
VERSION = "batch_v3" # Keep this in Small Letters only

PATHS = {
	"base": "wasb:///user/sshuser/data_dedup_modularized"
}

DATABASES = {
	"mongo":{
		"spark.mongodb.input.uri":"mongodb://ro_dsl:EHJpUwVO2vgMuk@tech-contentgrail-mongo-prod-01:27017,tech-contentgrail-mongo-prod-02:27017,tech-contentgrail-mongo-prod-03:27017,tech-contentgrail-mongo-prod-04:27017/?authSource=contentgrail&replicaSet=cg-mongo-prod",
		"spark.mongodb.input.database": "contentgrail",
		"spark.mongodb.input.collection": "",
		"spark.mongodb.input.readPreference.name": "primaryPreferred"
	},
	"ES": {
		"es.nodes": "10.141.11.88,10.141.11.89,10.141.11.90",
		"es.port" : '9200',
		"es.nodes.wan.only": "true",
		"es.resource": ""
	},
	"ES_CLUSTER_NODES": ["10.141.11.88","10.141.11.89","10.141.11.90"],
	"CQI_CB_ES": "10.144.131.98"
}

GOALS = {
	"all": [
		"1st CBSE","2nd CBSE","3rd CBSE","4th CBSE","5th CBSE","6th CBSE","7th CBSE","8th CBSE","9th CBSE","10th CBSE","11th CBSE","12th CBSE",
		"Engineering", "Medical",
		"Banking", "Banking and Clerical", "Teaching",
		],
	"K12": ["1st CBSE","2nd CBSE","3rd CBSE","4th CBSE","5th CBSE","6th CBSE","7th CBSE","8th CBSE","9th CBSE","10th CBSE","11th CBSE","12th CBSE"],
	"prepg": ["Banking", "Banking and Clerical", "Teaching"],
	"preug": ["Engineering", "Medical"]
}

FORMAT_REFERENCES = {
	"all": [
		'5ec5867a0c88fe5860961943', '5ec586730c88fe5860961938','5ec586760c88fe586096193c', '5e9b564928e1174ef2785843','5ec586740c88fe586096193a',
		"5ec586890c88fe5860961958","5f17102be61885046e5d9780","5f171296e61885046e5da6a9","5f17177ee61885046e5db8d0","5ec5867f0c88fe5860961948"
		],
	"K12": ['5ec5867a0c88fe5860961943', '5ec586730c88fe5860961938',
                    '5ec586760c88fe586096193c', '5e9b564928e1174ef2785843',
                    '5ec586740c88fe586096193a'],
	"prepg": [],
	"preug": ["5ec586890c88fe5860961958","5f17102be61885046e5d9780","5f171296e61885046e5da6a9","5f17177ee61885046e5db8d0","5ec5867f0c88fe5860961948"]
}

KAFKA_CONFIG = {
	"prod": {
		"kafka.bootstrap.servers": '10.144.131.17:9092,10.144.131.18:9092,10.144.131.19:9092',
		"subscribe": 'cqi_deduplication_cg_delta_prod',
		"startingOffsets": "",
		# "endingOffsets": "latest",
		# "enable.auto.commit": True,
	},
	"staging": {
		"kafka.bootstrap.servers": '10.144.20.35:9092,10.144.20.37:9092,10.144.20.38:9092',
		"subscribe": 'cqi_deduplication_cg_delta', # cqi_deduplication_cg_delta_staging
		"startingOffsets": "",
		# "endingOffsets": "latest",
		# "enable.auto.commit": True
	}
}



# "startingOffsets": """ {"cqi_deduplication_cg_delta":{"0":1020911,"1":1020911,"2":1020911}} """