from sqlalchemy import create_engine,text
import sys
sys.path.append('/home/ubuntu')
import pandas as pd
from bs4 import BeautifulSoup
import ast
import re
import html
import math
import numpy as np
import math
import json
from pprint import pprint
from elasticsearch import Elasticsearch,helpers
es = Elasticsearch("10.144.20.4")
#es.indices.create('cqi-duplicate-contentadmin')
num_threads = 100
expected_output = []
#from lsh import cache, minhash
#hasher = minhash.MinHasher(seeds=1000, char_ngram=5, hashbytes=4)
#lshcache = cache.Cache(num_bands=200, hasher=hasher)
import Levenshtein
import csv
#import networkx as nx
import threading

from sqlalchemy import create_engine,text
import pandas as pd
from bs4 import BeautifulSoup
import ast
import re
import html
import math
import numpy as np
import time
import threading
num_threads = 100
expected_output = []
from elasticsearch import Elasticsearch,helpers
#es = Elasticsearch("10.140.10.7")
#from lsh import cache, minhash
#hasher = minhash.MinHasher(seeds=1000, char_ngram=5, hashbytes=4)
#lshcache = cache.Cache(num_bands=200, hasher=hasher)
import Levenshtein
import csv




new_df = pd.read_json("new_grail_data",orient="records")
all_contents_new_columns = new_df[new_df["question_images"].apply(lambda x: x[0]["image_0"]=="No images")]
all_contents_new_columns = all_contents_new_columns[all_contents_new_columns["answer_images"].apply(lambda x: x[0]["image_0"]=="No images")]
print(all_contents_new_columns.columns)
list_production = all_contents_new_columns.values.tolist()

print(len(list_production))
def lavenstein_ratio(set_a, set_b):
    return Levenshtein.ratio(set_a, set_b)

def fetch_Laven(result_df):
    if str(result_df.duplicate_question_text) != ' ':
        return lavenstein_ratio(str(result_df.original_question_text),str(result_df.duplicate_question_text))
    else:
        return 0

def calculate_laven_ratios(formated_output):
    laven_df = pd.DataFrame(formated_output,columns=['original_question_code','original_question_text','duplicate_question_code','duplicate_question_text'])
    laven_df['Laven_ratios'] = laven_df.apply(fetch_Laven,axis=1)
    laven_df = laven_df[laven_df['Laven_ratios']>0.59]
    if laven_df.empty:
        return []
    else:
        laven_df_quest = laven_df[['original_question_code','duplicate_question_code',"Laven_ratios"]]
        laven_list = laven_df_quest.values.tolist()
        return laven_list

def find_dups(query,finalresult,length,question_code):
    ids=[]
    for result,score in finalresult:
        if result['question_code'] == question_code:
            continue
        ids.append((result['question_code'],result['question_ans_clean'],result['question'],result['correct_option']))
    return ids


def search(question_text,answer_text,question_code,expected_output):

    #search_query = { "query": {"match_phrase_prefix": {"question_ans_clean": {"query": query, "slop": 15, "max_expansions": 5}}}}
    if answer_text:
        query = str(question_text) + " " + str(answer_text)
        length = len(query)
         #complex_query['query']['match'].append({'question_ans_clean': {'query': question_answer_clean}})
        field = "question_ans_clean"
    else:
        query = question_text
        length = len(query)
        #complex_query['query']['match'].append({'question': {'query': question_answer_clean}})
        field = "question"
    complex_query = {
        "size" : 50,
        "query": {
            "match": {
                field: {
                    "query": query
                }
            }
        }
    }
    search = es.search(index='cqi-duplicate-grail-staging', doc_type='my_type', body=complex_query)
    finalresult = []
    for res in search['hits']['hits']:
        result = res['_source']
        score = res['_score']
        finalresult.append([result, score])
    output = find_dups(query, finalresult, length, question_code)
    if len(output) >= 1:
        formated_output = [(question_code,question_text,x[0],x[2]) for x in output]
        formated_laven = calculate_laven_ratios(formated_output)
        expected_output.extend(formated_laven)

    else:
        return 

threads = [threading.Thread(target=search, args=(items[10],items[1],items[8],expected_output)) for items in list_production]
    
for i in range(0,int(len(threads)/num_threads)+1):
    [ti.start() for ti in threads[i*num_threads:(i+1)*num_threads]]
    [ti.join() for ti in threads[i*num_threads:(i+1)*num_threads]]

candidate_df = pd.DataFrame(expected_output,columns=['original_question_code','duplicate_question_code',"Laven_ratios"])
candidate_df.to_csv("multi_quest_CA.csv")
main_question_text= pd.merge(all_contents_new_columns, candidate_df, left_on = 'question_code', right_on = 'original_question_code')
print("1st")
main_question_text = main_question_text.drop(columns=['question_code','created_at','exams','is_prev_year','is_practice','is_test'])
main_question_text = main_question_text.rename(index=str, columns={"question_answer_clean": "original_question_answer_clean","question_info_clean_complete":"original_question_info_clean_complete","correct_option":"original_correct_option"})
duplicate_question_text = pd.merge(all_contents_new_columns, main_question_text, left_on = 'question_code', right_on = 'duplicate_question_code')
print("2nd")
duplicate_question_text = duplicate_question_text.rename(index=str, columns={"question_answer_clean": "duplicate_question_answer_clean","question_info_clean_complete":"duplicate_question_info_clean_complete","correct_option":"duplicate_correct_option"})

duplicate_question_text = duplicate_question_text.drop(columns=['question_code','created_at','exams','is_prev_year','is_practice','is_test'])
duplicate_question_text.to_csv("multi_quest_grail_phy.csv")
def fetch_Laven_ratios(result_df):
    return lavenstein_ratio(str(result_df.original_question_answer_clean),str(result_df.duplicate_question_answer_clean))


def fetch_jaccard_ratios(result_df):
    shingles_a = shingles(result_df.original_question_answer_clean)
    shingles_b = shingles(result_df.duplicate_question_answer_clean)
    jaccard_sim = jaccard(shingles_a, shingles_b)
    return jaccard_sim
    
def fetch_Minhash_ratios(result_df):
    fingerprint_a = set(hasher.fingerprint(str(result_df.original_question_answer_clean).encode('utf8')))
    fingerprint_b = set(hasher.fingerprint(str(result_df.duplicate_question_answer_clean).encode('utf8')))
    minhash_sim = len(fingerprint_a & fingerprint_b) / len(fingerprint_a | fingerprint_b)
    return minhash_sim

def shingles(text, char_ngram=5):
    text = str(text)
    return set(text[head:head + char_ngram] for head in range(0, len(text) - char_ngram))


def lavenstein_ratio(set_a, set_b):
    return Levenshtein.ratio(set_a, set_b)


def jaccard(set_a, set_b):
    intersection = set_a & set_b
    union = set_a | set_b
    if len(union) != 0:
        return len(intersection) / len(union)
    else:
        return 0

#duplicate_question_text['Minhash_ratios'] = duplicate_question_text.apply(fetch_Minhash_ratios,axis=1)
#print("minhash")
#duplicate_question_text['jaccard_ratios'] = duplicate_question_text.apply(fetch_jaccard_ratios,axis=1)
#print("jaccard")
#duplicate_question_text['Laven_ratios'] = duplicate_question_text.apply(fetch_Laven_ratios,axis=1)
print("laven")
duplicate_question_text.to_csv("multi_grail.csv")