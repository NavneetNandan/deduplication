from pyspark import AccumulatorParam
from enum import Enum

class ListAccumulator(AccumulatorParam):

    def zero(self, init_value: list):
        return init_value

    def addInPlace(self, v1: list, v2: list):
        return v1 + v2


# class JobContext(object):
#     def __init__(self,sc):
#         self.acc = sc.accumulator(list(), ListAccumulator())
    
#     def inc_acc(self,val):
#         self.acc += val