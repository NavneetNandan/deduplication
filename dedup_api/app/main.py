from fastapi import FastAPI
import uvicorn
from routers.handles import router

def main():
    app = FastAPI(title="CQI-Deduplication-App")
    app.include_router(router)
    uvicorn.run(app,host="0.0.0.0")

if __name__ == "__main__":
    main()