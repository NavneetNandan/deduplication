from elasticsearch import Elasticsearch
from pymongo import MongoClient
from config.functional_config import ES_URL, K12_INDEX, PREPG_INDEX, PREUG_INDEX, MONGO_URL
from fastapi import HTTPException
import pandas as pd

from modules.dedup_driver import Dedup

es = Elasticsearch(ES_URL)
client = MongoClient(MONGO_URL)
dedup_object = Dedup(es)

def fetch_dedup_results(id, field_name, goal):
    query_body = {
        "query": {
            "bool": {
                "filter": [
                    {
                        "bool": {
                            "should": [
                                {
                                    "match": {
                                        field_name: id
                                    }
                                }
                            ],
                            "minimum_should_match": 1
                        }
                    }
                ]
            }
        },
        "_source":["exact_questions"]
    }
    if goal=="K12":
        index_name = K12_INDEX
    elif goal=="prepg":
        index_name = PREPG_INDEX
    elif goal=="preug":
        index_name = PREUG_INDEX
    else:
        raise HTTPException(422, detail="Invalid goal name '{}'".format(goal))
    
    resp = es.search(query_body,index_name)
    resp = resp["hits"]["hits"]

    if len(resp)==0:
        return 0, {}
    else:
        return 1, {field_name:id, "exact_duplicates":resp[0]["_source"]["exact_questions"]}

def find_duplicates(id,field_name,goal):
    print(field_name)
    filter={
        field_name: id
    }
    project={
        'id': 1, 
        'question_code': 1, 
        'content': 1,
        "_version": 1,
        "subtype":1
    }

    result = client['contentgrail']['learning_objects'].find(
    filter=filter,
    projection=project
    )

    los = [x for x in result]

    if len(los)>0:
        df = pd.DataFrame(los)
        df = df.sort_values("_version").head(1) # select latest LO which has max '_version' number
        resp = dedup_object.dedup_driver(df, goal)
        return resp
    else:
        return {"message":"{} {} is not present in collection 'learning_objects'".format(field_name,id)}