from fastapi import APIRouter, HTTPException
from modules.utils import fetch_dedup_results, find_duplicates
from config.functional_config import GOALS
from typing import Optional

router = APIRouter()

@router.get("/candidates")
def get_duplicates(question_id: Optional[int] = None, question_code: Optional[str] = None, goal: str = None):
    if not goal or goal not in GOALS:
        raise HTTPException(status_code=422, detail="\'goal\' is missing or It is incorrect")
    if not question_code and not question_id:
        raise HTTPException(status_code=422, detail="Either 'question_id' or 'question_code' is required")
    else:
        field_name = ""
        value_dict = {"question_id":question_id, "question_code":question_code}
        if question_id:
            field_name = "question_id"
            hits,response = fetch_dedup_results(question_id,field_name,goal)
        else:
            field_name = "question_code"
            hits,response = fetch_dedup_results(question_code,field_name,goal)
        
        if hits:
            return response
        else:
            # return {"message":"no match found"}
            value = value_dict[field_name]
            name = field_name if field_name=="question_code" else "id"
            response = find_duplicates(value, name, goal)
            return response