from elasticsearch import Elasticsearch,helpers
es=Elasticsearch([{'host':'10.144.20.4','timeout':500}])



def search(question_code,source):
    complex_query = {
        "query": {
            "match": {
                "question_code": {
                    "query": question_code
                }
            }
        }
    }

    try:
        if source == 'content-admin':
            index_name = 'cqi-duplicate-contentadmin'
        elif source == 'production':
            index_name = 'cqi-duplicate-production'
        elif source == "content-grail":
            index_name = 'cqi-duplicate-grail'
        
        search = es.search(index=index_name, doc_type='my_type', body=complex_query)
        question = search['hits']['hits'][0]['_source']["question"]
        answer = search['hits']['hits'][0]['_source']["correct_option"]
        print("answer",answer)
        duplicate_question_image_url = ast.literal_eval(search["hits"]["hits"][0]["_source"]["question_images"])[0]["image_0"]
        print("duplicate_question_image_url",duplicate_question_image_url)

        return (question_code,question,answer,duplicate_question_image_url)
    except Exception as e: 
        print("I am in Exception Atleast")
        print(e)
        return 0
    

    
while(1):
    for i in ["EM0066521","EM0075710","EM0075698","EM0066509","IL0003554","IL0003576","EM0075687","EM0066499","EM0024856","EM0024857","EM0066711","EM0076183","EM0021656","EM0021657","EM0024792"]:
        search(i,"content-grail")

    #search("EM0459512","content-grail")
    
    