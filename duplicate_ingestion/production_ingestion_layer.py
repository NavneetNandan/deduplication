from sqlalchemy import create_engine,text
import sys
sys.path.append('/home/ubuntu')
import pandas as pd
import ast
import re
import html
import math
import numpy as np
import time
import csv
from datetime import date,timedelta
import datetime
import json
from connection import DBConnection
import random 


class Prod_Ingestion:
    def __init__(self,ydtime,full_run = False):
        self.connection = DBConnection()
        self.prod_connection = self.connection.get_production_connection()
        self.ca_connection = self.connection.get_ca_connection()
        self.full_run = full_run
        self.ydtime = ydtime


    def get_todays_date(self):
        today = date.today()
        td = today.strftime("%Y-%m-%d")
        ydtime = "00:00:00"
        tdtime = "6:00:00"
        tdtime = td+" "+tdtime
        yesterday = date.today() - timedelta(1)
        yd = yesterday.strftime("%Y-%m-%d")
        ydtime = yd+" "+ydtime
        return (ydtime,tdtime,yesterday,td)


    def get_question_data_from_prod(self,ydtime,tdtime):
        if self.full_run:
            query = "select question_code, version, answers, bodies as question_info, category, updated_at from content_qvers qv where version = (select max(version) from content_qvers where qv.question_code = question_code)"

        else:
            query = "select question_code, version, answers, bodies as question_info, category, updated_at from content_qvers qv where version = (select max(version) from content_qvers where qv.question_code = question_code) and updated_at between '{}' and '{}'".format(self.ydtime,tdtime)
        
        self.question_answer_df = pd.read_sql_query(query,self.prod_connection)
        self.idlist = self.question_answer_df["question_code"].tolist()
    
    @staticmethod
    def is_practice(x):
        if x:
            if "Practice" in x:
                return 1
        else:
            return 0

    @staticmethod
    def is_test(x):
        if x:
            if "Test" in x:
                return 1
        else:
            return 0

    @staticmethod
    def tag_exams(x):
        if x !=0:
            main_list = []
            list_of_exams = list(ast.literal_eval(x))
            for exam in list_of_exams:
                dicti = {"exam_id":exam}
                main_list.append(dicti)
            return main_list
        else:
            return [{'exam_id': -1}]
            

    @staticmethod
    def Parse_answer_Text(answerinfo_object):
        explan = None
        correct_option = None
        list_of_options = []
        for each_answer in answerinfo_object:
            option_text = None
            option_is_correct = None
            explanation = None
            try:
                if 'body' in each_answer.keys():
                    option_text = each_answer['body']['en']
                    option_text = re.sub('&nbsp;',' ',option_text).replace('&there4;', ' ').replace('&#39;','')

                elif 'bodies' in each_answer.keys():
                    option_text = each_answer['bodies']['en']
                    option_text = re.sub('&nbsp;',' ',option_text).replace('&there4;', ' ').replace('&#39;','')
                list_of_options.append(option_text)
            except Exception as e:
                pass

            try:
                option_is_correct = each_answer['correct']
                option_is_correct = re.sub('&nbsp;',' ',option_is_correct).replace('&there4;', ' ').replace('&#39;','').replace('<sup>','').replace('</sup>','').replace('<br>','').replace('<br />','')
            except Exception as e:
                pass
            try:
                explanation = each_answer['explanation']
                explanation = re.sub('&nbsp;',' ',explanation).replace('&there4;', ' ').replace('&#39;','').replace('<sup>','').replace('</sup>','').replace('<br>','').replace('<br />','')
            except Exception as e:
                pass
        
            if option_is_correct == True:
                try:
                    explan = explanation['en']
                
                    explan = re.sub('&nbsp;',' ',explan).replace('&there4;', ' ').replace('&#39;','').replace('<sup>','').replace('</sup>','').replace('<br>','').replace('<br />','')
                
                except Exception as e:
                    pass
                correct_option = option_text
            
        return [correct_option,list_of_options,explan]



    def get_question_test_mode(self,d1,yesterday):
        csv_path = '/home/ubuntu/cqi/questionHygiene/Views/Prod'+d1+'data.csv'
        try:
            self.question_test_mode = pd.read_csv(csv_path)
        except:
            csv_path = '/home/ubuntu/cqi/questionHygiene/Views/Prod'+yesterday+'data.csv'
            self.question_test_mode = pd.read_csv(csv_path)

        self.question_test_mode.purpose.fillna(0,inplace = True)
        self.question_test_mode['is_practice'] = self.question_test_mode.purpose.apply(lambda x: self.is_practice(x))
        self.question_test_mode['is_test'] = self.question_test_mode.purpose.apply(lambda x: self.is_test(x))
        self.question_test_mode.exams_tagged.fillna(0,inplace = True)

        self.question_test_mode['exams'] = self.question_test_mode.exams_tagged.apply(lambda x:self.tag_exams(x))
        self.question_test_mode.is_prev_year.fillna(False,inplace = True)
        self.question_test_mode.is_prev_year.replace(False, 0, inplace=True)

    def merge_qa_test(self):
        question_answer_test_df = pd.merge(self.question_answer_df,self.question_test_mode,left_on="question_code",right_on="qcode",how='left')
        self.question_answer_test_df_5 = question_answer_test_df[["question_code","question_info","answers","is_practice","created_at","is_test","exams","is_prev_year","category"]]

    def get_learning_map(self):
        if self.full_run:
            question_learningmap_df  = pd.read_sql_query("select qt.question_id,q.question_code,s.name as subject,s.id as subject_id from questions_topics qt inner join subjects s on qt.subject_id = s.id inner join questions q on q.id=qt.question_id",self.ca_connection)
        else:
            question_learningmap_df  = pd.read_sql_query("select qt.question_id,q.question_code,s.name as subject,s.id as subject_id from questions_topics qt inner join subjects s on qt.subject_id = s.id inner join questions q on q.id=qt.question_id where q.question_code in ('%s')" % ("','".join(self.idlist)),self.ca_connection)

        question_learningmap_df.drop(columns='subject_id',inplace=True)
        question_learningmap_df.drop_duplicates(inplace=True)
        self.question_learningmap_df_grouped = question_learningmap_df.groupby("question_code").agg({"subject":'first'}).reset_index()

    def merge_learning_map(self):
        all_contents = pd.merge(self.question_answer_test_df_5,self.question_learningmap_df_grouped,on="question_code",how='left')
        self.all_contents_new = all_contents.groupby('question_code').agg({'question_info':'first', 'answers': 'first','subject':'first','is_test':'first','is_practice':'first','created_at':'first','exams':'first','is_prev_year':'first','category':'first'}).reset_index()

        self.all_contents_new.dropna(subset=['question_info'],inplace=True)
        self.all_contents_new["answers"].fillna("", inplace=True)
        self.all_contents_new["subject"].fillna("dummy", inplace=True)
        self.all_contents_new["is_practice"].fillna(0, inplace=True)
        self.all_contents_new["is_test"].fillna(0, inplace=True)
        self.all_contents_new.rename(index=str, columns={"category": "question_type"},inplace=True)
        self.all_contents_new['answer_info'] = self.all_contents_new['answers'].apply(lambda x: self.Parse_answer_Text(x))
        self.all_contents_new['answers'] = self.all_contents_new['answer_info'].apply(lambda x: x[0])


    def get_data_from_prod(self):
        ydtime,tdtime,yesterday,today = self.get_todays_date()
        
        self.get_question_data_from_prod(ydtime,tdtime)

        if len(self.idlist) == 0:
            return pd.DataFrame()
            

        self.get_question_test_mode(today,yesterday)
        self.merge_qa_test()
        self.get_learning_map()
        self.merge_learning_map()
        return self.all_contents_new
       



