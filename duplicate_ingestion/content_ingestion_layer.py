from sqlalchemy import create_engine,text
import sys
sys.path.append('/home/ubuntu')
import pandas as pd
import ast
import re
import html
import math
import numpy as np
import time
import csv
from datetime import date,timedelta
import datetime
import json
import random 



class CA_Ingestion:

    def __init__(self,ydtime,full_run=False):
        engine = create_engine("mysql+pymysql://root:P0o9i8u7lpkojihu@content-admin-production-read-replica.crdfxi46vdem.ap-southeast-1.rds.amazonaws.com/content_admin")
        self.ca_connection = engine.connect()
        self.full_run = full_run
        self.ydtime = ydtime


    def get_todays_date(self):
        today = date.today()
        td = today.strftime("%Y-%m-%d")
        ydtime = "00:00:00"
        tdtime = "6:00:00"
        tdtime = td+" "+tdtime
        yesterday = date.today() - timedelta(1)
        yd = yesterday.strftime("%Y-%m-%d")
        ydtime = yd+" "+ydtime
        return (ydtime,tdtime,yd,td)
    
    @staticmethod
    def find_cg_url(text):
        try:
            urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
            urls = list(set(urls))
            if not urls:
                return 0
            return urls
        except:
            return 0

    @staticmethod
    def remove_backslash(lis_text):
        if lis_text == 0:
            return 0
    
        main_list = []
        for text in lis_text:
            new_text = text.replace("\\","")
            main_list.append(new_text)

        return main_list
    
    @staticmethod
    def get_url_es(x):
        main_list = []
        if x == 0:
            return [{"image_0":"No images"}]
        for index,each in enumerate(x):
            main_list.append({"image_"+str(index):str(each)})
        return main_list

    def get_question_data_from_ca(self,ydtime,tdtime):
        '''if self.full_run:
            question_df = pd.read_sql_query("select questions.id, questions.questioninfo,questions.question_code,questions.updated_at,"\
                "lookups.lookup_value from questions LEFT JOIN lookups ON questions.question_type=lookups.id",con = self.ca_connection)
        else:
            question_df = pd.read_sql_query("select questions.id, questions.questioninfo,questions.question_code,questions.updated_at,"\
                "lookups.lookup_value from questions LEFT JOIN lookups ON questions.question_type=lookups.id where questions.updated_at "\
                "BETWEEN '{}' AND '{}'".format(self.ydtime,tdtime),con = self.ca_connection)

        question_df.to_csv("questions.csv")'''
        question_df = pd.read_csv("questions.csv")
        question_df.drop_duplicates(inplace=True)
        question_df["question_code"] = question_df.question_code.apply(str)
        self.questionsdf_dup = question_df[~question_df['question_code'].str.contains("DUP")]
        self.questionsdf_dup.dropna(inplace=True)
        self.questionsdf_dup['id'] = self.questionsdf_dup.id.astype(float)
        self.questionsdf_dup["question_images"] = self.questionsdf_dup['questioninfo'].apply(self.find_cg_url)
        self.questionsdf_dup["question_images"] = self.questionsdf_dup['question_images'].apply(self.remove_backslash)
        self.questionsdf_dup["question_images"] = self.questionsdf_dup['question_images'].apply(self.get_url_es)
        self.idlist = self.questionsdf_dup['id'].tolist()

  

    def get_answer_data(self):
        if self.full_run:
            answer_df = pd.read_sql_query("select answerinfo,is_correct,question_id from answers ",self.ca_connection)
        else:
            answer_df = pd.read_sql_query("select answerinfo,is_correct,question_id from answers where question_id in (" + ','.join((str(n) for n in self.idlist )) + ")",self.ca_connection)
        answer_df = pd.read_csv("answers.csv")
        answersdf_1 = answer_df[answer_df["is_correct"]==1]
        self.answersdf_2 = answersdf_1.groupby("question_id")['answerinfo'].apply(lambda answerinfo: ','.join(answerinfo)).to_frame()
        answer_df.to_csv("answers.csv")
        self.answersdf_2 = self.answersdf_2.reset_index()

        

    
    def merge_question_answer(self):
        self.question_answer_df = pd.merge(self.questionsdf_dup,self.answersdf_2,left_on="id",right_on="question_id",how='left')
        self.question_answer_df.rename(index=str, columns={"questioninfo": "question_info","answerinfo":"answers","updated_at":"updated","question_images":"question_images"},inplace=True)


    @staticmethod
    def is_practice(x):
        if x == "Practice":
            return 1
        else:
            return 0

    @staticmethod
    def is_test(x):
        if x == "Test":
            return 1
        else:
            return 0

    @staticmethod
    def tag_exams(x):
        print(x)
        try:
            if x !='{nan}' or x !='nan':
                main_list = []
                list_of_exams = list(ast.literal_eval(x))
                for exam in list_of_exams:
                    dicti = {"exam_id":int(exam)}
                    main_list.append(dicti)
            
                return main_list

            else:
                return [{'exam_id': -1}]
        except:
            return [{'exam_id': -1}]





    def get_question_test_mode(self,d1,yesterday):
        #csv_path = '/home/ubuntu/cqi/questionHygiene/Views/CA'+d1+'data.csv'
        csv_path = '/Users/nikeshmangwani/Downloads/CA2019-07-31data.csv'
        try:
            self.question_test_mode = pd.read_csv(csv_path)
        except:
            csv_path = '/home/ubuntu/cqi/questionHygiene/Views/CA'+yesterday+'data.csv'
            self.question_test_mode = pd.read_csv(csv_path)

        self.question_test_mode.question_mode.fillna(0,inplace = True)
        self.question_test_mode['is_practice'] = self.question_test_mode.question_mode.apply(lambda x: self.is_practice(x))
        self.question_test_mode['is_test'] = self.question_test_mode.question_mode.apply(lambda x: self.is_test(x))
        self.question_test_mode['exams'] = self.question_test_mode.exams_tagged.apply(lambda x:self.tag_exams(x))
        self.question_test_mode.is_previous_year_qn.fillna(False,inplace = True)
        self.question_test_mode.is_previous_year_qn.replace(False, 0, inplace=True)
        self.question_test_mode.rename(index=str, columns={"is_previous_year_qn": "is_prev_year"},inplace=True)

    

    def merge_qa_test(self):
        question_answer_test_df = pd.merge(self.question_answer_df,self.question_test_mode,on="question_id",how='left')
        self.question_answer_test_df_5 = question_answer_test_df[["question_code","question_info","answers","is_practice","created_at","is_test","exams","is_prev_year","question_id","lookup_value","updated","question_images"]]
    
    def get_learning_map(self):
        '''if self.full_run:
            question_learningmap_df = pd.read_sql_query("select qt.question_id,s.name as subject,s.id as subject_id from questions_topics qt inner join subjects s on qt.subject_id = s.id",self.ca_connection)
        else:
            question_learningmap_df = pd.read_sql_query("select qt.question_id,s.name as subject,s.id as subject_id from questions_topics qt inner join subjects s on qt.subject_id = s.id where question_id in (" + ','.join((str(n) for n in self.idlist )) + ")",self.ca_connection)
        question_learningmap_df.to_csv("learning_map.csv")'''
        question_learningmap_df = pd.read_csv("learning_map.csv")
        question_learningmap_df.drop(columns='subject_id',inplace=True)
        question_learningmap_df.drop_duplicates(inplace=True)
        self.question_learningmap_df_grouped = question_learningmap_df.groupby("question_id").agg({"subject":'first'}).reset_index()

    def merge_learning_map(self):
        all_contents = pd.merge(self.question_answer_test_df_5,self.question_learningmap_df_grouped,on="question_id",how='left')
        self.all_contents_new = all_contents.groupby('question_code').agg({'question_info':'first', 'answers': 'first','subject':'first','is_test':'first','is_practice':'first','created_at':'first','exams':'first','is_prev_year':'first',"question_id":"first","lookup_value":"first","updated":"first","question_images":"first"}).reset_index()
        self.all_contents_new.dropna(subset=['question_info'],inplace=True)
        self.all_contents_new["answers"].fillna("", inplace=True)
        self.all_contents_new["subject"].fillna("dummy", inplace=True)
        self.all_contents_new["is_practice"].fillna(0, inplace=True)
        self.all_contents_new["is_test"].fillna(0, inplace=True)
        self.all_contents_new.rename(index=str, columns={"lookup_value": "question_type"},inplace=True)
        self.all_contents_new.to_csv("uncleaned_content_admin.csv")





    def get_data_from_ca(self):
        ydtime,tdtime,yesterday,today = self.get_todays_date()
        self.get_question_data_from_ca(ydtime,tdtime)

        if len(self.idlist) == 0:
            return pd.DataFrame()
        self.get_answer_data()
        self.merge_question_answer()
        self.get_question_test_mode(today,yesterday)
        self.merge_qa_test()
        self.get_learning_map()
        self.merge_learning_map()
        return self.all_contents_new
       







        


