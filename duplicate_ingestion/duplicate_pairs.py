import threading
num_threads = 500
expected_output = []
from elasticsearch import Elasticsearch,helpers
es = Elasticsearch("10.144.20.4")
import pandas as pd
from lsh import cache, minhash
hasher = minhash.MinHasher(seeds=1000, char_ngram=5, hashbytes=4)
lshcache = cache.Cache(num_bands=200, hasher=hasher)
import Levenshtein


class Duplicate_Pairs:
    @staticmethod
    def find_dups(query,finalresult,length,question_code):
        ids=[]
        for result,score in finalresult:
            if result['question_code'] == question_code:
                continue
            ids.append(result['question_code'])
        return ids
    
    def search(self,question_text,answer_text,question_code):
        if answer_text:
            query = question_text + " " + answer_text
            length = len(query)
            field = "question_ans_clean"
        else:
            query = question_text
            length = len(query)
            field = "question"
        complex_query = {
            "size" : 50,
            "query": {
                "match": {
                    field: {
                        "query": query
                    }
                }
            }
        }

        try:
            search = es.search(index='cqi-duplicate-production', doc_type='my_type', body=complex_query)
            finalresult = []
            for res in search['hits']['hits']:
                result = res['_source']
                score = res['_score']
                finalresult.append([result, score])
            output = find_dups(query, finalresult, length, question_code)
            if len(output) >= 1:
                formated_output = [(question_code,x) for x in output]
                self.expected_output.extend(formated_output)

            else:
            # no_output_codes.append((question_code,query))
                return 
        except:
            print("not found",question_code)

    def find_duplicate(self,list_data):
        self.expected_output = []
        threads = [threading.Thread(target=self.search, args=(items[8],items[0],items[7],self.expected_output)) for items in list_data]




    
    def start_finding_dup(self):
        content_admin_cleaned_df = pd.read_json('new_content_data', orient='records')
        print(content_admin_cleaned_df.head())
        #self.find_duplicate(list_data)




'''list_production = all_contents_new_columns.values.tolist()
 

def find_dups(query,finalresult,length,question_code):
    ids=[]
    for result,score in finalresult:
        if result['question_code'] == question_code:
            continue
        ids.append(result['question_code'])
    return ids


def search(question_text,answer_text,question_code,expected_output):
    #search_query = { "query": {"match_phrase_prefix": {"question_ans_clean": {"query": query, "slop": 15, "max_expansions": 5}}}}
    if answer_text:
        query = question_text + " " + answer_text
        length = len(query)
         #complex_query['query']['match'].append({'question_ans_clean': {'query': question_answer_clean}})
        field = "question_ans_clean"
    else:
        query = question_text
        length = len(query)
        #complex_query['query']['match'].append({'question': {'query': question_answer_clean}})
        field = "question"
    complex_query = {
        "size" : 50,
        "query": {
            "match": {
                field: {
                    "query": query
                }
            }
        }
    }

    try:
        search = es.search(index='cqi-duplicate-production', doc_type='my_type', body=complex_query)
        finalresult = []
        for res in search['hits']['hits']:
            result = res['_source']
            score = res['_score']
            finalresult.append([result, score])
        output = find_dups(query, finalresult, length, question_code)
        if len(output) >= 1:
            formated_output = [(question_code,x) for x in output]
            expected_output.extend(formated_output)

        else:
        # no_output_codes.append((question_code,query))
            return 
    except:
        print("not found",question_code)

threads = [threading.Thread(target=search, args=(items[8],items[0],items[7],expected_output)) for items in list_production]
    
for i in range(0,int(len(threads)/num_threads)+1):
    [ti.start() for ti in threads[i*num_threads:(i+1)*num_threads]]
    [ti.join() for ti in threads[i*num_threads:(i+1)*num_threads]]

candidate_df = pd.DataFrame(expected_output,columns=['original_question_code','duplicate_question_code'])
candidate_df.to_csv("multi_quest_prod.csv")
main_question_text= pd.merge(all_contents_new_columns, candidate_df, left_on = 'question_code', right_on = 'original_question_code')
print("1st")
main_question_text = main_question_text.drop(columns=['question_code','created_at','exams','is_prev_year','is_practice','is_test'])
main_question_text = main_question_text.rename(index=str, columns={"question_answer_clean": "original_question_answer_clean","question_info_clean_complete":"original_question_info_clean_complete","correct_option":"original_correct_option"})
duplicate_question_text = pd.merge(all_contents_new_columns, main_question_text, left_on = 'question_code', right_on = 'duplicate_question_code')
print("2nd")
duplicate_question_text = duplicate_question_text.rename(index=str, columns={"question_answer_clean": "duplicate_question_answer_clean","question_info_clean_complete":"duplicate_question_info_clean_complete","correct_option":"duplicate_correct_option"})

duplicate_question_text = duplicate_question_text.drop(columns=['question_code','created_at','exams','is_prev_year','is_practice','is_test'])'''
