from sqlalchemy import create_engine,text
import pandas as pd
from bs4 import BeautifulSoup
import ast
import re
import html
import math
import numpy as np
import math
import json
from pprint import pprint
from datetime import date,timedelta
import datetime

class Content_Grail_Ingestion:

    def __init__(self,ydtime,full_run=False):
        self.ssl = {'ssl': {'ca': 'BaltimoreCyberTrustRoot.crt.pem'}}
        self.full_run = full_run
        engine = create_engine("mysql+pymysql://mysqladmin@embibemysqlsvr-stg:Embibe@123456!@embibemysqlsvr-stg.mysql.database.azure.com/contentgrail",connect_args=self.ssl)
        self.grail_connection = engine.connect()
        self.ydtime = ydtime

    def get_todays_date(self):
        today = date.today()
        td = today.strftime("%Y-%m-%d")
        ydtime = "00:00:00"
        tdtime = "6:00:00"
        tdtime = td+" "+tdtime
        yesterday = date.today() - timedelta(1)
        yd = yesterday.strftime("%Y-%m-%d")
        ydtime = yd+" "+ydtime
        return (ydtime,tdtime,yesterday,td)

    def get_question_data_from_grail(self,ydtime,tdtime):
        if self.full_run:
            query = "SELECT distinct(question_code),version,subtype,content->'$.question_details.en.question_txt' as question_text,purpose,content->'$.question_details.en.answers[*].body' as answers,content->'$.question_details.en.answers[*].is_correct' as correctness,content->'$.question_meta_tags.topics_learn_path_code' as exams,content->'$.question_meta_tags.topics_learn_path_name' as subjects FROM learning_objects lo WHERE type='Question' and question_code is not NUll and version = (select max(version) from learning_objects where question_code = lo.question_code)"
        else:
            query = "SELECT distinct(question_code),version,subtype,content->'$.question_details.en.question_txt' as question_text,purpose,content->'$.question_details.en.answers[*].body' as answers,content->'$.question_details.en.answers[*].is_correct' as correctness,content->'$.question_meta_tags.topics_learn_path_code' as exams,content->'$.question_meta_tags.topics_learn_path_name' as subjects FROM learning_objects lo WHERE type='Question' and question_code is not NUll and version = (select max(version) from learning_objects where question_code = lo.question_code) and _updated between '{}' and '{}'".format(self.ydtime,tdtime)
        self.contentdf = pd.read_sql_query(query,self.grail_connection)

    
    @staticmethod
    def find_cg_url(text):
        try:
            urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
            urls = list(set(urls))
            if not urls:
                return 0
            return urls
        except:
            return 0

    @staticmethod
    def remove_backslash(lis_text):
        if lis_text == 0:
            return 0
    
        main_list = []
        for text in lis_text:
            new_text = text.replace("\\","")
            main_list.append(new_text)

        return main_list
    
    @staticmethod
    def is_practice(x):
        if x=='"Practice"'or x=='["Practice"]':
            return 1
        else:
            return 0

    @staticmethod
    def is_test(x):
        if x:
            if x == '"Test"' or x == '["Test"]':
                return 1
        else:
            return 0
    
    
    @staticmethod
    def get_exam_list(x):
        if x == 0:
            return [{"exam_id":-1}]
        exam_list = []
        x = ast.literal_eval(x)
        if len(x) == 0:
            return [{"exam_id":-1}]
        for i in x:
            z = i.split("--")
            temp = re.findall(r'\d+', z[1])
            if not temp:
                return [{"exam_id":-1}]

            exam_list.append({"exam_id":int(temp[0])})
        return exam_list

    @staticmethod
    def get_subject_list(x):
        if x == 0:
            return 0
        x = ast.literal_eval(x)
        if not x:
            return 0
        try:
            x = x[0]
            z = x.split("--")
            return z[2]
        except:
            print("something")

    @staticmethod
    def get_url_es(x):
        main_list = []
        if x == 0:
            return [{"image_0":"No images"}]
        for index,each in enumerate(x):
            main_list.append({"image_"+str(index):str(each)})
        return main_list

    @staticmethod
    def boolean_eval(bool_string):
        try:
            bool_string = bool_string.replace('t','T')
            bool_string = bool_string.replace('f','F')
        
            bool_string = bool_string.replace('null','False')
        except:
            print(bool_string)
            
        return ast.literal_eval(bool_string)

    @staticmethod
    def get_answer(x):
        try:
            return ast.literal_eval(x)
        except:
            #print("I am printing")
            return ""

    @staticmethod
    def join_answers(row):
        try:
            answers_list = []
            if len(row["correctness_1"]) == 0:
                return ""
    
            for idx, val in enumerate(row["correctness_1"]):
                if val == True:
                    answers_list.append(row["answers_1"][idx])

            s = ' '
            return s.join(answers_list)
        except:
            #print(row)
            pass



    def get_data_from_grail(self):
        ydtime,tdtime,yesterday,today = self.get_todays_date()
        self.get_question_data_from_grail(ydtime,tdtime)

        self.contentdf.purpose.fillna(0,inplace = True)

        self.contentdf.loc[self.contentdf['purpose'].str.contains("'Practice'",na=False), 'purpose'] = '"Practice"'

        self.contentdf['is_practice'] = self.contentdf.purpose.apply(lambda x: self.is_practice(x))

        self.contentdf['is_test'] = self.contentdf.purpose.apply(lambda x: self.is_test(x))

        self.contentdf["is_test"].fillna(0, inplace=True)

        self.contentdf["is_practice"].fillna(0, inplace=True)

        self.contentdf.loc[self.contentdf['subtype'].str.contains('True False',na=False), 'subtype'] = 'TrueFalse'

        self.contentdf.loc[self.contentdf['subtype'].str.contains('Single Choice',na=False), 'subtype'] = 'SingleChoice'

        self.contentdf.loc[self.contentdf['subtype'].str.contains('Multiple Choice',na=False), 'subtype'] = 'MultipleChoice'

        self.contentdf.loc[self.contentdf['subtype'].str.contains('Fill In The Blanks',na=False), 'subtype'] = 'FillInTheBlanks'

        self.contentdf.loc[self.contentdf['subtype'].str.contains('Subjective Numerical',na=False), 'subtype'] = 'SubjectiveNumerical'
    
        self.contentdf.loc[self.contentdf['subtype'].str.contains('IntegerType',na=False), 'subtype'] = 'Integer'

        self.contentdf.loc[self.contentdf['subtype'].str.contains('Matrix Match Grid',na=False), 'subtype'] = 'MatrixMatchGrid'

        self.contentdf["exams"].fillna(0, inplace=True)


        self.contentdf['exam_list'] = self.contentdf.exams.apply(lambda x: self.get_exam_list(x))

        self.contentdf["subjects"].fillna(0, inplace=True)


        self.contentdf['subject_list'] = self.contentdf.subjects.apply(lambda x: self.get_subject_list(x))

        self.contentdf.subject_list.replace(0, "dummy",inplace=True)

        self.contentdf['correctness'].fillna("False",inplace=True)

        self.contentdf['answers'].fillna('[]',inplace=True)

        self.contentdf['correctness_1'] = self.contentdf.apply(lambda x : self.boolean_eval(x.correctness), axis = 1)

        self.contentdf['answers_1'] = self.contentdf['answers'].apply(lambda x: self.get_answer(x))

        self.contentdf["combined_answers"] = self.contentdf.apply(self.join_answers, axis=1)

        self.contentdf = self.contentdf[["question_code","question_text","combined_answers","subject_list","is_test","is_practice","exam_list","subtype"]]

        self.contentdf["created_at"] = ""

        self.contentdf["is_prev_year"] = ""

        self.contentdf = self.contentdf[["question_code","question_text","combined_answers","subject_list","is_test","is_practice","created_at","exam_list","is_prev_year","subtype"]]

        self.contentdf.columns = ["question_code","question_info","answers","subject","is_test","is_practice","created_at","exams","is_prev_year","question_type"]
        
        self.contentdf["question_images"] = self.contentdf["question_info"].apply(self.find_cg_url)
        self.contentdf["answer_images"] = self.contentdf["answers"].apply(self.find_cg_url)
        self.contentdf["question_images"] = self.contentdf["question_images"].apply(self.remove_backslash)
        self.contentdf["answer_images"] = self.contentdf["answer_images"].apply(self.remove_backslash)

        self.contentdf["question_images"] = self.contentdf["question_images"].apply(self.get_url_es)
        self.contentdf["answer_images"] = self.contentdf["answer_images"].apply(self.get_url_es)
        return self.contentdf


























