from sqlalchemy import create_engine,text
import sys
sys.path.append('/home/ubuntu')
import pandas as pd
from bs4 import BeautifulSoup
import ast
import re
import html
import math
import numpy as np
import time
import csv
from datetime import date,timedelta
import datetime
import json
import random



class Proccessing:
    def __init__(self,ca_df=pd.DataFrame(),prod_df=pd.DataFrame(),grail_df=pd.DataFrame()):
        self.ca_df = ca_df
        self.prod_df = prod_df
        self.grail_df = grail_df
        self.prod_flag = False
        self.ca_flag = False
        self.grail_flag = False
    
    @staticmethod
    def Parse_Question_Text(question_info_object):
        try:
            question_info_object = question_info_object['en']
        except:
            pass
        try:
            qcontent = re.sub('&nbsp;',' ',question_info_object).replace('&there4;', ' ')
            return qcontent
        except:
            return question_info_object

    
    @staticmethod
    def clean(text):
        text = str(text)
        if not text:
            return "No explanation_given"
        else:
            soup = BeautifulSoup(text,'html.parser')
            soup.prettify()
            text = soup.get_text()
            text = text.replace("\n"," ")
            text = text.replace("\r"," ")
            text = text.replace("  "," ")
            text = text.replace("   "," ")
            text = re.sub(r'\n',"",text)
            text = re.sub(r'\r',"",text)
            try:
                text = ast.literal_eval(text)
                text = re.sub(r'\n',"",text)
                text = re.sub(r'\r',"",text)
            except:
                pass

            return text

    @staticmethod
    def cleaning_content(qcontent):
        try:
            qcontent = re.sub('<[^>]*>',' ',qcontent).replace('&nbsp;', ' ').replace('&there4;', ' ').replace('&#39;','').replace('<sup>','').replace('</sup>','').replace('<br>','').replace('<br />','')
        except Exception as e:
            pass
        return qcontent
    
    @staticmethod
    def get_symbols(mstring):
        try:
            mstring = str(mstring)
            list_symbols=[]
            list_items = mstring.split()
            for item in list_items:
                if(len(item)==1):
                    if((re.match('[a-z]',item) == None) and (item!=' ') and (item not in ['.',',','\'',';','?','\u2061'])):
                        list_symbols.append(item)
                
            list_symbols = list(set(list_symbols))
            return list_symbols
        except:
            print(mstring)
            return []

    @staticmethod
    def clean_data(string,list_symbols):
        try:
            string = string.split(' ')
            unwanted = []
            for i in string:
                if(len(i) == 1):
                    if(ord(i)>128 and i not in list_symbols):
                        unwanted.append(i)
            unwanted = list(unwanted)
            for i in unwanted:
                string.remove(i)
            string = ' '.join(string)
            return string
        except:
            return string

    @staticmethod
    def remove_unicode_content(qcontent):
        try:
            qcontent = re.sub('\u2061','',qcontent)
            return qcontent
        except:
            return qcontent


    @staticmethod
    def remove_page_margin(content):
        if content.startswith("<!--"):
            return ""
        else:
            return content
    
    

    def process_dataframe(self,all_contents_new):
        all_contents_new['question_info_clean'] = all_contents_new['question_info'].apply(lambda x: self.Parse_Question_Text(x))

        all_contents_new.dropna(subset=['question_info_clean'],inplace=True)

        all_contents_new['question_info_clean'] = all_contents_new['question_info_clean'].apply(lambda x: self.clean(x))

        all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean'].apply(lambda x: self.cleaning_content(x))

        all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(lambda x: self.remove_unicode_content(x))

        all_contents_new = all_contents_new.reset_index()

        all_contents_new['symbols'] = all_contents_new['question_info_clean_complete'].apply(lambda x: self.get_symbols(x))
        list_of_list = all_contents_new['symbols'].tolist()
        flat_list = [item for sublist in list_of_list for item in sublist]
        list_symbols  = list(set(flat_list))
        
        all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(lambda x: self.clean_data(x,list_symbols))

        all_contents_new['answer_info'] = all_contents_new['answers'].apply(lambda x: self.Parse_Question_Text(x))

        all_contents_new['correct_option'] = all_contents_new['answer_info'].apply(lambda x: self.clean(x))
        
        #all_contents_new['correct_option'] = all_contents_new['correct_option'].apply(lambda x: self.remove_page_margin(x))

        all_contents_new_removed_dup = all_contents_new[~all_contents_new['question_code'].str.contains("DUP")]

        all_contents_new_columns = all_contents_new_removed_dup[["question_code","subject","is_test","is_practice","created_at","exams","is_prev_year","question_info_clean_complete","correct_option","question_type","question_images"]]

        all_contents_new_columns["question_info_clean_complete"] = all_contents_new_columns["question_info_clean_complete"].apply(lambda x: str(x))
        all_contents_new_columns["correct_option"] = all_contents_new_columns["correct_option"].apply(lambda x: str(x))
        
        all_contents_new_columns["question_answer_clean"] = all_contents_new_columns["question_info_clean_complete"] +" "+ all_contents_new_columns["correct_option"]

        all_contents_new_columns["is_test"].fillna(0, inplace=True)
        all_contents_new_columns["is_practice"].fillna(0, inplace=True)

        return all_contents_new_columns


    def start_processing(self):
        if self.ca_df.empty:
            print("emptyDF")

        else:
            content_dataframe = self.process_dataframe(self.ca_df)
            content_dataframe.to_json(r"new_content_data",orient='records')
            self.ca_flag = True
       

        if self.prod_df.empty:
            print("emptyDF")
        else:
            prod_dataframe = self.process_dataframe(self.prod_df)
            prod_dataframe.to_json(r"new_production_data",orient='records')
            self.prod_flag = True
        
        if self.grail_df.empty:
            print("emptyDF")
        else:
            grail_dataframe = self.process_dataframe(self.grail_df)
            grail_dataframe.to_json(r"new_grail_data",orient='records')
            print("Today its updated")
            self.grail_flag = True

        return (self.ca_flag,self.prod_flag,self.grail_df)
        


    





    
    















