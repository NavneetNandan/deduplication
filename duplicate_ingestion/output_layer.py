from sqlalchemy import create_engine,text
import sys
sys.path.append('/home/ubuntu')
import pandas as pd
from bs4 import BeautifulSoup
import ast
import re
import html
import math
import numpy as np
import time
from elasticsearch import Elasticsearch,helpers
es = Elasticsearch("10.144.20.4")
import csv
import networkx as nx
from datetime import date,timedelta
import datetime
import json
import random
from process_ingestion import Proccessing
from datetime import date,timedelta
import datetime


class Ingest_ES:
    def __init__(self,ca_flag,prod_flag,grail_flag):
        self.flag_dict = {"ca":ca_flag,"prod":prod_flag,"grail":grail_flag}
        self.json_file_name_dict = {"ca":"new_content_data","prod":"new_production_data","grail":"new_grail_data"}
        self.index_dict = {"ca":"cqi-duplicate-contentadmin","prod":"cqi-duplicate-production","grail":"cqi-duplicate-grail-staging"}
        self.keys = ["ca","prod","grail"]
    
    def ingest_in_es(self,db_key):
        if self.flag_dict[db_key]:
            
            with open(self.json_file_name_dict[db_key]) as f:
                data = json.load(f)
            k = ({'_type':'my_type', '_index': self.index_dict[db_key],'_id':quest_data["question_code"],'question_code':quest_data['question_code'],'question_ans_clean':quest_data['question_answer_clean'],'question':quest_data['question_info_clean_complete'],'correct_option':quest_data['correct_option'],'is_test':quest_data['is_test'],'is_practice':quest_data['is_practice'],'subject':quest_data['subject'],'created_at':quest_data['created_at'],'exams':quest_data['exams'],'is_prev_year':quest_data['is_prev_year'],"question_type":quest_data["question_type"],"question_images":quest_data["question_images"]} for quest_data in data)
            
            helpers.bulk(es,k)

    def data_ingestion(self):
        for db_key in self.keys:
            self.ingest_in_es(db_key)

        today = date.today()
        td = today.strftime("%Y-%m-%d")
        input_date_df = pd.read_csv("last_saved.csv")
        length = input_date_df.shape[0]
        tdtime = "00:00:00"
        input_date_df.loc[length] = td+" "+tdtime
        input_date_df.to_csv("last_saved.csv")






