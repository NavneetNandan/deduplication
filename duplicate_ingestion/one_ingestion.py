from sqlalchemy import create_engine,text
import sys
sys.path.append('/home/ubuntu')
import pandas as pd
from bs4 import BeautifulSoup
import ast
import re
import html
import math
import numpy as np
import time
from datetime import date,timedelta
import datetime
import json
import random 
from content_ingestion_layer import CA_Ingestion
#from production_ingestion_layer import Prod_Ingestion
from process_ingestion import Proccessing
from output_layer import Ingest_ES
from content_grail_ingestion_layer import Content_Grail_Ingestion
#from duplicate_pairs import Duplicate_Pairs

class Single_ingestion:

    def get_last_successfull_run_date(self):
        self.input_date_df = pd.read_csv("last_saved.csv")
        self.last_date = self.input_date_df.iloc[-1]


    def gather_data(self):
        #ca_ingest = CA_Ingestion(self.last_date,full_run = True)
        #self.ca_df = ca_ingest.get_data_from_ca()
        #prod_ingest = Prod_Ingestion(self.last_date,full_run = True)
        #self.prod_df = prod_ingest.get_data_from_prod()
        self.grail_ingestion = Content_Grail_Ingestion(self.last_date,full_run = True)
        self.grail_df = self.grail_ingestion.get_data_from_grail()


    def process_data(self):
        #input_process = Proccessing(self.ca_df,self.prod_df,self.grail_df)
        #self.ca_flag, self.prod_flag,self.grail_flag = input_process.start_processing()

        input_process = Proccessing(grail_df=self.grail_df)
        self.ca_flag, self.prod_flag,self.grail_flag = input_process.start_processing()


    def ingest_in_es(self):
        #output_layer = Ingest_ES(self.ca_flag,self.prod_flag,self.grail_flag)
        #output_layer.data_ingestion()

        output_layer = Ingest_ES(False,False,True)
        output_layer.data_ingestion()
    
    def find_pairs(self):
        duplicate = Duplicate_Pairs()
        


    

    

if __name__ == "__main__":
    single_ingestion = Single_ingestion()
    single_ingestion.get_last_successfull_run_date()
    #single_ingestion.gather_data()
    #single_ingestion.process_data()
    single_ingestion.ingest_in_es()
    #single_ingestion.find_pairs()






















