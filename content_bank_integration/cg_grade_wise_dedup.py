import sys
import json

from elasticsearch import helpers
import pandas as pd
import pymongo
from elasticsearch import Elasticsearch
from pylatexenc.latex2text import LatexNodes2Text
import networkx as nx
import csv
from bs4 import BeautifulSoup
import ast
import re
import math
import threading
import Levenshtein
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from scipy.spatial.distance import cosine
from sentence_transformers import SentenceTransformer
import uuid

mongo_uri = "mongodb://ro_dsl:EHJpUwVO2vgMuk@10.141.11.77:27017/?authSource=contentgrail&readPreference=secondaryPreferred&appname=MongoDB%20Compass&ssl=false"

client = pymongo.MongoClient(mongo_uri)
db = client.contentgrail
collection = db.learning_objects
lm_coll = db.learning_maps
lmf_coll = db.learning_map_formats
lo_coll = db.learning_objects


def get_lm_codes(grades):
    lm_data = list(
        lm_coll.find({'status': 'active', 'format.index': 2, 'format.name': {"$in": grades}}, {"code": 1, "_id": 0}))
    return pd.DataFrame(lm_data).code.to_list()


def get_learning_objects(lo_type, lms, a_id):
    if a_id == None:
        lo_data = list(lo_coll.find(
            {'type': lo_type, 'status': 'Approved', 'content.question_meta_tags.learning_maps': {"$in": lms}}))
    else:
        lo_data = list(lo_coll.find(
            {'type': lo_type, "author_id": a_id, 'content.question_meta_tags.learning_maps': {"$in": lms}}))

    return pd.DataFrame(lo_data)


def get_grade_wise_learning_objects(lo_type, grades, a_id=None):
    lms = get_lm_codes(grades)
    data = get_learning_objects(lo_type, lms, a_id)
    return data

for grade in range(5, 13):
    ## Get questions with Grade
    all_data_dump = 'cg-{}th.csv'.format(grade)
    extracted_clean_data_json_dump = 'cg-{}th-cleaned'.format(grade)
    es_index = "cg-{}th-duplication-questions".format(grade)
    candidate_pairs_dump = 'cg-{}th-candidate_pairs.csv'.format(grade)
    cluster_exact_file = 'cg_{}_exact_duplicates_clusters.csv'.format(grade)
    final_question_level_output_index = 'cqi_cg_dedup_questions'
    final_cluster_level_output_index = 'cqi_cg_dedup_clusters'

    ques_los = get_grade_wise_learning_objects('Question', ["{}st CBSE".format(grade)])
    print(len(ques_los))
    tenth_qs = ques_los['question_code'].unique()
    df = pd.DataFrame(collection.find({"question_code": {"$in": list(tenth_qs)}},
                                      projection={"id": True, "_version": True, "question_code": True, "subtype": True,
                                                  "content": True}))
    df.to_csv(all_data_dump)
    print("data dumped to {}".format(all_data_dump))
    original_len = len(df)
    df = df.drop_duplicates(subset=["question_code"])
    after_duplicate_removal = len(df)
    print("total_duplicates are {}".format(original_len - after_duplicate_removal))
    df_extract = df[["id", "_version", "question_code", "subtype", "content"]]


    def extract_content_field(x):
        try:
            content = ast.literal_eval(x)
        except:
            content = x

        try:
            question_details = content["question_details"]
            tag = list(question_details.keys())[0]
            question_text = question_details[tag]["question_txt"]
        except:
            print(content)
            question_text = ""
        try:
            whole_answer = question_details[tag]["answers"]
            whole_answer_body = []
            # print(whole_answer)
            if not whole_answer:
                whole_answer_body = []
                correct_answer = ""

            else:
                try:
                    for answer in whole_answer:
                        if answer["is_correct"] == True:
                            correct_answer = answer["body"]
                            whole_answer_body.append(correct_answer)

                        whole_answer_body.append(answer["body"])
                except:
                    try:
                        for answer in whole_answer[0]:
                            if answer["is_correct"] == True:
                                correct_answer = answer["body"]
                                whole_answer_body.append(correct_answer)

                            whole_answer_body.append(answer["body"])
                    except:
                        whole_answer_body = []
                        correct_answer = ""
        except:
            whole_answer_body = []
            correct_answer = ""

        try:
            question_text_answer = (question_text, correct_answer, whole_answer_body)
        except:
            question_text_answer = (question_text, "", whole_answer_body)

        return question_text_answer


    df_extract["extracted_field"] = df_extract["content"].apply(extract_content_field)

    df_extract["question_text"] = df_extract["extracted_field"].apply(lambda x: x[0])

    df_extract["correct_answer_text"] = df_extract["extracted_field"].apply(lambda x: x[1])

    df_extract["whole_answer_body"] = df_extract["extracted_field"].apply(lambda x: x[2])


    # sys.path.append('/home/ubuntu')


    class Proccessing:
        def __init__(self, ca_df=pd.DataFrame(), prod_df=pd.DataFrame(), grail_df=pd.DataFrame()):
            self.ca_df = ca_df
            self.prod_df = prod_df
            self.grail_df = grail_df
            self.prod_flag = False
            self.ca_flag = False
            self.grail_flag = False
            # self.file_name = file_name

        @staticmethod
        def Parse_Question_Text(question_info_object):
            try:
                question_info_object = question_info_object['en']
            except:
                pass
            try:
                qcontent = re.sub('&nbsp;', ' ', question_info_object).replace('&there4;', ' ')
                return qcontent
            except:
                return question_info_object

        @staticmethod
        def clean(text):
            text = str(text)
            if not text:
                return "No explanation_given"
            else:
                soup = BeautifulSoup(text, 'html.parser')
                soup.prettify()
                soup_text = soup.get_text()
                soup_text = soup_text.replace("\\\\", "\\")
                soup_text = soup_text.replace("\\n", " ")
                try:
                    latex_text = LatexNodes2Text().latex_to_text(soup_text)
                except:
                    print(soup_text)
                    new_generated_text = soup_text.strip('\"').replace('\\\\', '\\').replace('\\"', '\"').replace('\\n',
                                                                                                                  '\n').replace(
                        "\\r", "\r").replace("\\t", "\t")
                    latex_text = LatexNodes2Text().latex_to_text(new_generated_text)

                latex_text = latex_text.strip('\"')
                latex_text = re.sub(r"(^Q\w+.)", '', latex_text)
                latex_text = re.sub(r"(^Q.\w+.:)", '', latex_text)
                latex_text = re.sub(r"(^Q\w+.)", '', latex_text)
                latex_text = re.sub(r"(^Ans\w+.)", '', latex_text)
                latex_text = latex_text.replace("\n", " ")
                latex_text = latex_text.replace("\r", " ")
                latex_text = latex_text.replace("  ", " ")
                latex_text = latex_text.replace("   ", " ")
                latex_text = re.sub(r'\n', "", latex_text)
                latex_text = re.sub(r'\r', "", latex_text)

            return latex_text

        @staticmethod
        def cleaning_content(qcontent):
            try:
                qcontent = re.sub('<[^>]*>', ' ', qcontent).replace('&nbsp;', ' ').replace('&there4;', ' ').replace('&#39;',
                                                                                                                    '').replace(
                    '<sup>', '').replace('</sup>', '').replace('<br>', '').replace('<br />', '')
            except Exception as e:
                pass
            return qcontent

        @staticmethod
        def get_symbols(mstring):
            try:
                mstring = str(mstring)
                list_symbols = []
                list_items = mstring.split()
                for item in list_items:
                    if (len(item) == 1):
                        if ((re.match('[a-z]', item) == None) and (item != ' ') and (
                                item not in ['.', ',', '\'', ';', '?', '\u2061'])):
                            list_symbols.append(item)

                list_symbols = list(set(list_symbols))
                return list_symbols
            except:
                print(mstring)
                return []

        @staticmethod
        def clean_data(string, list_symbols):
            try:
                string = string.split(' ')
                unwanted = []
                for i in string:
                    if (len(i) == 1):
                        if (ord(i) > 128 and i not in list_symbols):
                            unwanted.append(i)
                unwanted = list(unwanted)
                for i in unwanted:
                    string.remove(i)
                string = ' '.join(string)
                return string
            except:
                return string

        @staticmethod
        def remove_unicode_content(qcontent):
            try:
                qcontent = re.sub('\u2061', '', qcontent)
                return qcontent
            except:
                return qcontent

        @staticmethod
        def remove_page_margin(content):
            if content.startswith("<!--"):
                return ""
            else:
                return content

        def process_dataframe(self, all_contents_new):
            #         print(all_contents_new.columns)
            all_contents_new['question_info_clean'] = all_contents_new['question_text'].apply(
                lambda x: self.Parse_Question_Text(x))

            all_contents_new.dropna(subset=['question_info_clean'], inplace=True)

            all_contents_new['question_info_clean'] = all_contents_new['question_info_clean'].apply(lambda x: self.clean(x))

            all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean'].apply(
                lambda x: self.cleaning_content(x))

            all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(
                lambda x: self.remove_unicode_content(x))

            all_contents_new = all_contents_new.reset_index()

            all_contents_new['symbols'] = all_contents_new['question_info_clean_complete'].apply(
                lambda x: self.get_symbols(x))
            list_of_list = all_contents_new['symbols'].tolist()
            flat_list = [item for sublist in list_of_list for item in sublist]
            list_symbols = list(set(flat_list))

            all_contents_new['question_info_clean_complete'] = all_contents_new['question_info_clean_complete'].apply(
                lambda x: self.clean_data(x, list_symbols))

            all_contents_new['answer_info'] = all_contents_new['correct_answer_text'].apply(
                lambda x: self.Parse_Question_Text(x))

            all_contents_new['correct_option'] = all_contents_new['answer_info'].apply(lambda x: self.clean(x))

            all_contents_new['correct_option'] = all_contents_new['correct_option'].apply(
                lambda x: self.remove_page_margin(x))

            # all_contents_new_removed_dup = all_contents_new[~all_contents_new['question_code'].str.contains("DUP")]

            # all_contents_new_columns = all_contents_new_removed_dup[["question_code","subject","is_test","is_practice","created_at","exams","is_prev_year","question_info_clean_complete","correct_option","question_type","question_images","answer_images"]]

            all_contents_new["question_info_clean_complete"] = all_contents_new["question_info_clean_complete"].apply(
                lambda x: str(x))
            all_contents_new["correct_option"] = all_contents_new["correct_option"].apply(lambda x: str(x))

            # all_contents_new["question_answer_clean"] = all_contents_new["question_info_clean_complete"] +" "+ all_contents_new["correct_option"]

            # all_contents_new_columns["is_test"].fillna(0, inplace=True)
            # all_contents_new_columns["is_practice"].fillna(0, inplace=True)

            return all_contents_new

        def start_processing(self):
            #         print(self.grail_df)
            if self.ca_df.empty:
                print("emptyDF")

            else:
                content_dataframe = self.process_dataframe(self.ca_df)
                content_dataframe.to_json(r"new_content_data", orient='records')
                self.ca_flag = True

            if self.prod_df.empty:
                print("emptyDF")
            else:
                prod_dataframe = self.process_dataframe(self.prod_df)
                prod_dataframe.to_json(r"new_production_data", orient='records')
                self.prod_flag = True

            if self.grail_df.empty:
                print("emptyDF")
            else:
                #             print(self.grail_df)
                grail_dataframe = self.process_dataframe(self.grail_df)
                return grail_dataframe
                # old_dataframe = pd.read_json("new_grail_data",orient="records")
                # newly_merged_df = pd.concat( [grail_dataframe, old_dataframe],ignore_index=True)
                grail_dataframe.to_json(self.file_name, orient='records')
                # newly_merged_df.to_json(r"new_grail_data",orient='records')
                print("Today its updated")
                self.grail_flag = True

            return (self.ca_flag, self.prod_flag, self.grail_flag)


    input_process = Proccessing(grail_df=df_extract)
    grail_dataframe = input_process.start_processing()

    grail_dataframe_extract = grail_dataframe[
        ["id", "question_code", "subtype", "question_text", "question_info_clean_complete", "correct_answer_text",
         "correct_option", "whole_answer_body"]]
    # grail_dataframe_extract

    grail_dataframe_extract.to_json(extracted_clean_data_json_dump, orient="records")

    sys.path.append('/home/ubuntu')

    es = Elasticsearch("10.144.131.98")


    # from process_ingestion import Proccessing


    class Ingest_ES:
        def __init__(self, ca_flag, prod_flag, grail_flag):
            self.flag_dict = {"ca": ca_flag, "prod": prod_flag, "grail": grail_flag}
            self.json_file_name_dict = {"ca": "new_content_data", "prod": "new_production_data",
                                        "grail": extracted_clean_data_json_dump}
            self.index_dict = {"ca": "cqi-duplicate-contentadmin", "prod": "cqi-duplicate-production", "grail": es_index}
            self.keys = ["ca", "prod", "grail"]

        def ingest_in_es(self, db_key):
            if self.flag_dict[db_key]:
                with open(self.json_file_name_dict[db_key]) as f:
                    data = json.load(f)
                k = ({'_type': 'my_type', '_index': self.index_dict[db_key], '_id': quest_data["id"],
                      'question_code': quest_data['question_code'], 'question': quest_data['question_info_clean_complete']}
                     for quest_data in data)

                helpers.bulk(es, k, request_timeout=40)

        def data_ingestion(self):
            for db_key in self.keys:
                self.ingest_in_es(db_key)


    output_layer = Ingest_ES(False, False, True)
    output_layer.data_ingestion()
    grail_dataframe_2 = grail_dataframe_extract[["question_code", "question_info_clean_complete"]]


    sys.path.append('/home/ubuntu')

    es = Elasticsearch("10.144.131.98")
    num_threads = 8
    expected_output = []
    # import networkx as nx



    num_threads = 8
    expected_output = []

    not_es_search_codes = []

    list_production = grail_dataframe_2.values.tolist()

    print(len(list_production))


    def lavenstein_ratio(set_a, set_b):
        return Levenshtein.ratio(set_a, set_b)


    def fetch_Laven(result_df):
        if str(result_df.duplicate_question_text) != ' ':
            return lavenstein_ratio(str(result_df.original_question_text), str(result_df.duplicate_question_text))
        else:
            return 0


    def calculate_laven_ratios(formated_output):
        #     print(formated_output)
        laven_df = pd.DataFrame(formated_output,
                                columns=['original_question_code', 'original_question_text', 'duplicate_question_code',
                                         'duplicate_question_text'])
        laven_df['Laven_ratios'] = laven_df.apply(fetch_Laven, axis=1)
        laven_df = laven_df[laven_df['Laven_ratios'] > 0.79]
        if laven_df.empty:
            return []
        else:
            laven_df_quest = laven_df[
                ['original_question_code', 'duplicate_question_code', "Laven_ratios", "original_question_text",
                 "duplicate_question_text"]]
            laven_list = laven_df_quest.values.tolist()
            return laven_list


    def find_dups(query, finalresult, length, question_code):
        ids = []
        for result, score in finalresult:
            if result['question_code'] == question_code:
                continue
            ids.append((result['question_code'], result['question']))
        return ids


    def search(question_text, question_code, expected_output, not_es_search_codes):
        query = str(question_text)
        length = len(query)
        field = "question"
        complex_query = {
            "size": 50,
            "query": {
                "match": {
                    field: {
                        "query": query
                    }
                }
            }
        }
        try:
            search = es.search(index=es_index, body=complex_query, request_timeout=100)
            finalresult = []
            for res in search['hits']['hits']:
                result = res['_source']
                score = res['_score']
                finalresult.append([result, score])
            output = find_dups(query, finalresult, length, question_code)
            if len(output) >= 1:
                formated_output = [(question_code, question_text, x[0], x[1]) for x in output]
                formated_laven = calculate_laven_ratios(formated_output)
                expected_output.extend(formated_laven)

            else:
                return
        except:
            not_es_search_codes.append(question_code)
            print(question_code)


    threads = [threading.Thread(target=search, args=(items[1], items[0], expected_output, not_es_search_codes)) for items in
               list_production]

    for i in range(0, int(len(threads) / num_threads) + 1):
        [ti.start() for ti in threads[i * num_threads:(i + 1) * num_threads]]
        [ti.join() for ti in threads[i * num_threads:(i + 1) * num_threads]]

    candidate_df = pd.DataFrame(expected_output,
                                columns=['original_question_code', 'duplicate_question_code', "Laven_ratios",
                                         "original_question_text", "duplicate_question_text"])
    del expected_output[:]
    del expected_output
    del list_production
    candidate_df.to_csv(candidate_pairs_dump)
    # candidate_df
    candidate_df["tuple_org_dup_text"] = candidate_df.apply(
        lambda x: (x["original_question_text"], x["duplicate_question_text"]), axis=1)

    candidate_df["same"] = candidate_df.apply(
        lambda x: True if (x["original_question_code"] == x["duplicate_question_code"]) else False, axis=1)


    embedder = SentenceTransformer('bert-base-nli-mean-tokens')




    def get_transformer_score(df):
        original_question_text, duplicate_question_text = df[0], df[1]
        query_embedding = embedder.encode([original_question_text])
        another_query_embedding = embedder.encode([duplicate_question_text])
        distances = cosine(query_embedding, another_query_embedding)
        real_distance = round((1 - distances) * 100, 2)
        return real_distance


    candidate_df_85 = candidate_df[candidate_df["Laven_ratios"] > 0.85]
    # candidate_df_85["transformer_scores"] = candidate_df_85["tuple_org_dup_text"].apply(get_transformer_score)
    # candidate_df_85

    candidate_df_85["transformer_score"] = 0  # TODO: Remove this line


    stop_words = set(stopwords.words('english'))

    stop_words.remove('not')
    stop_words.remove("which")
    stop_words.remove("what")
    stop_words.remove("why")
    stop_words.remove("how")


    def make_lower(text):
        return text.lower()


    def remove_punct(text):
        punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
        no_punct = ""
        for char in text:
            if char not in punctuations:
                no_punct = no_punct + char
        return no_punct


    def remove_stop_words(text):
        word_tokens = word_tokenize(text)
        filtered_sentence = []
        for w in word_tokens:
            if w not in stop_words:
                filtered_sentence.append(w)
        string = ' '.join(word for word in filtered_sentence)
        return string


    def lavenstein_ratio(set_a, set_b):
        return Levenshtein.ratio(set_a, set_b)


    def clean_laven_ratios(df):
        dup_cleaned_question_text = make_lower(df["duplicate_question_text"])
        cleaned_question_text = make_lower(df["original_question_text"])

        dup_cleaned_question_text = remove_punct(dup_cleaned_question_text)
        cleaned_question_text = remove_punct(cleaned_question_text)

        dup_cleaned_question_text = remove_stop_words(dup_cleaned_question_text)
        cleaned_question_text = remove_stop_words(cleaned_question_text)

        clean_ratio = lavenstein_ratio(cleaned_question_text, dup_cleaned_question_text)
        return clean_ratio


    candidate_df_85["clean_laven_ratios"] = candidate_df_85.apply(clean_laven_ratios, axis=1)
    candidate_df_85["same"] = candidate_df_85.apply(
        lambda x: True if (x["original_question_code"] == x["duplicate_question_code"]) else False, axis=1)
    candidate_df_85 = candidate_df_85[candidate_df_85["same"] != True]
    # candidate_df_85.rename(columns = {'transformer_scores':'transformer_score'}, inplace = True)
    candidate_df_85["true_labels"] = 1



    porter = PorterStemmer()
    clean_ratios_inside = []


    def check_duplicate_exact(df):
        original_question_text = df["original_question_text"]
        duplicate_question_text = df["duplicate_question_text"]

        dup_cleaned_question_text = remove_stop_words(duplicate_question_text)
        cleaned_question_text = remove_stop_words(original_question_text)

        dup_cleaned_question_text = remove_punct(dup_cleaned_question_text)
        cleaned_question_text = remove_punct(cleaned_question_text)

        dup_cleaned_question_text = make_lower(dup_cleaned_question_text)
        cleaned_question_text = make_lower(cleaned_question_text)

        original_question_text_list = cleaned_question_text.split(" ")
        duplicate_question_text_list = dup_cleaned_question_text.split(" ")

        x = []
        y = []

        for index, value in enumerate(original_question_text_list):
            try:
                if (value != duplicate_question_text_list[index]):
                    value = porter.stem(value)
                    x.append(value)
            except:
                value = porter.stem(value)
                x.append(value)

        for index, value in enumerate(duplicate_question_text_list):
            try:
                if (value != original_question_text_list[index]):
                    value = porter.stem(value)
                    y.append(value)
            except:
                value = porter.stem(value)
                y.append(value)

        rem_cleaned_question_text = ' '.join(word for word in x)
        rem_dup_cleaned_question_text = ' '.join(word for word in y)
        # print(rem_cleaned_question_text)
        # print("rem_dup_cleaned_question_text",rem_dup_cleaned_question_text)

        clean_ratio = lavenstein_ratio(rem_cleaned_question_text, rem_dup_cleaned_question_text)

        #     if ((df["transformer_score"]>=98) & (df["clean_laven_ratios"]>=0.98)):
        if ((df["clean_laven_ratios"] >= 0.98)):

            #         if (df["transformer_score"] <90) or (df["clean_laven_ratios"] <0.91):
            if (df["clean_laven_ratios"] < 0.91):
                clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                            original_question_text, duplicate_question_text, df["true_labels"], 0,
                                            df["transformer_score"], df["clean_laven_ratios"]))
                return 0

            if df["clean_laven_ratios"] >= 1.0:
                clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                            original_question_text, duplicate_question_text, df["true_labels"], 1,
                                            df["transformer_score"], df["clean_laven_ratios"]))
                return 1
            # else:
            # clean_ratios_inside.append((clean_ratio,rem_cleaned_question_text,rem_dup_cleaned_question_text,original_question_text,duplicate_question_text,df["true_labels"],0))
            # return 0
            if (rem_cleaned_question_text == "") or (rem_cleaned_question_text == " ") or (
                    rem_dup_cleaned_question_text == "") or (rem_dup_cleaned_question_text == " "):
                clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                            original_question_text, duplicate_question_text, df["true_labels"], 1,
                                            df["transformer_score"], df["clean_laven_ratios"]))
                return 1

            if clean_ratio >= 0.80:
                clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                            original_question_text, duplicate_question_text, df["true_labels"], 1,
                                            df["transformer_score"], df["clean_laven_ratios"]))
                return 1
            else:
                clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                            original_question_text, duplicate_question_text, df["true_labels"], 0,
                                            df["transformer_score"], df["clean_laven_ratios"]))
                return 0
            clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                        original_question_text, duplicate_question_text, df["true_labels"], 1,
                                        df["transformer_score"], df["clean_laven_ratios"]))
            return 1

        #     if (df["transformer_score"] <90) or (df["clean_laven_ratios"] <0.91):
        if (df["clean_laven_ratios"] < 0.91):
            clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                        original_question_text, duplicate_question_text, df["true_labels"], 0,
                                        df["transformer_score"], df["clean_laven_ratios"]))
            return 0

        if (rem_cleaned_question_text == "") or (rem_cleaned_question_text == " ") or (
                rem_dup_cleaned_question_text == "") or (rem_dup_cleaned_question_text == " "):
            clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                        original_question_text, duplicate_question_text, df["true_labels"], 1,
                                        df["transformer_score"], df["clean_laven_ratios"]))
            return 1

            # query_embedding = embedder.encode([rem_cleaned_question_text])
            # another_query_embedding = embedder.encode([rem_dup_cleaned_question_text])
            # distances = cosine(query_embedding, another_query_embedding)
            # real_distance = round((1-distances)*100,2)

        # return clean_ratio
        if (clean_ratio >= 0.84):
            clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                        original_question_text, duplicate_question_text, df["true_labels"], 1,
                                        df["transformer_score"], df["clean_laven_ratios"]))
            return 1

        else:
            clean_ratios_inside.append((clean_ratio, rem_cleaned_question_text, rem_dup_cleaned_question_text,
                                        original_question_text, duplicate_question_text, df["true_labels"], 0,
                                        df["transformer_score"], df["clean_laven_ratios"]))
            return 0


    candidate_df_85["pred_label"] = candidate_df_85.apply(check_duplicate_exact, axis=1)
    candidate_df_85_exact = candidate_df_85[candidate_df_85["pred_label"] == 1]
    candidate_df_85_similar = candidate_df_85[candidate_df_85["pred_label"] == 0]
    correct_answer_df = grail_dataframe_extract[["question_code", "correct_option"]]


    def get_answer(df):
        original_question_code = df["original_question_code"]
        duplicate_question_code = df["duplicate_question_code"]
        try:
            org_ans = correct_answer_df[correct_answer_df["id"] == original_question_code]["correct_option"].values[0]
        except:
            org_ans = ""
        try:
            dup_ans = correct_answer_df[correct_answer_df["id"] == duplicate_question_code]["correct_option"].values[0]
        except:
            return ""
        return (org_ans, dup_ans)


    candidate_df_85_exact["answers"] = candidate_df_85_exact.apply(get_answer, axis=1)

    org_correct_ans = pd.merge(correct_answer_df, candidate_df_85_exact, left_on="question_code",
                               right_on="original_question_code")
    dup_correct_ans = pd.merge(correct_answer_df, org_correct_ans, left_on="question_code",
                               right_on="duplicate_question_code")
    added_id_df = pd.merge(grail_dataframe_extract[['id', 'question_code']], dup_correct_ans, left_on="question_code",
                           right_on="original_question_code")
    added_id_df = added_id_df.drop(columns=['question_code'])
    added_id_df = added_id_df.rename(columns={"id": "original_id"})
    added_id_df = pd.merge(grail_dataframe_extract[['id', 'question_code']], added_id_df, left_on="question_code",
                           right_on="duplicate_question_code")
    added_id_df = added_id_df.drop(columns=['question_code'])
    added_id_df = added_id_df.rename(columns={"id": "duplicate_id"})
    dup_correct_ans = added_id_df

    def laven_ans_ratio(df):
        correct_option_x = df["correct_option_x"]
        correct_option_y = df["correct_option_y"]
        if (correct_option_x == "") or (correct_option_y == ""):
            return 1

        return lavenstein_ratio(correct_option_x, correct_option_y)


    def make_answer_lower(text):
        try:
            if math.isnan(text):
                return ""
        except:
            return text.lower()


    dup_correct_ans["correct_option_x"] = dup_correct_ans["correct_option_x"].apply(make_answer_lower)
    dup_correct_ans["correct_option_y"] = dup_correct_ans["correct_option_y"].apply(make_answer_lower)

    dup_correct_ans["laven_ans_ratio"] = dup_correct_ans.apply(laven_ans_ratio, axis=1)
    dup_correct_ans["laven_ans_ratio"].value_counts(bins=10)
    dup_correct_ans_exact = dup_correct_ans[
        (dup_correct_ans["laven_ans_ratio"] > 0.72) & (dup_correct_ans["laven_ans_ratio"] <= 1)]
    dup_correct_ans_exact[['original_question_text', 'duplicate_question_text']]


    duplicate_pairs_list = []
    for index, row in dup_correct_ans_exact.iterrows():
        duplicate_pairs_list.append((str(row["original_id"]), str(row["duplicate_id"])))
    G_dup = nx.Graph()
    G_dup.add_edges_from(duplicate_pairs_list)
    clusters = list(G_dup.subgraph(c) for c in nx.connected_components(G_dup))
    # clusters = list(nx.connected_component_subgraphs(G_dup))
    cluster_outcsv_85 = open(cluster_exact_file, 'w')
    cluster_writer_85 = csv.writer(cluster_outcsv_85)
    cluster_writer_85.writerow(['cluster_question_codes'])
    for i in range(len(clusters)):
        cluster_writer_85.writerow([', '.join(clusters[i].nodes())])
    cluster_outcsv_85.close()
    cluster_df = pd.read_csv(cluster_exact_file)
    cluster_level_info = []
    failed = []
    for i, row in cluster_df.iterrows():
        questions = row['cluster_question_codes'].split(',')
        #     hygiene_detail_res = requests.post("http://api.embibe.com/dsl/cqi/question_hygiene",json={"qid":row['cluster_question_codes']})
        cluster_id = uuid.uuid1()
        cluster_q_ids = []
        cluster_hygiene_scores = []
        cluster_reps = []
        # data = hygiene_detail_res.json()['data']
        #     max_hygiene = max([i['avg_hygiene_score'] for i in data])
        max_hygiene = -1
        index = -1
        question_level_info = []
        max_found = False
        for q_i, question_id in enumerate(questions):
            #     for question_hygiene in data:
            question_id = question_id.strip()
            try:
                avg_hygiene = es.get('cqi-content-bank', question_id)['_source']['avg_hygiene_score']
            except Exception:
                failed.append(question_id)
                pass
            #         question_id = question_hygiene['question_id']
            #         avg_hygiene = question_hygiene['avg_hygiene_score']
            max_hygiene = avg_hygiene if avg_hygiene > max_hygiene else max_hygiene
            index = q_i if avg_hygiene > max_hygiene else index
            question_level_info.append(
                {'question_id': question_id, 'cluster_id': cluster_id, 'avg_hygiene_score': avg_hygiene,
                 'is_rep': False,'grade': grade})
            cluster_q_ids.append(question_id)
            cluster_hygiene_scores.append(avg_hygiene)
            cluster_reps.append(False)
        question_level_info[index]['is_rep'] = True
        cluster_reps[index] = True
        cluster_level_info.append(
            {'question_ids': cluster_q_ids, 'cluster_id': cluster_id, 'avg_hygiene_scores': cluster_hygiene_scores,
             'rep': cluster_reps,'grade':grade})
        for question_info in question_level_info:
            print(question_info['question_id'])
            try:
                es.update('cqi-content-bank', id=question_info['question_id'], body={
                    "doc": {"dedup_cluster_id": question_info['cluster_id'], "dedup_is_rep": question_info['is_rep']}})
            except Exception as e:
                pass
            es.index(final_question_level_output_index, question_info)

    for cluster_info in cluster_level_info:
        es.index(final_cluster_level_output_index,cluster_info)
